package com.cronos.practice.leaderboard.service;

import com.cronos.bukkit.utils.BukkitUtil;
import com.cronos.practice.leaderboard.Leaderboard;
import com.cronos.practice.leaderboard.configuration.Configuration;
import com.sainttx.holograms.api.Hologram;
import com.sainttx.holograms.api.HologramManager;
import com.sainttx.holograms.api.HologramPlugin;
import com.sainttx.holograms.api.line.HologramLine;
import com.sainttx.holograms.api.line.TextLine;
import lombok.NonNull;
import org.bukkit.Location;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class HologramService extends BukkitUtil {

    private final HologramManager manager;
    private final Configuration configuration;
    private final ContentService contentService;
    private Hologram hologram;

    public HologramService(Leaderboard plugin) {
        super(plugin);
        this.contentService = plugin.getContentService();
        this.configuration = plugin.getConfiguration();
        this.manager = JavaPlugin.getPlugin(HologramPlugin.class).getHologramManager();
        load();
    }

    public void load() {
        if (Objects.isNull(configuration.getLocation()))
            return;
        hologram = new Hologram("practice_leaderboard", configuration.getLocation());
        manager.addActiveHologram(hologram);
        setLines();
    }

    public void refresh() {
        if (Objects.isNull(configuration.getLocation())) {
            load();
            return;
        }
        new ArrayList<>(hologram.getLines()).forEach(hologramLine -> hologram.removeLine(hologramLine));
        setLines();
    }

    private void setLines() {
        if (Objects.isNull(configuration.getLocation()))
            return;
        addLine(color(configuration.getTitle()), 0);
        for (int i = 1; i <= configuration.getLength(); i++) {
            int finalI = i;
            contentService.get(i)
                    .ifPresent(content -> addLine(colorAndFormat(configuration.getContent(), "position", content.getPosition(), "elo", content.getElo(), "name", content.getName()), finalI));
        }
        if (!hologram.isSpawned())
            hologram.spawn();
    }

    public void reload() {
        if (Objects.nonNull(hologram)) {
            hologram.despawn();
            manager.deleteHologram(hologram);
        }
        load();
    }

    public void move(@NonNull Location location) {
        if (Objects.nonNull(hologram))
            hologram.teleport(location);
    }

    private void addLine(String text, int index) {
        if (Objects.isNull(hologram))
            return;
        HologramLine line = new TextLine(hologram, text);
        hologram.addLine(line, index);
    }
}
