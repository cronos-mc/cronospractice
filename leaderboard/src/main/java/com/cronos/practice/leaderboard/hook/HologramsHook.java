package com.cronos.practice.leaderboard.hook;

import com.cronos.commons.hooks.PluginHook;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class HologramsHook extends PluginHook {

    @Override
    public String getPluginName() {
        return "Holograms";
    }

    @Override
    public boolean isRequired() {
        return true;
    }
}
