package com.cronos.practice.leaderboard.command;

import com.cronos.bukkit.commands.BukkitCommand;
import com.cronos.bukkit.commands.integrated.HelpCommand;
import com.cronos.bukkit.commands.integrated.ReloadCommand;
import com.cronos.bukkit.commands.integrated.VersionCommand;
import com.cronos.commons.commands.CommandResult;
import com.cronos.commons.utils.StringUtil;
import com.cronos.practice.leaderboard.Leaderboard;
import org.bukkit.command.CommandSender;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class MainCommand extends BukkitCommand {

    public MainCommand(Leaderboard plugin) {
        super(plugin, "leaderboard");

        setDescription("Manage leaderboard.");
        addSubCommands(new ReloadCommand(plugin, "leaderboard.reload"),
                new VersionCommand(plugin),
                new SetLocationCommand(plugin),
                new RefreshCommand(plugin));
    }

    @Override
    protected CommandResult execute(CommandSender sender, Object[] objects) {
        sender.sendMessage(color(StringUtil.getCenteredMessage("&f&m                                    ")));
        sender.sendMessage(color(StringUtil.getCenteredMessage("&b&l" + getBukkitPlugin().getName() + " &f&lv" + getBukkitPlugin().getDescription().getVersion())));
        sender.sendMessage(color(StringUtil.getCenteredMessage("&f&lDeveloped by &b&lSharkz")));
        sender.sendMessage(color(StringUtil.getCenteredMessage("&f&m                                    ")));
        return CommandResult.SUCCESS;
    }
}
