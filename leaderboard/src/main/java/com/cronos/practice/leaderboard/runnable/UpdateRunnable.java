package com.cronos.practice.leaderboard.runnable;

import com.cronos.practice.leaderboard.Leaderboard;
import com.cronos.practice.leaderboard.service.ContentService;
import com.cronos.practice.leaderboard.service.HologramService;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class UpdateRunnable extends BukkitRunnable {

    private final ContentService service;
    private final HologramService hologramService;

    public UpdateRunnable(Leaderboard plugin) {
        this.service = plugin.getContentService();
        this.hologramService = plugin.getHologramService();
    }

    @Override
    public void run() {
        service.update();
        hologramService.refresh();
    }
}
