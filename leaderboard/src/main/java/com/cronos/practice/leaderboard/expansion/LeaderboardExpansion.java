package com.cronos.practice.leaderboard.expansion;

import com.cronos.core.bukkit.BukkitCore;
import com.cronos.practice.leaderboard.model.LeaderboardContent;
import com.cronos.practice.leaderboard.service.ContentService;
import lombok.RequiredArgsConstructor;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@RequiredArgsConstructor
public class LeaderboardExpansion extends PlaceholderExpansion {

    private BukkitCore plugin;
    private final ContentService contentService;

    @Override
    public @NotNull String getIdentifier() {
        return "practice";
    }

    @Override
    public @NotNull String getAuthor() {
        return "Sharkz";
    }

    @Override
    public @NotNull String getVersion() {
        return "1.0";
    }

    @Override
    public String getRequiredPlugin() {
        return "Bukkit-Core";
    }

    @Override
    public boolean canRegister() {
        return (plugin = (BukkitCore) Bukkit.getPluginManager().getPlugin(getRequiredPlugin())) != null;
    }

    @Override
    public String onRequest(OfflinePlayer player, @NotNull String args) {
        if (args.contains("top_")) {
            int index = Integer.parseInt(args.replaceAll("[^0-9]", ""));
            Optional<LeaderboardContent> element = contentService.get(index);
            if (!element.isPresent())
                return "&c&lx";
            LeaderboardContent p = element.get();
            if (args.contains("_name"))
                return p.getName();
            else if (args.contains("_elo"))
                return String.valueOf(p.getElo());
        }
        return "&c&lx";
    }
}
