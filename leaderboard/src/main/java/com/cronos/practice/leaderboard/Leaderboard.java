package com.cronos.practice.leaderboard;

import com.cronos.bukkit.BukkitPlugin;
import com.cronos.practice.leaderboard.command.MainCommand;
import com.cronos.practice.leaderboard.configuration.Configuration;
import com.cronos.practice.leaderboard.expansion.LeaderboardExpansion;
import com.cronos.practice.leaderboard.hook.HologramsHook;
import com.cronos.practice.leaderboard.hook.PlaceholderAPIHook;
import com.cronos.practice.leaderboard.internalization.LeaderboardEn;
import com.cronos.practice.leaderboard.runnable.UpdateRunnable;
import com.cronos.practice.leaderboard.service.ContentService;
import com.cronos.practice.leaderboard.service.HologramService;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j(topic = "Leaderboard")
@Getter
public final class Leaderboard extends BukkitPlugin {

    /* Configuration */
    private Configuration configuration;

    /* Content */
    private ContentService contentService;

    /* Hologram */
    private HologramService hologramService;

    @Override
    public void preEnable() {
    }

    @Override
    public void postEnable() {
        /* Hooks */
        registerHook(new HologramsHook());
        registerHook(new PlaceholderAPIHook());
        getHookService().run();
        if (!isHooked(HologramsHook.class) || !isHooked(PlaceholderAPIHook.class))
            return;

        /* Translations */
        registerTranslation(LeaderboardEn.class);

        /* Configuration */
        this.configuration = registerConfiguration(Configuration.class, this);

        /* Content */
        this.contentService = new ContentService(this);

        /* Hologram */
        this.hologramService = new HologramService(this);

        /* Placeholder Expansion */
        log.info("Registering placeholder expansion...");
        new LeaderboardExpansion(contentService).register();
        log.info("Placeholder expansion registered!");

        /* Commands */
        registerCommand(MainCommand.class, this);

        /* Updater */
        getTaskService()
                .getFactory()
                .newRepeatingTask(new UpdateRunnable(this), 10);
    }

    @Override
    public void onReload() {
        super.onReload();

        /* Hologram */
        this.hologramService.reload();
    }
}
