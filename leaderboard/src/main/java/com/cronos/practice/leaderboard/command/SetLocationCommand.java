package com.cronos.practice.leaderboard.command;

import com.cronos.bukkit.commands.BukkitCommand;
import com.cronos.bukkit.configuration.serializers.LocationSerializer;
import com.cronos.commons.commands.CommandResult;
import com.cronos.commons.commands.SenderType;
import com.cronos.practice.leaderboard.Leaderboard;
import com.cronos.practice.leaderboard.configuration.Configuration;
import com.cronos.practice.leaderboard.service.HologramService;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.IOException;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class SetLocationCommand extends BukkitCommand {

    private final Configuration configuration;
    private final HologramService hologramService;

    public SetLocationCommand(Leaderboard plugin) {
        super(plugin, "set");
        this.configuration = plugin.getConfiguration();
        this.hologramService = plugin.getHologramService();

        setDescription("Set leaderboard location.");
        setAllowedSenderType(SenderType.PLAYER);
        setPermission("leaderboard.set");
    }

    @Override
    protected CommandResult execute(CommandSender commandSender, Object[] objects) {
        Location location = ((Player) commandSender).getLocation();
        hologramService.move(location);
        try {
            new LocationSerializer()
                    .serialize(configuration.getConfiguration()
                            .createSection("location"), location);
            configuration.save();
            //message(commandSender, LeaderboardEn.TELEPORTED);
        } catch (IOException | IllegalAccessException e) {
            //message(commandSender, LeaderboardEn.CONFIGURATION_ERROR);
            e.printStackTrace();
        }
        return CommandResult.SUCCESS;
    }
}
