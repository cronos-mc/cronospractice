package com.cronos.practice.leaderboard.model;

import lombok.Data;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class LeaderboardContent {

    private final int position;
    private final String name;
    private final int elo;
}
