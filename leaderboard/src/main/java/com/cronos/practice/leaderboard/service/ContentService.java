package com.cronos.practice.leaderboard.service;

import com.cronos.core.bukkit.BukkitCore;
import com.cronos.core.commons.players.PlayerRepository;
import com.cronos.practice.leaderboard.Leaderboard;
import com.cronos.practice.leaderboard.model.LeaderboardContent;
import com.cronos.practice.leaderboard.repository.ContentRepository;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ContentService {

    private final ContentRepository repository;
    private final PlayerRepository playerRepository;

    public ContentService(Leaderboard plugin) {
        this.repository = new ContentRepository();
        this.playerRepository = BukkitCore.getPlugin(BukkitCore.class).getPlayerRepository();
    }

    public void update() {
        this.repository.clear();
        AtomicInteger position = new AtomicInteger(1);
        playerRepository.loadAll()
                .stream()
                .sorted((o1, o2) -> Integer.compare(o2.getElo(), o1.getElo()))
                .forEachOrdered(profile -> repository.getContent().add(new LeaderboardContent(position.getAndIncrement(), profile.getName(), profile.getElo())));
    }

    public Optional<LeaderboardContent> get(int index) {
        return this.repository.get(index);
    }
}
