package com.cronos.practice.leaderboard.configuration;

import com.cronos.bukkit.configuration.ConfigurableFileWrapper;
import com.cronos.commons.configuration.Configurable;
import com.cronos.practice.leaderboard.Leaderboard;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Location;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
@Setter
public class Configuration extends ConfigurableFileWrapper {

    @Configurable
    private Location location;

    @Configurable
    private int length;

    @Configurable
    private String title;

    @Configurable
    private String content;

    public Configuration(Leaderboard plugin) {
        super(plugin, "config.yml");
    }
}
