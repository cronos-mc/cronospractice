package com.cronos.practice.leaderboard.hook;

import com.cronos.commons.hooks.PluginHook;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class PlaceholderAPIHook extends PluginHook {

    @Override
    public String getPluginName() {
        return "PlaceholderAPI";
    }

    @Override
    public boolean isRequired() {
        return true;
    }
}
