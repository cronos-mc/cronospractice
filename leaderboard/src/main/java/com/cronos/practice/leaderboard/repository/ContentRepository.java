package com.cronos.practice.leaderboard.repository;

import com.cronos.practice.leaderboard.model.LeaderboardContent;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ContentRepository {

    @Getter
    private final List<LeaderboardContent> content = new ArrayList<>();

    public void clear() {
        this.content.clear();
    }

    public Optional<LeaderboardContent> get(int index) {
        return content.stream()
                .filter(leaderboardContent -> leaderboardContent.getPosition() == index)
                .findFirst();
    }
}
