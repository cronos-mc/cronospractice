package com.cronos.practice.leaderboard.internalization;

import com.cronos.commons.internalization.InternalizationService;
import com.cronos.commons.internalization.Translation;
import com.cronos.commons.internalization.TranslationPack;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public enum LeaderboardEn implements TranslationPack {
    CONFIGURATION_ERROR("&c&l!! &eAn error occurred while save configuration!"),
    TELEPORTED("&a&l!! &7Leaderboard has been teleported to your location!"),
    REFRESHED("&a&l!! &7Leaderboard has been refreshed!"),
    ;

    private final List<String> content;
    private InternalizationService manager;

    LeaderboardEn(String... content) {
        this.content = Arrays.asList(content);
    }

    @Override
    public Locale getLanguage() {
        return new Locale("en", "US");
    }

    @Override
    public Translation get() {
        return new Translation(this, this.name(), this.content);
    }

    @Override
    public List<Translation> translations() {
        return Arrays.stream(values()).map(LeaderboardEn::get).collect(Collectors.toList());
    }

    @Override
    public void setManager(InternalizationService manager) {
        this.manager = manager;
    }
}
