package com.cronos.practice.leaderboard.command;

import com.cronos.bukkit.commands.BukkitCommand;
import com.cronos.commons.commands.CommandResult;
import com.cronos.practice.leaderboard.Leaderboard;
import com.cronos.practice.leaderboard.internalization.LeaderboardEn;
import com.cronos.practice.leaderboard.service.ContentService;
import com.cronos.practice.leaderboard.service.HologramService;
import org.bukkit.command.CommandSender;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class RefreshCommand extends BukkitCommand {

    private final ContentService contentService;
    private final HologramService hologramService;

    public RefreshCommand(Leaderboard plugin) {
        super(plugin, "refresh");
        this.contentService = plugin.getContentService();
        this.hologramService = plugin.getHologramService();

        setDescription("Refresh leaderboard.");
        setPermission("leaderboard.refresh");
    }

    @Override
    protected CommandResult execute(CommandSender commandSender, Object[] objects) {
        contentService.update();
        hologramService.reload();
       // message(commandSender, LeaderboardEn.REFRESHED);
        return CommandResult.SUCCESS;
    }
}
