package com.cronos.practice.bukkit.model.scoreboard;

import com.cronos.practice.bukkit.model.scoreboard.content.*;
import com.cronos.practice.bukkit.model.scoreboard.content.spectator.SpectatorBoxingScoreboardContent;
import com.cronos.practice.bukkit.model.scoreboard.content.spectator.SpectatorPlayingScoreboardContent;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@RequiredArgsConstructor
@Getter
public enum Scoreboards {
    LOBBY(new LobbyScoreboardContent()),
    QUEUE(new QueueScoreboardContent()),
    WAITING(new WaitingScoreboardContent()),
    STARTING(new StartingScoreboardContent()),
    PLAYING(new PlayingScoreboardContent()),
    CELEBRATING(new CelebratingScoreboardContent()),
    BOXING(new BoxingScoreboardContent()),

    /* Spectators */
    SPECTATOR_PLAYING(new SpectatorPlayingScoreboardContent()),
    SPECTATOR_BOXING(new SpectatorBoxingScoreboardContent())
    ;

    private final ScoreboardContent content;
}
