package com.cronos.practice.bukkit.commands.misc;

import com.cronos.bukkit.commands.BukkitCommand;
import com.cronos.commons.commands.CommandResult;
import com.cronos.commons.commands.SenderType;
import com.cronos.commons.internalization.integrated.CommandsEn;
import com.cronos.practice.bukkit.Practice;
import com.cronos.practice.bukkit.internalization.PracticeEn;
import com.cronos.practice.bukkit.service.HistoryService;
import com.cronos.practice.bukkit.ui.game.MatchHistoryUI;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ViewHistoryCommand extends BukkitCommand {

    private final HistoryService service;

    public ViewHistoryCommand(Practice plugin) {
        super(plugin, "vgh");
        this.service = Practice.getBootstrap().getHistoryService();

        setAllowedSenderType(SenderType.PLAYER);
        setDescription("View match history.");

        addRequiredArgument("player", String.class);
        addRequiredArgument("game", String.class);
    }

    @Override
    protected CommandResult execute(CommandSender commandSender, Object[] objects) {
        getArgument(objects, 0, String.class)
                .ifPresentOrElse(playerName -> {
                    UUID playerId = Bukkit.getOfflinePlayer(playerName).getUniqueId();
                    getArgument(objects, 1, String.class)
                            .ifPresentOrElse(rawHistoryId -> {
                                UUID historyId = UUID.fromString(rawHistoryId);
                                service.getByIdAndPlayer(historyId, playerId)
                                        .ifPresentOrElse(matchEntry -> openUI((Player) commandSender, MatchHistoryUI.class, matchEntry, service.getById(historyId).orElse(null)),
                                                () -> message(commandSender, PracticeEn.GAME_RESULT_NOT_FOUND));
                            }, () -> message(commandSender, CommandsEn.COMMAND_INVALID_SYNTAX, "syntax", getSyntax()));
                }, () -> message(commandSender, CommandsEn.COMMAND_INVALID_SYNTAX, "syntax", getSyntax()));
        return CommandResult.SUCCESS;
    }
}
