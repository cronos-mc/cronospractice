package com.cronos.practice.bukkit.runnable.game;

import com.cronos.bukkit.utils.xseries.XSound;
import com.cronos.core.commons.utils.TimeFormatter;
import com.cronos.practice.bukkit.internalization.PracticeEn;
import com.cronos.practice.bukkit.model.arena.SpawnLocation;
import com.cronos.practice.bukkit.model.game.GameState;
import com.cronos.practice.bukkit.model.game.PracticeGame;
import com.cronos.practice.bukkit.model.game.SoloGame;
import com.cronos.practice.bukkit.model.game.TeamGame;
import com.cronos.practice.bukkit.model.kit.PracticeKit;
import com.cronos.practice.bukkit.model.kit.SpecialKit;
import com.cronos.practice.bukkit.model.player.PlayerState;
import com.cronos.practice.bukkit.utils.PlayerUtils;
import org.bukkit.GameMode;
import org.bukkit.World;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class GameStartRunnable extends GameRunnable {

    public GameStartRunnable(PracticeGame<?> game, int runningTime) {
        super(game, runningTime);
        getPlayers().forEach(player -> player.setState(PlayerState.ALIVE));
        getBukkitPlayers().forEach(player -> {
            player.setGameMode(GameMode.ADVENTURE);
            player.getInventory().clear();
            player.setHealth(player.getMaxHealth());
            player.setFoodLevel(20);
            if (game.getKit().getSpecialKit() == SpecialKit.BOXING || game.getKit().getSpecialKit() == SpecialKit.SUMO)
                player.setSaturation(1000f);
            else
                player.setSaturation(12f);
        });
        getPlayers().forEach(player -> {
            int ping = player.getAsPlayer()
                    .map(PlayerUtils::getPing)
                    .orElse(0);
            StringBuilder opponents = new StringBuilder();
            if (game instanceof TeamGame)
                ((TeamGame) game).getParticipants()
                        .stream()
                        .filter(practiceTeam -> practiceTeam.getMembers().stream().noneMatch(player1 -> player1.getUniqueId().equals(player.getUniqueId())))
                        .forEach(practiceTeam -> practiceTeam.getMembers()
                                .forEach(player1 -> opponents.append(player1.getName()).append(" ")));
            else
                ((SoloGame) game).getParticipants()
                        .stream()
                        .filter(player1 -> !player1.getUniqueId().equals(player.getUniqueId()))
                        .forEach(player1 -> opponents.append(player1.getName()).append(" "));
            player.message(PracticeEn.GAME_INFORMATION, "kit", game.getKit().getDisplayName(),
                    "map", game.getArena().getName(),
                    "ping", ping,
                    "opponent", opponents.toString());
        });
        teleportPlayers();
        giveKits();
    }

    @Override
    public void onTick(int time) {
        if (time % 10 == 0 || time < 10) {
            game.message(PracticeEn.GAME_START_TIMER, "time", TimeFormatter.formatDuration(time));
            game.playSound(XSound.ENTITY_EXPERIENCE_ORB_PICKUP);
        }
    }

    @Override
    public void onComplete() {
        game.playSound(XSound.ENTITY_DRAGON_FIREBALL_EXPLODE);
        game.message(PracticeEn.GAME_STARTED);
        game.setState(GameState.PLAYING);
    }

    private void giveKits() {
        PracticeKit kit = game.getKit();
        getBukkitPlayers()
                .forEach(player -> {
                    player.setGameMode(GameMode.SURVIVAL);
                    player.getInventory().clear();
                    kit.getContent()
                            .forEach(kitItem -> kitItem.getSlot().forEach(integer -> player.getInventory().setItem(integer, kitItem.getItem())));
                });
    }

    private void teleportPlayers() {
        Deque<SpawnLocation> locations = new LinkedList<>(game.getArena().getSpawnLocations());
        World world = game.getArena().getWorld();
        SpawnLocation defaultLoc = locations.getLast();
        getPlayers().forEach(player -> player.getAsPlayer()
                .ifPresent(player1 -> {
                    SpawnLocation location = locations.poll();
                    if (Objects.isNull(location))
                        location = defaultLoc;
                    player1.teleport(location.toLocation(world));
                }));
    }
}
