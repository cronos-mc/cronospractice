package com.cronos.practice.bukkit.utils;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class EloUtils {

    public static int getWinElo(int elo) {
        return elo / 100 * 3;
    }

    public static int getLoseElo(int elo) {
        return getWinElo(elo) / 2;
    }
}
