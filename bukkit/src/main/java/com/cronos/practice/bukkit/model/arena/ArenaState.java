package com.cronos.practice.bukkit.model.arena;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public enum ArenaState {
    INITIALIZING,
    AVAILABLE,
    BUSY,
    RESETTING,
    INITIALIZATION_FAILED
}
