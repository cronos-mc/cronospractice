package com.cronos.practice.bukkit.service;

import com.cronos.core.commons.players.PlayerProfile;
import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.model.player.PracticePlayer;
import com.cronos.practice.bukkit.repository.PlayerRepository;
import lombok.NonNull;
import org.bukkit.OfflinePlayer;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class PlayerService {

    private final PlayerRepository repository;

    public PlayerService(Bootstrap bootstrap) {
        this.repository = new PlayerRepository();
    }

    public void add(@NonNull PracticePlayer player) {
        getPlayers().removeIf(player1 -> player1.equals(player));
        this.repository.add(player);
    }

    public void remove(@NonNull PracticePlayer player) {
        this.repository.remove(player);
    }

    public Optional<PracticePlayer> getByProfile(@NonNull PlayerProfile profile) {
        return this.repository.getByProfile(profile);
    }

    public Optional<PracticePlayer> getByPlayer(@NonNull OfflinePlayer player) {
        return this.repository.getByPlayer(player);
    }

    public Optional<PracticePlayer> getByUniqueId(@NonNull UUID uniqueId) {
        return this.repository.getByUniqueId(uniqueId);
    }

    public List<PracticePlayer> getPlayers() {
        return this.repository.getPlayers();
    }
}
