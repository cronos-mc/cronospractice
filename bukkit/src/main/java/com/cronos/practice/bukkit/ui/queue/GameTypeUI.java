package com.cronos.practice.bukkit.ui.queue;

import com.cronos.bukkit.ui.CronosUI;
import com.cronos.bukkit.utils.items.ItemBuilder;
import com.cronos.bukkit.utils.xseries.XMaterial;
import com.cronos.practice.bukkit.Practice;
import com.cronos.practice.bukkit.service.GameService;
import com.cronos.practice.bukkit.service.QueueService;
import com.cronos.practice.bukkit.model.game.GameType;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class GameTypeUI extends CronosUI {

    private final QueueService service;
    private final GameService gameService;

    public GameTypeUI(Practice plugin) {
        super(plugin);
        this.service = Practice.getBootstrap().getQueueService();
        this.gameService = Practice.getBootstrap().getGameService();
        createInventory("&f» &6Select a game type", 45);
    }

    @Override
    public void open(Player player, Object[] objects) {

        addItem(21, new ItemBuilder(Material.DIAMOND_SWORD)
                .setName("&f&lRanked")
                .addFlags(ItemFlag.values())
                .setLore("&r", "&6In queue", "&f&l" + service.countByType(GameType.RANKED),
                        "&r", "&6Playing", "&f&l" + gameService.getByType(GameType.RANKED),
                        "&r", "&6&lClick&7 to open kit selection."))
                .setClickAction(e -> openUI(player, KitSelectionUI.class, GameType.RANKED));

        addItem(23, new ItemBuilder(Material.WOOD_AXE)
                .setName("&f&lUn-Ranked")
                .addFlags(ItemFlag.values())
                .setLore("&r", "&6In queue", "&f&l" + service.countByType(GameType.UNRANKED),
                        "&r", "&6Playing", "&f&l" + gameService.getByType(GameType.UNRANKED),
                        "&r", "&6&lClick&7 to open kit selection."))
                .setClickAction(e -> openUI(player, KitSelectionUI.class, GameType.UNRANKED));

        /* Fillers */
        fillBorders(new ItemBuilder(XMaterial.WHITE_STAINED_GLASS_PANE.parseItem()).setName("&r"));
    }
}
