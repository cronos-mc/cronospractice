package com.cronos.practice.bukkit.ui.selector;

import com.cronos.bukkit.utils.items.ItemBuilder;
import com.cronos.practice.bukkit.Practice;
import com.cronos.practice.bukkit.model.kit.PracticeKit;
import lombok.NonNull;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class KitSelectorUI extends SingleSelectorUI<PracticeKit> {

    public KitSelectorUI(@NonNull Practice plugin) {
        super(plugin);
        createInventory("&f» &6&lSelect a kit", 54);

        this.buttonProvider = kit -> new ItemBuilder(kit.getIcon())
                .addLoreLine("&r", "&6&lClick&7 to select");
        this.objects.addAll(Practice.getBootstrap().getKitService().getKits());
    }
}
