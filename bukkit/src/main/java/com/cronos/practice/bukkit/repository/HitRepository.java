package com.cronos.practice.bukkit.repository;

import com.cronos.commons.utils.Optional;
import com.cronos.practice.bukkit.model.hit.HitRecord;
import lombok.Data;
import lombok.NonNull;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class HitRepository {

    private final List<HitRecord> records = new ArrayList<>();

    public void add(@NonNull HitRecord record) {
        this.records.add(record);
    }

    public void remove(@NonNull HitRecord record) {
        this.records.remove(record);
    }

    public Optional<HitRecord> getByPlayer(@NonNull UUID id) {
        return new Optional<>(this.records
                .stream()
                .filter(record -> record.getId().equals(id))
                .findFirst());
    }

    public Optional<HitRecord> getByPlayer(@NonNull Player player) {
        return getByPlayer(player.getUniqueId());
    }
}
