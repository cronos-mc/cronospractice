package com.cronos.practice.bukkit.service;

import com.cronos.commons.utils.Optional;
import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.model.scoreboard.PracticeScoreboard;
import com.cronos.practice.bukkit.repository.ScoreboardRepository;
import com.cronos.practice.bukkit.runnable.ScoreboardUpdateRunnable;
import lombok.Data;
import lombok.NonNull;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class ScoreboardService {

    private final ScoreboardRepository repository;

    public ScoreboardService(Bootstrap bootstrap) {
        this.repository = new ScoreboardRepository();
        bootstrap.getPlugin()
                .getTaskService()
                .getFactory()
                .newRepeatingTask(new ScoreboardUpdateRunnable(this), 0);
    }

    public void add(@NonNull PracticeScoreboard scoreboard) {
        this.repository.add(scoreboard);
    }

    public void remove(@NonNull PracticeScoreboard scoreboard) {
        this.repository.remove(scoreboard);
    }

    public Optional<PracticeScoreboard> getByPlayer(@NonNull Player player) {
        return this.repository.getByPlayer(player);
    }

    public Optional<PracticeScoreboard> getByPlayer(@NonNull UUID uuid) {
        return this.repository.getByPlayer(uuid);
    }

    public List<PracticeScoreboard> getScoreboards() {
        return this.repository.getScoreboards();
    }
}
