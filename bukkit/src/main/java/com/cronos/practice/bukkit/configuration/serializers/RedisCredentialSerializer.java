package com.cronos.practice.bukkit.configuration.serializers;

import com.cronos.bukkit.configuration.serializers.YmlSerializer;
import com.cronos.commons.exceptions.YmlDeserializationException;
import com.cronos.commons.exceptions.YmlSerializationException;
import com.cronos.commons.utils.Optional;
import com.cronos.messaging.redis.RedisCredentials;
import lombok.NonNull;
import org.bukkit.configuration.ConfigurationSection;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class RedisCredentialSerializer implements YmlSerializer<RedisCredentials> {

    @Override
    public Optional<RedisCredentials> deserialize(@NonNull ConfigurationSection section) throws YmlDeserializationException {
        String hostname = section.getString("host", "localhost");
        int port = section.getInt("port", 6379);
        String password = section.getString("password");
        boolean ssl = section.getBoolean("ssl");
        return Optional.of(new RedisCredentials(hostname, port, password, ssl));
    }

    @Override
    public void serialize(ConfigurationSection section, @NonNull RedisCredentials redisCredentials) throws YmlSerializationException {

    }
}
