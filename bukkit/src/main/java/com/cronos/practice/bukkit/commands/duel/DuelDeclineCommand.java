package com.cronos.practice.bukkit.commands.duel;

import com.cronos.bukkit.commands.BukkitCommand;
import com.cronos.commons.commands.CommandResult;
import com.cronos.commons.commands.SenderType;
import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.internalization.PracticeEn;
import com.cronos.practice.bukkit.service.DuelService;
import lombok.NonNull;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class DuelDeclineCommand extends BukkitCommand {

    private final DuelService service;

    public DuelDeclineCommand(@NonNull Bootstrap bootstrap) {
        super(bootstrap.getPlugin(), "decline", "deny");
        this.service = bootstrap.getDuelService();

        setDescription("Decline duel invitation.");
        setAllowedSenderType(SenderType.PLAYER);

        addOptionalArgument("player", Player.class);
    }

    @Override
    protected CommandResult execute(CommandSender commandSender, Object[] objects) {
        getArgument(objects, Player.class)
                .ifPresent(target -> {
                    Player player = (Player) commandSender;
                    service.getRequest(target.getUniqueId(), player.getUniqueId())
                            .ifPresentOrElse(request -> {
                                message(commandSender, PracticeEn.DUEL_REQUEST_DENIED, "name", target.getName());
                                message(target, PracticeEn.DUEL_REQUEST_DENIED_, "name", player.getName());
                                service.remove(request);
                            }, () -> message(commandSender, PracticeEn.NO_DUEL_REQUEST, "name", target.getName()));
                });
        return CommandResult.SUCCESS;
    }
}
