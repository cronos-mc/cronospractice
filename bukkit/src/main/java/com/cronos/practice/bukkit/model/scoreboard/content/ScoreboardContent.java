package com.cronos.practice.bukkit.model.scoreboard.content;

import com.cronos.bukkit.utils.BukkitUtil;
import com.cronos.practice.bukkit.Practice;
import lombok.NonNull;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public abstract class ScoreboardContent extends BukkitUtil {

    protected ScoreboardContent() {
        super(Practice.getPlugin(Practice.class));
    }

    public String getTitle(@NonNull Player player) {
        return Practice.getBootstrap().getConfiguration().getServerName();
    }

    public abstract List<String> getLines(@NonNull Player player);
}
