package com.cronos.practice.bukkit.loaders;

import com.cronos.commons.configuration.YamlFileLoadException;
import com.cronos.commons.tasks.startup.PostLoadTask;
import com.cronos.commons.utils.Optional;
import com.cronos.practice.bukkit.Practice;
import com.cronos.practice.bukkit.configuration.KitConfiguration;
import com.cronos.practice.bukkit.service.KitService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@RequiredArgsConstructor
@Slf4j(topic = "Kit Loader")
public class KitLoader implements PostLoadTask {

    private final Practice plugin;
    private final KitService service;

    @Override
    public void run() {
        log.info("Loading kits...");
        File worldsFolder = new File(plugin.getDataFolder(), "kits");
        if (!worldsFolder.exists())
            if (!worldsFolder.mkdir())
                log.error("Cannot create 'kits' folder!");
        if (!service.getDataFolder().exists())
            if (!service.getDataFolder().mkdir())
                log.error("Cannot create 'kits' folder!");

        service.getKits().clear();
        service.getKits().addAll(Arrays.stream(Objects.requireNonNull(service.getDataFolder().listFiles()))
                .filter(file -> file.getName().contains(".yml"))
                .map(file -> new KitConfiguration(plugin, file))
                .peek(kitConfiguration -> {
                    try {
                        kitConfiguration.load();
                    } catch (YamlFileLoadException e) {
                        log.error("Cannot load kit from file '{}'!", kitConfiguration.getFile().getName());
                        e.printStackTrace();
                    }
                })
                .map(KitConfiguration::parse)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList()));
        log.info("{} kits loaded!", service.getKits().size());
    }
}
