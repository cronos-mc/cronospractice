package com.cronos.practice.bukkit.service;

import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.model.cooldown.CooldownType;
import com.cronos.practice.bukkit.model.cooldown.PracticeCooldown;
import com.cronos.practice.bukkit.repository.CooldownRepository;
import com.cronos.practice.bukkit.runnable.CooldownUpdateRunnable;
import lombok.NonNull;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class CooldownService {

    private final CooldownRepository repository;

    public CooldownService(Bootstrap bootstrap) {
        this.repository = new CooldownRepository();

        new CooldownUpdateRunnable(bootstrap, this)
                .runTaskTimer(bootstrap.getPlugin(), 0, 1);
    }

    public void add(@NonNull PracticeCooldown cooldown) {
        this.repository.add(cooldown);
    }

    public void remove(@NonNull PracticeCooldown cooldown) {
        this.repository.remove(cooldown);
    }

    public void remove(@NonNull Player player) {
        getCooldowns().removeIf(cooldown -> player.getUniqueId().equals(cooldown.getPlayerId()));
    }

    public boolean isInCooldown(@NonNull Player player, @NonNull CooldownType type) {
        return get(player, type)
                .filter(practiceCooldown -> !practiceCooldown.isExpired())
                .isPresent();
    }

    public Optional<PracticeCooldown> get(@NonNull Player player, @NonNull CooldownType type) {
        return this.repository.get(player.getUniqueId(), type);
    }

    public List<PracticeCooldown> get(@NonNull Player player) {
        return this.repository.get(player.getUniqueId());
    }

    public List<PracticeCooldown> getCooldowns() {
        return this.repository.getCooldowns();
    }
}
