package com.cronos.practice.bukkit.event.game;

import com.cronos.practice.bukkit.model.game.PracticeGame;
import com.cronos.practice.bukkit.model.game.SoloGame;
import com.cronos.practice.bukkit.model.game.TeamGame;
import com.cronos.practice.bukkit.model.player.PracticePlayer;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class GameEvent extends Event {

    private static final HandlerList HANDLERS_LIST = new HandlerList();
    private final PracticeGame<?> game;
    private final List<Player> players;

    public GameEvent(PracticeGame<?> game) {
        this.game = game;
        this.players = new ArrayList<>();
        if (game instanceof TeamGame)
            ((TeamGame) game).getParticipants()
                    .forEach(practiceTeam -> practiceTeam.getMembers()
                            .stream()
                            .map(PracticePlayer::getAsPlayer)
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .filter(Player::isOnline)
                            .forEach(this.players::add));
        else
            ((SoloGame) game).getParticipants()
                    .stream()
                    .map(PracticePlayer::getAsPlayer)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .filter(Player::isOnline)
                    .forEach(this.players::add);

    }

    public List<Player> getSpectators() {
        return game.getSpectators()
                .stream()
                .map(PracticePlayer::getAsPlayer)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .filter(Player::isOnline)
                .collect(Collectors.toList());
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLERS_LIST;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS_LIST;
    }
}
