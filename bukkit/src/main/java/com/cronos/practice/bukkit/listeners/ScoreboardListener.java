package com.cronos.practice.bukkit.listeners;

import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.event.game.GameStateChangeEvent;
import com.cronos.practice.bukkit.event.queue.QueueJoinEvent;
import com.cronos.practice.bukkit.event.queue.QueueQuitEvent;
import com.cronos.practice.bukkit.model.game.GameState;
import com.cronos.practice.bukkit.model.game.PracticeGame;
import com.cronos.practice.bukkit.model.kit.SpecialKit;
import com.cronos.practice.bukkit.model.scoreboard.PracticeScoreboard;
import com.cronos.practice.bukkit.model.scoreboard.Scoreboards;
import com.cronos.practice.bukkit.service.ScoreboardService;
import lombok.NonNull;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ScoreboardListener implements Listener {

    private final ScoreboardService service;

    public ScoreboardListener(Bootstrap bootstrap) {
        this.service = bootstrap.getScoreboardService();
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        service.getByPlayer(e.getPlayer())
                .ifPresentOrElse(fastBoard -> {
                }, () -> service.add(new PracticeScoreboard(e.getPlayer())));
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        service.getByPlayer(e.getPlayer())
                .ifPresent(scoreboard -> {
                    service.remove(scoreboard);
                    scoreboard.getFastBoard().delete();
                });
    }

    @EventHandler
    public void onQueueJoin(@NonNull QueueJoinEvent e) {
        e.getPlayers().forEach(player -> service.getByPlayer(player)
                .ifPresent(scoreboard -> scoreboard.setContent(Scoreboards.QUEUE)));
    }

    @EventHandler
    public void onQueueQuit(@NonNull QueueQuitEvent e) {
        e.getPlayers().forEach(player -> service.getByPlayer(player)
                .ifPresent(scoreboard -> {
                    if (e.getReason().equals(QueueQuitEvent.Reason.MANUAL))
                        scoreboard.setContent(Scoreboards.LOBBY);
                    else if (e.getReason().equals(QueueQuitEvent.Reason.GAME_JOIN))
                        scoreboard.setContent(Scoreboards.WAITING);
                }));
    }

    @EventHandler
    public void onGameStateChange(GameStateChangeEvent e) {
        e.getSpectators()
                .forEach(player -> updateScoreboard(player, e.getGame(), e.getNewState(), true));
        e.getPlayers()
                .forEach(player -> updateScoreboard(player, e.getGame(), e.getNewState(), false));
    }

    private void updateScoreboard(@NonNull Player player, @NonNull PracticeGame<?> game, @NonNull GameState state, boolean spectator) {
        service.getByPlayer(player)
                .ifPresent(scoreboard -> {
                    switch (state) {
                        case WAITING:
                            scoreboard.setContent(Scoreboards.WAITING);
                            break;
                        case STARTING:
                            scoreboard.setContent(Scoreboards.STARTING);
                            break;
                        case PLAYING:
                            if (spectator) {
                                if (game.getKit().getSpecialKit().equals(SpecialKit.BOXING))
                                    scoreboard.setContent(Scoreboards.SPECTATOR_BOXING);
                                else
                                    scoreboard.setContent(Scoreboards.SPECTATOR_PLAYING);
                            } else
                                scoreboard.setContent(Scoreboards.PLAYING);
                            break;
                        case CELEBRATING:
                            scoreboard.setContent(Scoreboards.CELEBRATING);
                            break;
                        case ENDED:
                            scoreboard.setContent(Scoreboards.LOBBY);
                            break;
                    }
                });
    }
}
