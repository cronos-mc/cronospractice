package com.cronos.practice.bukkit.model.history;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public enum ItemStackType {
    NORMAL,
    ARMOR
}
