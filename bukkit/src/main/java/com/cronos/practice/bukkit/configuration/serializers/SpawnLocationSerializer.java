package com.cronos.practice.bukkit.configuration.serializers;

import com.cronos.commons.exceptions.YmlDeserializationException;
import com.cronos.commons.exceptions.YmlSerializationException;
import com.cronos.commons.utils.Optional;
import com.cronos.practice.bukkit.model.arena.SpawnLocation;
import lombok.NonNull;
import org.bukkit.configuration.ConfigurationSection;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class SpawnLocationSerializer extends PracticeSerializer<SpawnLocation> {

    @Override
    public Optional<SpawnLocation> deserialize(@NonNull ConfigurationSection section) throws YmlDeserializationException {
        if (!exists("x", section)
                || !exists("y", section)
                || !exists("z", section))
            return Optional.empty();
        double x = section.getDouble("x");
        double y = section.getDouble("y");
        double z = section.getDouble("z");
        float yaw = section.getLong("yaw", 0);
        float pitch = section.getLong("pitch", 0);
        return Optional.of(new SpawnLocation(x, y, z, yaw, pitch));
    }

    @Override
    public void serialize(ConfigurationSection section, @NonNull SpawnLocation spawnLocation) throws YmlSerializationException {
        section.set("x", spawnLocation.getX());
        section.set("y", spawnLocation.getY());
        section.set("z", spawnLocation.getZ());
        section.set("yaw", spawnLocation.getYaw());
        section.set("pitch", spawnLocation.getPitch());
    }
}
