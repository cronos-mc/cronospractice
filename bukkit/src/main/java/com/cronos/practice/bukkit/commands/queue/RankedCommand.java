package com.cronos.practice.bukkit.commands.queue;

import com.cronos.bukkit.commands.BukkitCommand;
import com.cronos.commons.commands.CommandResult;
import com.cronos.commons.commands.SenderType;
import com.cronos.practice.bukkit.Practice;
import com.cronos.practice.bukkit.ui.queue.KitSelectionUI;
import com.cronos.practice.bukkit.model.game.GameType;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class RankedCommand extends BukkitCommand {

    public RankedCommand(Practice plugin) {
        super(plugin, "ranked");

        setDescription("Open ranked queue ui.");
        setAllowedSenderType(SenderType.PLAYER);
    }

    @Override
    protected CommandResult execute(CommandSender commandSender, Object[] objects) {
        openUI((Player) commandSender, KitSelectionUI.class, GameType.RANKED);
        return CommandResult.SUCCESS;
    }
}
