package com.cronos.practice.bukkit.commands.queue;

import com.cronos.bukkit.commands.BukkitCommand;
import com.cronos.commons.commands.CommandResult;
import com.cronos.commons.commands.SenderType;
import com.cronos.practice.bukkit.Practice;
import com.cronos.practice.bukkit.ui.queue.GameTypeUI;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class QueueCommand extends BukkitCommand {

    public QueueCommand(Practice plugin) {
        super(plugin, "queue");

        setDescription("Open queue ui.");
        setAllowedSenderType(SenderType.PLAYER);

        addSubCommand(new QueueLeaveCommand(plugin));
    }

    @Override
    protected CommandResult execute(CommandSender commandSender, Object[] objects) {
        openUI((Player) commandSender, GameTypeUI.class);
        return CommandResult.SUCCESS;
    }
}
