package com.cronos.practice.bukkit.model.winner;

import com.cronos.practice.bukkit.model.game.TeamGame;
import com.cronos.practice.bukkit.model.player.PlayerState;
import com.cronos.practice.bukkit.model.team.PracticeTeam;
import lombok.NonNull;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class TeamWinnerSelector implements WinnerSelector<PracticeTeam, TeamGame> {

    @Override
    public Optional<PracticeTeam> getWinner(@NonNull TeamGame game) {
        List<PracticeTeam> remaining = game.getParticipants()
                .stream()
                .filter(practiceTeam -> practiceTeam.getMembers()
                        .stream()
                        .anyMatch(player -> player.getState().equals(PlayerState.ALIVE)))
                .collect(Collectors.toList());
        if (remaining.size() > 1)
            return Optional.empty();
        return Optional.of(remaining.get(0));
    }
}
