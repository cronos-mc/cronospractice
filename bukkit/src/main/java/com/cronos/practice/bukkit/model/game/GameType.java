package com.cronos.practice.bukkit.model.game;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public enum GameType {
    RANKED,
    UNRANKED
}
