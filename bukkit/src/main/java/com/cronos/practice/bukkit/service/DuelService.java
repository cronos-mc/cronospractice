package com.cronos.practice.bukkit.service;

import com.cronos.commons.utils.Optional;
import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.model.duel.DuelQueue;
import com.cronos.practice.bukkit.model.duel.DuelRequest;
import com.cronos.practice.bukkit.repository.DuelRepository;
import lombok.NonNull;

import java.util.List;
import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class DuelService {

    private final DuelRepository repository;
    private final QueueService queueService;

    public DuelService(@NonNull Bootstrap bootstrap) {
        this.repository = new DuelRepository();
        this.queueService = bootstrap.getQueueService();
    }

    public void add(@NonNull DuelRequest request) {
        this.repository.add(request);
    }

    public void add(@NonNull DuelQueue duelQueue) {
        this.queueService.add(duelQueue);
    }

    public void remove(@NonNull DuelRequest request) {
        this.repository.remove(request);
    }

    public void remove(@NonNull DuelQueue duelQueue) {
        this.queueService.add(duelQueue);
    }

    public Optional<DuelRequest> getRequest(@NonNull UUID requester, @NonNull UUID target) {
        return this.repository.getRequest(requester, target);
    }

    public List<DuelRequest> getRequests() {
        return repository.getRequests();
    }
}
