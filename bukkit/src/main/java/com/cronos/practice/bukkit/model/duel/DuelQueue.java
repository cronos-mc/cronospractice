package com.cronos.practice.bukkit.model.duel;

import com.cronos.practice.bukkit.model.game.GameType;
import com.cronos.practice.bukkit.model.kit.PracticeKit;
import com.cronos.practice.bukkit.model.queue.QueueElement;
import lombok.Getter;
import lombok.NonNull;
import org.bukkit.entity.Player;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class DuelQueue extends QueueElement {

    private final Player player1;
    private final Player player2;

    public DuelQueue(@NonNull Player player1, @NonNull Player player2, @NonNull PracticeKit kit) {
        super(player1.getUniqueId(), false, kit, GameType.UNRANKED);
        this.player1 = player1;
        this.player2 = player2;
    }
}
