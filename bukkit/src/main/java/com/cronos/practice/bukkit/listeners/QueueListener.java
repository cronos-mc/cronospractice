package com.cronos.practice.bukkit.listeners;

import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.event.queue.QueueQuitEvent;
import com.cronos.practice.bukkit.internalization.PracticeEn;
import com.cronos.practice.bukkit.service.QueueService;
import lombok.NonNull;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class QueueListener implements Listener {

    private final QueueService service;

    public QueueListener(@NonNull Bootstrap bootstrap) {
        this.service = bootstrap.getQueueService();
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerQuit(PlayerQuitEvent e) {
        service.getByPlayer(e.getPlayer())
                .ifPresent(element -> {
                    element.message(PracticeEn.QUEUE_LEFT, "kit", element.getKit().getDisplayName());
                    service.remove(element, QueueQuitEvent.Reason.DISCONNECTION);
                });
    }
}
