package com.cronos.practice.bukkit.model.kit;

import lombok.Data;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class KitItem {

    private final List<Integer> slot;
    private final ItemStack item;
}
