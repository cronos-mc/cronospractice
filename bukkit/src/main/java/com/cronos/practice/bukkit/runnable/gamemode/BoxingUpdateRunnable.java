package com.cronos.practice.bukkit.runnable.gamemode;

import com.cronos.commons.utils.Optional;
import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.model.game.GameState;
import com.cronos.practice.bukkit.model.game.SoloGame;
import com.cronos.practice.bukkit.model.history.MatchEntry;
import com.cronos.practice.bukkit.model.hit.HitRecord;
import com.cronos.practice.bukkit.model.kit.SpecialKit;
import com.cronos.practice.bukkit.model.player.PlayerState;
import com.cronos.practice.bukkit.model.player.PracticePlayer;
import com.cronos.practice.bukkit.model.scoreboard.Scoreboards;
import com.cronos.practice.bukkit.service.GameService;
import com.cronos.practice.bukkit.service.HistoryService;
import com.cronos.practice.bukkit.service.HitService;
import com.cronos.practice.bukkit.service.ScoreboardService;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class BoxingUpdateRunnable extends BukkitRunnable {

    private final GameService gameService;
    private final HitService hitService;
    private final ScoreboardService scoreboardService;
    private final HistoryService historyService;

    public BoxingUpdateRunnable(Bootstrap bootstrap) {
        this.gameService = bootstrap.getGameService();
        this.hitService = bootstrap.getHitService();
        this.scoreboardService = bootstrap.getScoreboardService();
        this.historyService = bootstrap.getHistoryService();
    }

    @Override
    public void run() {
        // Scoreboard
        List<SoloGame> games = gameService
                .getGames()
                .stream()
                .filter(game -> game.getKit().getSpecialKit().equals(SpecialKit.BOXING))
                .filter(game -> game.getState().equals(GameState.PLAYING))
                .filter(game -> game instanceof SoloGame)
                .map(game -> (SoloGame) game)
                .collect(Collectors.toList());

        games.forEach(game -> game.getParticipants()
                .stream()
                .map(PracticePlayer::getUniqueId)
                .map(scoreboardService::getByPlayer)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .forEach(scoreboard -> scoreboard.setContent(Scoreboards.BOXING)));

        games.forEach(game -> {
            game.getParticipants()
                    .stream()
                    .filter(player -> hitService.getByPlayer(player.getUniqueId())
                            .map(HitRecord::getHits)
                            .orElse(0) >= 100)
                    .findFirst()
                    .ifPresent(player -> {
                        PracticePlayer practicePlayer = game.getParticipants()
                                .stream()
                                .filter(player1 -> !player1.getUniqueId().equals(player.getUniqueId()))
                                .findFirst()
                                .orElse(null);
                        Player loser = Objects.requireNonNull(practicePlayer)
                                .getAsPlayer()
                                .orElse(null);
                        if (game.getState().equals(GameState.ENDED) || Objects.isNull(loser))
                            return;
                        loser.setHealth(0.1);
                        historyService.getByGame(game)
                                .ifPresent(history -> history.getEntries().add(new MatchEntry(loser)));
                        loser.getInventory().clear();
                        practicePlayer.setState(PlayerState.DEAD);
                    });
        });
    }
}
