package com.cronos.practice.bukkit.event.queue;

import com.cronos.practice.bukkit.model.queue.QueueElement;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class QueueEvent extends Event {

    private static final HandlerList HANDLERS_LIST = new HandlerList();
    private final QueueElement element;
    private final List<Player> players;

    public QueueEvent(QueueElement element) {
        this.element = element;
        if (element.isParty())
            this.players = element.getAsParty()
                    .map(party -> party.getMembers()
                            .stream()
                            .map(Bukkit::getPlayer)
                            .filter(Objects::nonNull)
                            .collect(Collectors.toList()))
                    .orElseGet(ArrayList::new);
        else
            this.players = Collections.singletonList(element.getAsPlayer()
                    .orElse(null));
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLERS_LIST;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS_LIST;
    }

}
