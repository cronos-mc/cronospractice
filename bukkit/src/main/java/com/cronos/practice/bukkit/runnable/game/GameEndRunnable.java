package com.cronos.practice.bukkit.runnable.game;

import com.cronos.commons.utils.Optional;
import com.cronos.core.bukkit.BukkitCore;
import com.cronos.core.commons.players.PlayerProfile;
import com.cronos.core.commons.players.PlayerRepository;
import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.Practice;
import com.cronos.practice.bukkit.configuration.Configuration;
import com.cronos.practice.bukkit.internalization.PracticeEn;
import com.cronos.practice.bukkit.model.game.*;
import com.cronos.practice.bukkit.model.history.MatchEntry;
import com.cronos.practice.bukkit.model.items.PracticeItem;
import com.cronos.practice.bukkit.model.player.PracticePlayer;
import com.cronos.practice.bukkit.model.team.PracticeTeam;
import com.cronos.practice.bukkit.service.BlockService;
import com.cronos.practice.bukkit.service.GameService;
import com.cronos.practice.bukkit.service.HistoryService;
import com.cronos.practice.bukkit.service.ItemService;
import com.cronos.practice.bukkit.utils.EloUtils;
import lombok.NonNull;
import org.bukkit.GameMode;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class GameEndRunnable extends GameRunnable {

    private final List<PracticePlayer> winners = new ArrayList<>();
    private final List<PracticePlayer> losers = new ArrayList<>();
    private final Configuration configuration;
    private final PlayerRepository repository;
    private final ItemService itemService;
    private final GameService gameService;
    private final HistoryService historyRepository;
    private final BlockService blockService;

    public GameEndRunnable(PracticeGame<?> game, int runningTime) {
        super(game, runningTime);
        Bootstrap bootstrap = Practice.getBootstrap();
        this.gameService = bootstrap.getGameService();
        this.configuration = bootstrap.getConfiguration();
        this.repository = BukkitCore.getPlugin(BukkitCore.class).getPlayerRepository();
        this.itemService = bootstrap.getItemService();
        this.historyRepository = bootstrap.getHistoryService();
        this.blockService = bootstrap.getBlockService();

        getBukkitPlayers().forEach(player -> {
            player.setHealth(player.getMaxHealth());
            blockService.clear(player.getUniqueId());
            historyRepository.getByGame(game)
                    .ifPresent(history -> {
                        if (history.getEntries()
                                .stream()
                                .noneMatch(matchEntry -> matchEntry.getUniqueId().equals(player.getUniqueId())))
                            history.getEntries().add(new MatchEntry(player));
                    });
        });
        if (game instanceof TeamGame) {
            PracticeTeam winner = (PracticeTeam) game.getWinner();
            this.winners.addAll(winner.getMembers());
            game.message(PracticeEn.GAME_WON, "winner", winner.getLeader().getName() + "'s team");
            historyRepository.getByGame(game).ifPresent(history -> {
                history.setWinner(winner.getLeader().getName() + "'s team");
                history.setLoser("?");
            });
            ((TeamGame) game).getParticipants()
                    .stream()
                    .filter(gameParticipant -> !gameParticipant.getUniqueId().equals(winner.getUniqueId()))
                    .forEach(practiceTeam -> losers.addAll(practiceTeam.getMembers()));
            return;
        }
        PracticePlayer winner = (PracticePlayer) game.getWinner();
        game.message(PracticeEn.GAME_WON, "winner", winner.getName());
        ((SoloGame) game).getParticipants()
                .stream()
                .filter(gameParticipant -> !gameParticipant.getUniqueId().equals(winner.getUniqueId()))
                .forEach(losers::add);
        historyRepository.getByGame(game).ifPresent(history -> {
            history.setWinner(winner.getName());
            StringBuilder builder = new StringBuilder();
            losers.stream()
                    .map(PracticePlayer::getName)
                    .forEach(s -> builder.append(s).append(" "));
            history.setLoser(builder.toString());
        });
        this.winners.add(winner);
    }

    @Override
    public void onTick(int time) {

    }

    @Override
    public void onComplete() {
        gameService.remove(game);
        getBukkitPlayers().forEach(player -> {
            player.teleport(configuration.getSpawnLocation());
            player.setGameMode(GameMode.SURVIVAL);
            player.setHealth(20d);
            player.setFoodLevel(20);
            player.getInventory().setBoots(null);
            player.getInventory().setChestplate(null);
            player.getInventory().setLeggings(null);
            player.getInventory().setHelmet(null);
            player.getActivePotionEffects()
                    .forEach(potionEffect -> player.removePotionEffect(potionEffect.getType()));
            player.setVelocity(new Vector(0, 0, 0));
            player.setFireTicks(0);
            player.setLevel(0);
            player.setExp(0);
            ((CraftPlayer) player).getHandle().getDataWatcher().watch(9, (byte) 0); // removes arrows in player body
            itemService.give(player, PracticeItem.Type.HUB);
            getBukkitPlayers().forEach(player::showPlayer);
        });
        if (game.getType() == GameType.RANKED) {
            convert(winners).forEach(profile -> {
                profile.setElo(profile.getElo() + EloUtils.getWinElo(profile.getElo()));
                repository.update(profile);
            });
            convert(losers).forEach(profile -> {
                profile.setElo(profile.getElo() - EloUtils.getLoseElo(profile.getElo()));
                repository.update(profile);
            });
        }
        historyRepository.sendMessage(game);
        game.setState(GameState.ENDED);
    }

    private List<PlayerProfile> convert(@NonNull List<PracticePlayer> players) {
        return players.stream()
                .map(player -> repository.getByUniqueId(player.getUniqueId()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

}
