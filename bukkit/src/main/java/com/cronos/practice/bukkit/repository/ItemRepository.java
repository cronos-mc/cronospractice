package com.cronos.practice.bukkit.repository;

import com.cronos.practice.bukkit.model.items.PracticeItem;
import lombok.Getter;
import lombok.NonNull;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ItemRepository {

    @Getter
    private final List<PracticeItem> items = new ArrayList<>();

    public void add(@NonNull PracticeItem item) {
        this.items.add(item);
    }

    public void remove(@NonNull PracticeItem item) {
        this.items.remove(item);
    }

    public Optional<PracticeItem> getByItemStack(@NonNull ItemStack itemStack) {
        return this.items.stream()
                .filter(item -> item.getItemStack().equals(itemStack))
                .findFirst();
    }
}
