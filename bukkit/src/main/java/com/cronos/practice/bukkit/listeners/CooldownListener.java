package com.cronos.practice.bukkit.listeners;

import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.model.cooldown.CooldownType;
import com.cronos.practice.bukkit.model.cooldown.PracticeCooldown;
import com.cronos.practice.bukkit.service.CooldownService;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class CooldownListener implements Listener {

    private final CooldownService service;

    public CooldownListener(Bootstrap bootstrap) {
        this.service = bootstrap.getCooldownService();
    }

    @EventHandler
    public void onEnderPearlThrow(PlayerInteractEvent e) {
        final Player p = e.getPlayer();
        if (Objects.isNull(e.getItem()) || !e.getItem().getType().equals(Material.ENDER_PEARL))
            return;
        if (service.isInCooldown(e.getPlayer(), CooldownType.ENDER_PEARL))
            e.setCancelled(true);
        else
            service.add(new PracticeCooldown(p.getUniqueId(), CooldownType.ENDER_PEARL));
    }
}
