package com.cronos.practice.bukkit.model.arena;

import com.cronos.practice.bukkit.Practice;
import com.cronos.practice.bukkit.model.kit.PracticeKit;
import com.cronos.practice.bukkit.utils.WorldUtils;
import lombok.Data;
import lombok.Setter;
import org.bukkit.World;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.util.List;
import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
@Setter
public class PracticeArena {

    private final String name;
    private final List<PracticeKit> kits;
    private final List<SpawnLocation> spawnLocations;
    private final File originalWorld;

    private World world;
    private ArenaState state = ArenaState.INITIALIZING;

    public void init() {
        this.state = ArenaState.INITIALIZING;
        WorldUtils.createTmpWorld(this)
                .ifPresentOrElse(world1 -> {
                    this.world = world1;
                    this.state = ArenaState.AVAILABLE;
                }, () -> this.state = ArenaState.INITIALIZATION_FAILED);
    }

    public void reset() {
        this.state = ArenaState.RESETTING;
        new BukkitRunnable() {
            @Override
            public void run() {
                WorldUtils.deleteWorld(world);
                world = null;
                WorldUtils.createTmpWorld(PracticeArena.this)
                        .ifPresentOrElse(world1 -> {
                            world = world1;
                            state = ArenaState.AVAILABLE;
                        }, () -> state = ArenaState.INITIALIZATION_FAILED);
            }
        }.runTaskLater(Practice.getPlugin(Practice.class), 45);
    }

    public boolean isValid() {
        return spawnLocations.size() > 1
                && spawnLocations.size() % 2 == 0
                && kits.size() > 0
                && originalWorld.exists()
                && originalWorld.isDirectory();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PracticeArena that = (PracticeArena) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
