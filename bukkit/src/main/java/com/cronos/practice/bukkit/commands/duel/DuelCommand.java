package com.cronos.practice.bukkit.commands.duel;

import com.cronos.bukkit.commands.BukkitCommand;
import com.cronos.commons.commands.CommandResult;
import com.cronos.commons.commands.SenderType;
import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.internalization.PracticeEn;
import com.cronos.practice.bukkit.model.duel.DuelRequest;
import com.cronos.practice.bukkit.model.kit.PracticeKit;
import com.cronos.practice.bukkit.service.DuelService;
import com.cronos.practice.bukkit.ui.selector.KitSelectorUI;
import lombok.NonNull;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.function.Consumer;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class DuelCommand extends BukkitCommand {

    private final DuelService service;

    public DuelCommand(@NonNull Bootstrap bootstrap) {
        super(bootstrap.getPlugin(), "duel");
        this.service = bootstrap.getDuelService();

        setDescription("Duel other players.");
        setAllowedSenderType(SenderType.PLAYER);

        addRequiredArgument("player", Player.class);

        addSubCommands(new DuelAcceptCommand(bootstrap),
                new DuelDeclineCommand(bootstrap));
    }

    @Override
    protected CommandResult execute(CommandSender commandSender, Object[] objects) {
        getArgument(objects, Player.class)
                .ifPresent(target -> {
                    Player player = (Player) commandSender;
                    service.getRequest(player.getUniqueId(), target.getUniqueId())
                            .ifPresentOrElse(request -> message(commandSender, PracticeEn.DUEL_INVITATION_ALREADY_SENT, "name", target.getName()),
                                    () -> {
                                        openUI(player, KitSelectorUI.class, (Consumer<PracticeKit>) practiceKit -> {
                                            service.add(new DuelRequest(player, target, practiceKit));
                                            message(player, PracticeEn.DUEL_INVITATION_SENT, "name", target.getName());
                                            colorAndFormat(PracticeEn.DUEL_REQUEST, "player", player.getName(), "kit", practiceKit.getDisplayName())
                                                    .forEach(s -> {
                                                        if (s.toLowerCase().contains("%actions%")) {
                                                            TextComponent accept = new TextComponent("[ACCEPT]");
                                                            accept.setColor(ChatColor.GREEN);
                                                            accept.setBold(true);
                                                            accept.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/duel accept " + player.getName()));
                                                            TextComponent spacer = new TextComponent(" - ");
                                                            spacer.setColor(ChatColor.DARK_GRAY);
                                                            accept.addExtra(spacer);
                                                            TextComponent deny = new TextComponent("[DECLINE]");
                                                            deny.setColor(ChatColor.RED);
                                                            deny.setBold(true);
                                                            deny.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/duel deny " + player.getName()));
                                                            accept.addExtra(deny);
                                                            target.sendMessage(accept);
                                                            return;
                                                        }
                                                        target.sendMessage(new TextComponent(TextComponent.fromLegacyText(s)));
                                                    });
                                        });
                                    });
                });
        return CommandResult.SUCCESS;
    }
}
