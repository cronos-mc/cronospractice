package com.cronos.practice.bukkit.service;

import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.repository.BlockRepository;
import lombok.NonNull;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class BlockService {

    private final BlockRepository repository;
    private final GameService gameService;

    public BlockService(Bootstrap bootstrap) {
        this.repository = new BlockRepository();
        this.gameService = bootstrap.getGameService();
    }

    public void add(@NonNull UUID uuid, @NonNull Block block) {
        this.repository.add(uuid, block);
    }

    public boolean canBreakBlock(@NonNull Player player, @NonNull Block block) {
        return repository.getBlocks()
                .values()
                .stream()
                .anyMatch(blocks -> blocks.stream()
                        .anyMatch(block1 -> block1.getX() == block.getX()
                                && block1.getY() == block.getY()
                                && block1.getZ() == block.getZ()));
    }

    public boolean canPlaceBlock(@NonNull Player player) {
        return gameService.isInGame(player.getUniqueId());
    }

    public void remove(@NonNull UUID uuid, @NonNull Block block) {
        this.repository.remove(uuid, block);
    }

    public List<Block> get(@NonNull UUID uuid) {
        return this.repository.get(uuid);
    }

    public void clear(@NonNull UUID uuid) {
        this.repository.clear(uuid);
    }

    public Map<UUID, List<Block>> getBlocks() {
        return this.repository.getBlocks();
    }
}
