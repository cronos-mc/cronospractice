package com.cronos.practice.bukkit.model.gamemode;

import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.model.kit.SpecialKit;
import lombok.NonNull;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface PracticeGameMode {

    SpecialKit getKit();

    void init(@NonNull Bootstrap bootstrap);

}
