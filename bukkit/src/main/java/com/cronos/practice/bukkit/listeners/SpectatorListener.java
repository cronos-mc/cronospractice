package com.cronos.practice.bukkit.listeners;

import com.cronos.commons.utils.Optional;
import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.model.game.PracticeGame;
import com.cronos.practice.bukkit.service.GameService;
import com.cronos.practice.bukkit.service.PlayerService;
import lombok.NonNull;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class SpectatorListener implements Listener {

    private final GameService gameService;
    private final PlayerService playerService;

    public SpectatorListener(@NonNull Bootstrap bootstrap) {
        this.gameService = bootstrap.getGameService();
        this.playerService = bootstrap.getPlayerService();
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof Player && isSpectator((Player) e.getEntity()))
            e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerDamage(EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof Player
                && e.getDamager() instanceof Player
                && isSpectator((Player) e.getDamager()))
            e.setCancelled(true);
    }

    @EventHandler
    public void onTeleport(PlayerTeleportEvent e) {
        getGame(e.getPlayer())
                .ifPresent(game -> {
                    if (Objects.nonNull(e.getTo()) && e.getTo().getWorld() != game.getArena().getWorld())
                        playerService.getByPlayer(e.getPlayer()).ifPresent(player1 -> gameService.removeSpectator(game, player1));
                });
    }

    private Optional<PracticeGame<?>> getGame(@NonNull Player player) {
        return new Optional<>(gameService.getGames()
                .stream()
                .filter(game -> game.getSpectators()
                        .stream()
                        .anyMatch(player2 -> player2.getUniqueId().equals(player.getUniqueId())))
                .findFirst());
    }

    private boolean isSpectator(@NonNull Player player) {
        return getGame(player).isPresent();
    }
}
