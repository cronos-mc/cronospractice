package com.cronos.practice.bukkit.configuration.serializers;

import com.cronos.commons.exceptions.YmlDeserializationException;
import com.cronos.commons.exceptions.YmlSerializationException;
import com.cronos.commons.utils.Optional;
import com.cronos.practice.bukkit.model.arena.PracticeArena;
import com.cronos.practice.bukkit.model.arena.SpawnLocation;
import com.cronos.practice.bukkit.model.kit.PracticeKit;
import com.cronos.practice.bukkit.service.ArenaService;
import com.cronos.practice.bukkit.service.KitService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.bukkit.configuration.ConfigurationSection;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@RequiredArgsConstructor
public class ArenaSerializer extends PracticeSerializer<PracticeArena> {

    private static final SpawnLocationSerializer serializer = new SpawnLocationSerializer();
    private final KitService kitService;
    private final ArenaService arenaService;

    @Override
    public Optional<PracticeArena> deserialize(@NonNull ConfigurationSection section) throws YmlDeserializationException {
        if (!exists("name", section)
                || !exists("spawn", section)
                || !exists("kits", section)
                || !exists("world", section))
            return Optional.empty();
        String name = section.getString("name");
        List<SpawnLocation> spawnLocation = serializer.deserializeList(section.getConfigurationSection("spawn"));
        List<PracticeKit> kits = section.getStringList("kits")
                .stream()
                .map(kitService::getByName)
                .filter(java.util.Optional::isPresent)
                .map(java.util.Optional::get)
                .collect(Collectors.toList());
        File originalWorld = new File(arenaService.getWorldsFolder(), section.getString("world"));
        return Optional.of(new PracticeArena(name, kits, spawnLocation, originalWorld));
    }

    @Override
    public void serialize(ConfigurationSection section, @NonNull PracticeArena arena) throws YmlSerializationException {
        section.set("name", arena.getName());
        serializer.serializeList(section.createSection("spawn"), arena.getSpawnLocations());
        section.set("kits", arena.getKits()
                .stream()
                .map(PracticeKit::getName)
                .collect(Collectors.toList()));
        section.set("world", arena.getOriginalWorld().getName());
    }
}
