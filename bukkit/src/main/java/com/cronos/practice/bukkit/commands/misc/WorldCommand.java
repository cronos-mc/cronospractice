package com.cronos.practice.bukkit.commands.misc;

import com.cronos.bukkit.commands.BukkitCommand;
import com.cronos.commons.commands.CommandResult;
import com.cronos.commons.commands.SenderType;
import com.cronos.practice.bukkit.Practice;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class WorldCommand extends BukkitCommand {

    public WorldCommand(Practice plugin) {
        super(plugin, "world");
        setDescription("Teleport to world spawn.");
        setAllowedSenderType(SenderType.PLAYER);
        setPermission("core.world");

        addRequiredArgument("world", String.class);
    }

    @Override
    protected CommandResult execute(CommandSender commandSender, Object[] objects) {
        getArgument(objects, String.class).ifPresent(s -> {
            World world = Bukkit.getWorld(s);
            if (Objects.isNull(world))
                return;
            ((Player) commandSender).teleport(world.getSpawnLocation());
        });
        return CommandResult.SUCCESS;
    }
}
