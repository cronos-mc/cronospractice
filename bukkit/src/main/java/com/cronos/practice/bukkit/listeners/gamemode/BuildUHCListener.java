package com.cronos.practice.bukkit.listeners.gamemode;

import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.model.game.GameState;
import com.cronos.practice.bukkit.model.kit.SpecialKit;
import com.cronos.practice.bukkit.service.GameService;
import com.cronos.practice.bukkit.service.HistoryService;
import com.cronos.practice.bukkit.service.PlayerService;
import net.minecraft.server.v1_8_R3.EntityArrow;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftArrow;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;

public class BuildUHCListener implements Listener {
    private final GameService gameService;

    public BuildUHCListener(Bootstrap bootstrap) {
        this.gameService = bootstrap.getGameService();
    }

    @EventHandler
    public void onProjectileLand(ProjectileHitEvent event) {
        Projectile projectile = event.getEntity();
        if (projectile.getType() != EntityType.ARROW)
            return;

        if (projectile.getShooter() == null || !(projectile.getShooter() instanceof Player))
            return;
        Player shooter = (Player) projectile.getShooter();
        gameService.getByPlayer(shooter)
                .filter(game -> game.getKit()
                        .getSpecialKit()
                        .equals(SpecialKit.BUILD_UHC))
                .filter(game -> !game.getState().equals(GameState.ENDED))
                .ifPresent(game -> {
                    EntityArrow arrow = ((CraftArrow) projectile).getHandle();
                    arrow.fromPlayer = 0;
                });
    }
}
