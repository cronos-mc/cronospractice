package com.cronos.practice.bukkit.model.kit;

import lombok.Data;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class PracticeKit {

    private final String name;
    private final ItemStack icon;
    private final String permission;
    private final List<KitItem> content;
    private final SpecialKit specialKit;

    public String getDisplayName() {
        return icon.getItemMeta()
                .getDisplayName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PracticeKit that = (PracticeKit) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
