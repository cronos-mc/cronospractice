package com.cronos.practice.bukkit.service;

import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.model.gamemode.PracticeGameMode;
import com.cronos.practice.bukkit.model.kit.SpecialKit;
import com.cronos.practice.bukkit.repository.GamemodeRepository;
import lombok.NonNull;

import java.util.List;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class GamemodeService {

    private final GamemodeRepository repository;
    private final Bootstrap bootstrap;

    public GamemodeService(@NonNull Bootstrap bootstrap) {
        this.repository = new GamemodeRepository();
        this.bootstrap = bootstrap;
    }

    public void init() {
        getGameModes().forEach(gameMode -> gameMode.init(bootstrap));
    }

    public void add(@NonNull PracticeGameMode gameMode) {
        this.repository.add(gameMode);
    }

    public void remove(@NonNull PracticeGameMode gameMode) {
        this.repository.remove(gameMode);
    }

    public Optional<PracticeGameMode> getByKit(@NonNull SpecialKit kit) {
        return this.repository.getByKit(kit);
    }

    public List<PracticeGameMode> getGameModes() {
        return this.repository.getGameModes();
    }
}
