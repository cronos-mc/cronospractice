package com.cronos.practice.bukkit.commands.queue;

import com.cronos.bukkit.commands.BukkitCommand;
import com.cronos.commons.commands.CommandResult;
import com.cronos.commons.commands.SenderType;
import com.cronos.practice.bukkit.Practice;
import com.cronos.practice.bukkit.event.queue.QueueQuitEvent;
import com.cronos.practice.bukkit.internalization.PracticeEn;
import com.cronos.practice.bukkit.service.QueueService;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class QueueLeaveCommand extends BukkitCommand {

    private final QueueService queueService;

    public QueueLeaveCommand(Practice plugin) {
        super(plugin, "leave");
        this.queueService = Practice.getBootstrap().getQueueService();

        setDescription("Leave current queue.");
        setAllowedSenderType(SenderType.PLAYER);
    }

    @Override
    protected CommandResult execute(CommandSender commandSender, Object[] objects) {
        queueService.getByPlayer((Player) commandSender)
                .ifPresentOrElse(element -> {
                    element.message(PracticeEn.QUEUE_LEFT, "kit", element.getKit().getDisplayName());
                    queueService.remove(element, QueueQuitEvent.Reason.MANUAL);
                }, () -> message(commandSender, PracticeEn.NOT_IN_QUEUE));
        return CommandResult.SUCCESS;
    }
}
