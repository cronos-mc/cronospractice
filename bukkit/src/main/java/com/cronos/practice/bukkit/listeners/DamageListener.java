package com.cronos.practice.bukkit.listeners;

import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.model.game.GameState;
import com.cronos.practice.bukkit.model.kit.SpecialKit;
import com.cronos.practice.bukkit.service.GameService;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class DamageListener implements Listener {

    private final GameService service;

    public DamageListener(Bootstrap bootstrap) {
        this.service = bootstrap.getGameService();
    }

    @EventHandler
    public void onPlayerDamage(EntityDamageEvent e) {
        if (!(e.getEntity() instanceof Player))
            return;
        service.getByPlayer((Player) e.getEntity())
                .filter(game -> !game.getState().equals(GameState.PLAYING))
                .ifPresent(game -> e.setCancelled(true));
        service.getByPlayer((Player) e.getEntity())
                .filter(game -> game.getState().equals(GameState.PLAYING))
                .filter(game -> !game.getKit().getSpecialKit().equals(SpecialKit.NONE))
                .filter(game -> !game.getKit().getSpecialKit().equals(SpecialKit.BUILD_UHC))
                .ifPresent(game -> {
                    e.setDamage(0);
                    ((Player) e.getEntity()).setHealth(20d);
                });
    }
}
