package com.cronos.practice.bukkit.repository;

import com.cronos.core.commons.party.Party;
import com.cronos.practice.bukkit.model.kit.PracticeKit;
import com.cronos.practice.bukkit.model.queue.QueueElement;
import com.cronos.practice.bukkit.model.game.GameType;
import lombok.Getter;
import lombok.NonNull;
import org.bukkit.entity.Player;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class QueueRepository {

    @Getter
    private final LinkedList<QueueElement> elements = new LinkedList<>();

    public void add(@NonNull QueueElement element) {
        this.elements.addLast(element);
    }

    public void remove(@NonNull QueueElement element) {
        this.elements.remove(element);
    }

    public LinkedList<QueueElement> getByKitAndType(@NonNull PracticeKit kit, @NonNull GameType type) {
        return this.elements.stream()
                .filter(element -> element.getKit().getName().equals(kit.getName()))
                .filter(element -> element.getType().equals(type))
                .sorted(Comparator.comparing(QueueElement::getSubscriptionDate))
                .collect(Collectors.toCollection(LinkedList::new));
    }


    public int countByTypeAndKit(@NonNull GameType type, @NonNull PracticeKit kit) {
        return (int) this.elements
                .stream()
                .filter(element -> element.getType().equals(type))
                .filter(element -> element.getKit().equals(kit))
                .count();
    }

    public int countByType(@NonNull GameType type) {
        return (int) this.elements
                .stream()
                .filter(element -> element.getType().equals(type))
                .count();
    }

    public boolean isInQueue(@NonNull Player player, @NonNull GameType type) {
        return this.elements
                .stream()
                .filter(element -> element.getType().equals(type))
                .anyMatch(element -> {
                    Party party = element.getAsParty()
                            .orElse(null);
                    if (Objects.nonNull(party))
                        return party.getMembers()
                                .stream()
                                .anyMatch(uuid -> uuid.equals(player.getUniqueId()));
                    Player player1 = element.getAsPlayer().orElse(null);
                    if (Objects.nonNull(player1))
                        return player1.getUniqueId().equals(player.getUniqueId());
                    return false;
                });
    }

    public boolean isInQueue(@NonNull Player player, @NonNull GameType type, @NonNull PracticeKit kit) {
        return getByKitAndType(kit, type)
                .stream()
                .anyMatch(element -> {
                    Party party = element.getAsParty()
                            .orElse(null);
                    if (Objects.nonNull(party))
                        return party.getMembers()
                                .stream()
                                .anyMatch(uuid -> uuid.equals(player.getUniqueId()));
                    Player player1 = element.getAsPlayer().orElse(null);
                    if (Objects.nonNull(player1))
                        return player1.getUniqueId().equals(player.getUniqueId());
                    return false;
                });
    }

    public boolean isInQueue(@NonNull Player player) {
        return this.elements
                .stream()
                .anyMatch(element -> {
                    Party party = element.getAsParty()
                            .orElse(null);
                    if (Objects.nonNull(party))
                        return party.getMembers()
                                .stream()
                                .anyMatch(uuid -> uuid.equals(player.getUniqueId()));
                    Player player1 = element.getAsPlayer().orElse(null);
                    if (Objects.nonNull(player1))
                        return player1.getUniqueId().equals(player.getUniqueId());
                    return false;
                });
    }

    public Optional<QueueElement> getByPlayer(@NonNull Player player) {
        return this.elements
                .stream()
                .filter(element -> {
                    Party party = element.getAsParty()
                            .orElse(null);
                    if (Objects.nonNull(party))
                        return party.getMembers()
                                .stream()
                                .anyMatch(uuid -> uuid.equals(player.getUniqueId()));
                    Player player1 = element.getAsPlayer().orElse(null);
                    if (Objects.nonNull(player1))
                        return player1.getUniqueId().equals(player.getUniqueId());
                    return false;
                })
                .findFirst();
    }
}
