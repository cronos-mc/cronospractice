package com.cronos.practice.bukkit;

import com.cronos.messaging.NetworkManager;
import com.cronos.messaging.redis.RedisMessenger;
import com.cronos.practice.bukkit.configuration.Configuration;
import com.cronos.practice.bukkit.listeners.*;
import com.cronos.practice.bukkit.messaging.BukkitPacketHandler;
import com.cronos.practice.bukkit.model.gamemode.BoxingGameMode;
import com.cronos.practice.bukkit.model.gamemode.BuildUHCGameMode;
import com.cronos.practice.bukkit.model.gamemode.SumoGameMode;
import com.cronos.practice.bukkit.service.*;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Slf4j(topic = "Bootstrap")
@Getter
public class Bootstrap implements NetworkManager {

    private final UUID identifier;

    /* Plugin */
    private final Practice plugin;

    /* Messenger */
    private final RedisMessenger messenger;

    /* Configuration */
    private final Configuration configuration;

    /* Kits */
    private final KitService kitService;

    /* Arenas */
    private final ArenaService arenaService;

    /* Players */
    private final PlayerService playerService;

    /* Games */
    private final GameService gameService;

    /* Hits */
    private final HitService hitService;

    /* Histories */
    private final HistoryService historyService;

    /* Queue */
    private final QueueService queueService;

    /* Scoreboards */
    private final ScoreboardService scoreboardService;

    /* Items */
    private final ItemService itemService;

    /* Blocks */
    private final BlockService blockService;

    /* Gamemodes */
    private final GamemodeService gamemodeService;

    /* Cooldown */
    private final CooldownService cooldownService;

    /* Duels */
    private final DuelService duelService;

    public Bootstrap(Practice plugin) {
        /* Identifier */
        this.identifier = UUID.randomUUID();

        /* Plugin */
        this.plugin = plugin;

        /* Configuration */
        this.configuration = plugin.registerConfiguration(Configuration.class, plugin);

        /* Messenger */
        BukkitPacketHandler packetHandler = new BukkitPacketHandler(this);
        this.messenger = new RedisMessenger(configuration.getRedisCredentials(), packetHandler);
        this.messenger.registerChannel(() -> "practice@general");
        this.messenger.init(this, plugin);

        /* Kits */
        this.kitService = new KitService(this);

        /* Arenas */
        this.arenaService = new ArenaService(this);

        /* Scoreboards */
        this.scoreboardService = new ScoreboardService(this);

        /* Items */
        this.itemService = new ItemService(this);

        /* Players */
        this.playerService = new PlayerService(this);

        /* Hits */
        this.hitService = new HitService(this);

        /* Histories */
        this.historyService = new HistoryService(this);

        /* Games */
        this.gameService = new GameService(this);

        /* Queue */
        this.queueService = new QueueService(this);

        /* Blocks */
        this.blockService = new BlockService(this);

        /* Gamemodes */
        this.gamemodeService = new GamemodeService(this);
        this.gamemodeService.add(new SumoGameMode());
        this.gamemodeService.add(new BoxingGameMode());
        this.gamemodeService.add(new BuildUHCGameMode());
        this.gamemodeService.init();

        /* Cooldown */
        this.cooldownService = new CooldownService(this);

        /* Duels */
        this.duelService = new DuelService(this);

        /* Packet Handler */
        packetHandler.setKitService(kitService);
        packetHandler.setMessenger(messenger);

        /* Listeners */
        registerListeners(new ConnexionListener(this),
                new FriendlyFireListener(this),
                new DamageListener(this),
                new DeathListener(this),
                new ScoreboardListener(this),
                new BlockListener(this),
                new ItemListener(this),
                new QueueListener(this),
                new LobbyListener(this),
                new WeaponListener(this),
                new PotionListener(this),
                new HitListener(this),
                new CooldownListener(this),
                new MiscListener(),
                new SpectatorListener(this));
    }

    public void registerListeners(Listener... listeners) {
        for (Listener listener : listeners)
            Bukkit.getPluginManager()
                    .registerEvents(listener, plugin);
    }

}
