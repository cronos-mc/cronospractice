package com.cronos.practice.bukkit.configuration.serializers;

import com.cronos.bukkit.configuration.serializers.ItemStackSerializer;
import com.cronos.commons.exceptions.YmlDeserializationException;
import com.cronos.commons.exceptions.YmlSerializationException;
import com.cronos.commons.utils.Optional;
import com.cronos.practice.bukkit.model.kit.KitItem;
import com.cronos.practice.bukkit.model.kit.PracticeKit;
import com.cronos.practice.bukkit.model.kit.SpecialKit;
import lombok.NonNull;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class KitSerializer extends PracticeSerializer<PracticeKit> {

    private static final KitItemSerializer kitItemSerializer = new KitItemSerializer();
    private static final ItemStackSerializer itemStackSerializer = new ItemStackSerializer();

    @Override
    public Optional<PracticeKit> deserialize(@NonNull ConfigurationSection section) throws YmlDeserializationException {
        if (!exists("id", section)
                || !exists("material", section)
                || !exists("content", section))
            return Optional.empty();
        String name = section.getString("id");
        String permission = section.getString("permission");
        ItemStack icon = itemStackSerializer.deserialize(section)
                .orElseThrow(() -> new YmlDeserializationException("Invalid icon for kit."));
        List<KitItem> content = kitItemSerializer.deserializeList(section.getConfigurationSection("content"));
        if (content.size() == 0)
            throw new YmlDeserializationException("Empty content for kit.");
        SpecialKit kit = SpecialKit.NONE;
        if (exists("type", section))
            kit = SpecialKit.valueOf(section.getString("type", "NONE"));
        return Optional.of(new PracticeKit(name, icon, permission, content, kit));
    }

    @Override
    public void serialize(ConfigurationSection section, @NonNull PracticeKit kit) throws YmlSerializationException {
        section.set("id", kit.getName());
        section.set("permission", kit.getPermission());
        itemStackSerializer.serialize(section, kit.getIcon());
        AtomicInteger index = new AtomicInteger(0);
        ConfigurationSection section1 = section.createSection("content");
        kit.getContent()
                .forEach(kitItem -> {
                    try {
                        kitItemSerializer.serialize(section1.createSection("item-" + index.getAndIncrement()), kitItem);
                    } catch (YmlSerializationException e) {
                        e.printStackTrace();
                    }
                });
    }
}
