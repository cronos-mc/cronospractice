package com.cronos.practice.bukkit.listeners;

import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.event.game.GameStateChangeEvent;
import com.cronos.practice.bukkit.model.game.GameState;
import com.cronos.practice.bukkit.model.hit.HitRecord;
import com.cronos.practice.bukkit.service.GameService;
import com.cronos.practice.bukkit.service.HitService;
import lombok.NonNull;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class HitListener implements Listener {

    private final HitService service;
    private final GameService gameService;

    public HitListener(@NonNull Bootstrap bootstrap) {
        this.service = bootstrap.getHitService();
        this.gameService = bootstrap.getGameService();
    }

    @EventHandler
    public void onHit(@NonNull PlayerInteractEvent e) {
        service.getByPlayer(e.getPlayer())
                .ifPresent(record -> record.setTmpCps(record.getTmpCps() + 1));
    }

    @EventHandler
    public void onPlayerHit(EntityDamageByEntityEvent e) {
        if (!(e.getEntity() instanceof Player)
                || !(e.getDamager() instanceof Player)
                || e.isCancelled())
            return;
        Player player = (Player) e.getDamager();
        gameService.getByPlayer(player)
                .filter(game -> game.getState().equals(GameState.PLAYING))
                .ifPresent(game -> service.getByPlayer(player).ifPresent(record -> record.setHits(record.getHits() + 1)));
    }

    @EventHandler
    public void onGameStart(@NonNull GameStateChangeEvent e) {
        if (!e.getNewState().equals(GameState.PLAYING))
            return;
        e.getPlayers()
                .forEach(player -> service.add(new HitRecord(player.getUniqueId())));
    }
}
