package com.cronos.practice.bukkit.model.team;

import com.cronos.bukkit.utils.xseries.XSound;
import com.cronos.commons.internalization.TranslationPack;
import com.cronos.practice.bukkit.model.participant.GameParticipant;
import com.cronos.practice.bukkit.model.player.PracticePlayer;
import lombok.Getter;
import lombok.NonNull;
import net.md_5.bungee.api.chat.BaseComponent;

import java.util.List;
import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class PracticeTeam extends GameParticipant {

    private final PracticePlayer leader;
    private final List<PracticePlayer> members;

    public PracticeTeam(UUID uniqueId, PracticePlayer leader, List<PracticePlayer> members) {
        super(uniqueId);
        this.leader = leader;
        this.members = members;
    }

    @Override
    public <T extends TranslationPack> void message(T translation, Object... placeholders) {
        members.forEach(player -> player.message(translation, placeholders));
    }

    @Override
    public void message(String message, Object... placeholders) {
        members.forEach(player -> player.message(message, placeholders));
    }

    @Override
    public void message(BaseComponent message) {
        members.forEach(player -> player.message(message));
    }

    @Override
    public void playSound(@NonNull XSound sound) {
        members.forEach(player -> player.playSound(sound));
    }
}
