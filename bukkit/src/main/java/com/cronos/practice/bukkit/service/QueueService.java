package com.cronos.practice.bukkit.service;

import com.cronos.commons.utils.Optional;
import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.event.queue.QueueJoinEvent;
import com.cronos.practice.bukkit.event.queue.QueueQuitEvent;
import com.cronos.practice.bukkit.model.game.GameType;
import com.cronos.practice.bukkit.model.kit.PracticeKit;
import com.cronos.practice.bukkit.model.queue.QueueElement;
import com.cronos.practice.bukkit.repository.QueueRepository;
import com.cronos.practice.bukkit.runnable.QueueUpdateRunnable;
import lombok.NonNull;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Comparator;
import java.util.Deque;
import java.util.LinkedList;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class QueueService {

    private final QueueRepository repository;

    public QueueService(@NonNull Bootstrap bootstrap) {
        this.repository = new QueueRepository();

        bootstrap.getPlugin()
                .getTaskService()
                .getFactory()
                .newRepeatingTask(new QueueUpdateRunnable(bootstrap, this), 2);
    }

    public void add(@NonNull QueueElement element) {
        this.repository.add(element);
        Bukkit.getPluginManager().callEvent(new QueueJoinEvent(element));
    }

    public void remove(@NonNull QueueElement element, QueueQuitEvent.Reason reason) {
        this.repository.remove(element);
        Bukkit.getPluginManager().callEvent(new QueueQuitEvent(element, reason));
    }

    public Deque<QueueElement> getByKitAndType(@NonNull PracticeKit kit, @NonNull GameType type) {
        return this.repository.getByKitAndType(kit, type);
    }

    public Deque<QueueElement> getByKitAndType(@NonNull PracticeKit kit, @NonNull GameType type, boolean team) {
        return this.repository.getByKitAndType(kit, type)
                .stream()
                .filter(queueElement -> queueElement.isParty() == team)
                .sorted(Comparator.comparing(QueueElement::getSubscriptionDate))
                .collect(Collectors.toCollection(LinkedList::new));
    }

    public int countByTypeAndKit(@NonNull GameType type, @NonNull PracticeKit kit) {
        return this.repository.countByTypeAndKit(type, kit);
    }

    public int countByType(@NonNull GameType type) {
        return this.repository.countByType(type);
    }

    public boolean isInQueue(@NonNull Player player, @NonNull GameType type) {
        return this.repository.isInQueue(player, type);
    }

    public boolean isInQueue(@NonNull Player player, @NonNull GameType type, @NonNull PracticeKit kit) {
        return this.repository.isInQueue(player, type, kit);
    }

    public boolean isInQueue(@NonNull Player player) {
        return this.repository.isInQueue(player);
    }

    public int getPosition(@NonNull Player player) {
        return getByPlayer(player)
                .map(element -> getElements().indexOf(element))
                .orElse(0);
    }

    public Optional<QueueElement> getByPlayer(@NonNull Player player) {
        return new Optional<>(this.repository.getByPlayer(player));
    }

    public LinkedList<QueueElement> getElements() {
        return this.repository.getElements();
    }
}
