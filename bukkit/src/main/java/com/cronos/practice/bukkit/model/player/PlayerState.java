package com.cronos.practice.bukkit.model.player;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public enum PlayerState {
    /* In Game */
    ALIVE,
    DEAD,
    SPECTATOR,

    /* Global */
    AVAILABLE,
    DISCONNECTED
}
