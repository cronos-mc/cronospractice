package com.cronos.practice.bukkit.repository;

import com.cronos.core.commons.players.PlayerProfile;
import com.cronos.practice.bukkit.model.player.PracticePlayer;
import lombok.Getter;
import lombok.NonNull;
import org.bukkit.OfflinePlayer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class PlayerRepository {

    @Getter
    private final List<PracticePlayer> players = new ArrayList<>();

    public void add(@NonNull PracticePlayer player) {
        this.players.add(player);
    }

    public void remove(@NonNull PracticePlayer player) {
        this.players.remove(player);
    }

    public Optional<PracticePlayer> getByProfile(@NonNull PlayerProfile profile) {
        return this.getByUniqueId(profile.getUniqueId());
    }

    public Optional<PracticePlayer> getByPlayer(@NonNull OfflinePlayer player) {
        return this.getByUniqueId(player.getUniqueId());
    }

    public Optional<PracticePlayer> getByUniqueId(@NonNull UUID uniqueId) {
        return this.players
                .stream()
                .filter(player -> player.getUniqueId().equals(uniqueId))
                .findFirst();
    }
}
