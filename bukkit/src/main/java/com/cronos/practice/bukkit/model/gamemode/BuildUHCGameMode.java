package com.cronos.practice.bukkit.model.gamemode;

import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.listeners.gamemode.BuildUHCListener;
import com.cronos.practice.bukkit.model.kit.SpecialKit;
import lombok.NonNull;

public class BuildUHCGameMode implements PracticeGameMode {

    @Override
    public SpecialKit getKit() {
        return SpecialKit.BUILD_UHC;
    }

    @Override
    public void init(@NonNull Bootstrap bootstrap) {
        bootstrap.registerListeners(new BuildUHCListener(bootstrap));
    }
}
