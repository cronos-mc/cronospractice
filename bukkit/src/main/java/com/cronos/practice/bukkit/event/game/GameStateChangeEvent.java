package com.cronos.practice.bukkit.event.game;

import com.cronos.practice.bukkit.model.game.GameState;
import com.cronos.practice.bukkit.model.game.PracticeGame;
import lombok.Getter;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class GameStateChangeEvent extends GameEvent {

    private final GameState currentState;
    private final GameState newState;

    public GameStateChangeEvent(PracticeGame<?> game, GameState currentState, GameState newState) {
        super(game);
        this.currentState = currentState;
        this.newState = newState;
    }
}
