package com.cronos.practice.bukkit.repository;

import com.cronos.commons.utils.Optional;
import com.cronos.practice.bukkit.model.game.PracticeGame;
import com.cronos.practice.bukkit.model.history.MatchEntry;
import com.cronos.practice.bukkit.model.history.MatchHistory;
import lombok.Getter;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class HistoryRepository {

    @Getter
    private final List<MatchHistory> histories = new ArrayList<>();

    public void add(@NonNull MatchHistory history) {
        this.histories.add(history);
    }

    public void remove(@NonNull MatchHistory history) {
        this.histories.remove(history);
    }

    public Optional<MatchHistory> getByGame(@NonNull PracticeGame<?> game) {
        return new Optional<>(this.histories.stream()
                .filter(history -> history.getGameId().equals(game.getUniqueId()))
                .findFirst());
    }

    public Optional<MatchHistory> getById(@NonNull UUID uuid) {
        return new Optional<>(this.histories.stream()
                .filter(history -> history.getId().equals(uuid))
                .findFirst());
    }

    public Optional<MatchEntry> getByIdAndPlayer(@NonNull UUID uuid, @NonNull UUID playerId) {
        return getById(uuid)
                .map(history -> history.getEntries()
                        .stream()
                        .filter(matchEntry -> matchEntry.getUniqueId().equals(playerId))
                        .findFirst())
                .filter(java.util.Optional::isPresent)
                .map(java.util.Optional::get);
    }
}
