package com.cronos.practice.bukkit.service;

import com.cronos.commons.utils.Optional;
import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.model.hit.HitRecord;
import com.cronos.practice.bukkit.repository.HitRepository;
import com.cronos.practice.bukkit.runnable.HitCounterRunnable;
import lombok.NonNull;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class HitService {

    private final HitRepository repository;

    public HitService(@NonNull Bootstrap bootstrap) {
        this.repository = new HitRepository();

        new HitCounterRunnable(bootstrap, this)
                .runTaskTimer(bootstrap.getPlugin(), 0, 1);
    }

    public void add(@NonNull HitRecord record) {
        this.repository.add(record);
    }

    public void remove(@NonNull HitRecord record) {
        this.repository.remove(record);
    }

    public Optional<HitRecord> getByPlayer(@NonNull UUID id) {
        return this.repository.getByPlayer(id);
    }

    public Optional<HitRecord> getByPlayer(@NonNull Player player) {
        return this.repository.getByPlayer(player);
    }

    public List<HitRecord> getRecords() {
        return this.repository.getRecords();
    }
}
