package com.cronos.practice.bukkit.configuration.serializers;

import com.cronos.bukkit.configuration.serializers.ItemStackSerializer;
import com.cronos.commons.exceptions.YmlDeserializationException;
import com.cronos.commons.exceptions.YmlSerializationException;
import com.cronos.commons.utils.Optional;
import com.cronos.practice.bukkit.model.kit.KitItem;
import lombok.NonNull;
import org.bukkit.configuration.ConfigurationSection;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class KitItemSerializer extends PracticeSerializer<KitItem> {

    private static final ItemStackSerializer itemSerializer = new ItemStackSerializer();

    @Override
    public Optional<KitItem> deserialize(@NonNull ConfigurationSection section) throws YmlDeserializationException {
        if (!exists("slot", section)
                || !exists("material", section))
            return Optional.empty();
        List<Integer> slots = new ArrayList<>();
        if (section.isInt("slot"))
            slots.add(section.getInt("slot"));
        else if (section.isList("slot"))
            slots.addAll(section.getIntegerList("slot"));
        return itemSerializer.deserialize(section)
                .map(itemStack -> new KitItem(slots, itemStack));
    }

    @Override
    public void serialize(ConfigurationSection section, @NonNull KitItem kitItem) throws YmlSerializationException {
        section.set("slot", kitItem.getSlot());
        itemSerializer.serialize(section, kitItem.getItem());
    }
}
