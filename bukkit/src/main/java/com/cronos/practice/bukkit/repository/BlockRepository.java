package com.cronos.practice.bukkit.repository;

import lombok.Getter;
import lombok.NonNull;
import org.bukkit.block.Block;

import java.util.*;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class BlockRepository {

    @Getter
    private final Map<UUID, List<Block>> blocks = new HashMap<>();

    public void add(@NonNull UUID uuid, @NonNull Block block) {
        this.blocks.putIfAbsent(uuid, new ArrayList<>());
        this.blocks.get(uuid).add(block);
    }

    public void remove(@NonNull UUID uuid, @NonNull Block block) {
        this.blocks.getOrDefault(uuid, new ArrayList<>())
                .removeIf(block1 -> block1.getX() == block.getX()
                        && block1.getY() == block.getY()
                        && block1.getZ() == block.getZ());
    }

    public List<Block> get(@NonNull UUID uuid) {
        return blocks.getOrDefault(uuid, new ArrayList<>());
    }

    public void clear(@NonNull UUID uuid) {
        blocks.remove(uuid);
    }
}
