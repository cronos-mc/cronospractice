package com.cronos.practice.bukkit.model.duel;

import com.cronos.practice.bukkit.model.kit.PracticeKit;
import lombok.Data;
import lombok.NonNull;
import org.bukkit.entity.Player;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class DuelRequest {

    private final Player requester;
    private final Player requested;
    private final PracticeKit kit;
    private final long date;

    public DuelRequest(@NonNull Player requester, @NonNull Player requested, @NonNull PracticeKit kit) {
        this.requester = requester;
        this.requested = requested;
        this.kit = kit;
        this.date = System.currentTimeMillis();
    }
}
