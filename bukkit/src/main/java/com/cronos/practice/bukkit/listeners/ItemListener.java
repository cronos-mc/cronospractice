package com.cronos.practice.bukkit.listeners;

import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.event.queue.QueueJoinEvent;
import com.cronos.practice.bukkit.event.queue.QueueQuitEvent;
import com.cronos.practice.bukkit.model.items.PracticeItem;
import com.cronos.practice.bukkit.service.GameService;
import com.cronos.practice.bukkit.service.ItemService;
import lombok.NonNull;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ItemListener implements Listener {

    private final ItemService service;
    private final GameService gameService;

    public ItemListener(@NonNull Bootstrap bootstrap) {
        this.service = bootstrap.getItemService();
        this.gameService = bootstrap.getGameService();
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        this.service.give(e.getPlayer(), PracticeItem.Type.HUB);
    }

    @EventHandler
    public void onItemDrop(PlayerDropItemEvent e) {
        if (e.getPlayer().hasPermission("practice.admin"))
            return;
        service.getByItemStack(e.getItemDrop().getItemStack())
                .ifPresent(item -> e.setCancelled(true));
    }

    @EventHandler
    public void onItemMove(InventoryClickEvent e) {
        if (Objects.nonNull(e.getClickedInventory())
                && e.getClickedInventory().equals(e.getWhoClicked().getInventory())
                && !gameService.isInGame(e.getWhoClicked().getUniqueId())
                && !e.getWhoClicked().hasPermission("practice.admin"))
            e.setCancelled(true);
    }

    @EventHandler
    public void onItemClick(PlayerInteractEvent e) {
        if (Objects.isNull(e.getItem())
                || !(e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)))
            return;
        service.getByItemStack(e.getItem())
                .ifPresent(item -> {
                    e.setCancelled(true);
                    e.getPlayer().performCommand(item.getCommand());
                });
    }

    @EventHandler
    public void onQueueJoin(QueueJoinEvent e) {
        e.getPlayers().forEach(player -> service.give(player, PracticeItem.Type.QUEUE));
    }

    @EventHandler
    public void onQueueQuit(QueueQuitEvent e) {
        e.getPlayers().forEach(player -> service.give(player, PracticeItem.Type.HUB));
    }
}
