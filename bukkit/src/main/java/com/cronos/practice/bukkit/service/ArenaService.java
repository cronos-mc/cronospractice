package com.cronos.practice.bukkit.service;

import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.loaders.ArenaLoader;
import com.cronos.practice.bukkit.model.arena.PracticeArena;
import com.cronos.practice.bukkit.model.kit.PracticeKit;
import com.cronos.practice.bukkit.repository.ArenaRepository;
import lombok.Getter;
import lombok.NonNull;

import java.io.File;
import java.util.List;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ArenaService {

    private final ArenaRepository repository;

    @Getter
    private final File dataFolder;

    @Getter
    private final File worldsFolder;

    public ArenaService(Bootstrap bootstrap) {
        this.repository = new ArenaRepository();
        this.dataFolder = new File(bootstrap.getPlugin().getDataFolder(), "arenas");
        this.worldsFolder = new File(bootstrap.getPlugin().getDataFolder(), "worlds");

        bootstrap.getPlugin()
                .registerPostLoadTask(new ArenaLoader(bootstrap, this));
    }

    public void add(@NonNull PracticeArena arena) {
        if (!arena.isValid())
            return;
        arena.init();
        this.repository.add(arena);
    }

    public void remove(@NonNull PracticeArena arena) {
        this.repository.remove(arena);
    }

    public List<PracticeArena> getByKit(@NonNull PracticeKit kit) {
        return this.repository.getByKit(kit);
    }

    public Optional<PracticeArena> getByName(@NonNull String name) {
        return this.repository.getByName(name);
    }

    public List<PracticeArena> getArenas() {
        return this.repository.getArenas();
    }
}
