package com.cronos.practice.bukkit.model.winner;

import com.cronos.practice.bukkit.model.game.PracticeGame;
import com.cronos.practice.bukkit.model.participant.GameParticipant;
import lombok.NonNull;

import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface WinnerSelector<P extends GameParticipant, T extends PracticeGame<P>> {

    Optional<P> getWinner(@NonNull T game);

}
