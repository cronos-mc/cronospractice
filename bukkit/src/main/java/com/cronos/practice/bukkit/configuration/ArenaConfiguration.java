package com.cronos.practice.bukkit.configuration;

import com.cronos.bukkit.configuration.ConfigurableFileWrapper;
import com.cronos.commons.utils.Optional;
import com.cronos.practice.bukkit.Practice;
import com.cronos.practice.bukkit.configuration.serializers.ArenaSerializer;
import com.cronos.practice.bukkit.model.arena.PracticeArena;
import com.cronos.practice.bukkit.service.ArenaService;
import com.cronos.practice.bukkit.service.KitService;
import lombok.SneakyThrows;

import java.io.File;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ArenaConfiguration extends ConfigurableFileWrapper {

    private final ArenaSerializer serializer;

    public ArenaConfiguration(Practice plugin, KitService kitService, ArenaService arenaService, File file) {
        super(plugin, file, null);
        this.serializer = new ArenaSerializer(kitService, arenaService);
    }

    @SneakyThrows
    public Optional<PracticeArena> parse() {
        return serializer.deserialize(this.getConfiguration());
    }
}
