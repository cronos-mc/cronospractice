package com.cronos.practice.bukkit.commands.kits;

import com.cronos.bukkit.commands.BukkitCommand;
import com.cronos.bukkit.utils.items.ItemBuilder;
import com.cronos.commons.commands.CommandResult;
import com.cronos.commons.commands.SenderType;
import com.cronos.commons.configuration.YamlFileLoadException;
import com.cronos.commons.exceptions.YmlSerializationException;
import com.cronos.commons.internalization.integrated.CommandsEn;
import com.cronos.commons.utils.Optional;
import com.cronos.practice.bukkit.Practice;
import com.cronos.practice.bukkit.configuration.KitConfiguration;
import com.cronos.practice.bukkit.internalization.PracticeEn;
import com.cronos.practice.bukkit.model.kit.KitItem;
import com.cronos.practice.bukkit.model.kit.PracticeKit;
import com.cronos.practice.bukkit.model.kit.SpecialKit;
import com.cronos.practice.bukkit.service.KitService;
import lombok.NonNull;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class KitCreateCommand extends BukkitCommand {

    private final KitService service;

    public KitCreateCommand(Practice plugin) {
        super(plugin, "create");
        this.service = Practice.getBootstrap().getKitService();

        setDescription("Create a new kit.");
        setAllowedSenderType(SenderType.PLAYER);
        setPermission("practice.kits.create");
        setAllowedSenderType(SenderType.PLAYER);

        addRequiredArgument("name", String.class);
    }

    @Override
    protected CommandResult execute(CommandSender commandSender, Object[] objects) {
        getArgument(objects, String.class).ifPresentOrElse(s -> {
            if (service.getByName(s).isPresent()) {
                message(commandSender, PracticeEn.KIT_ALREADY_EXISTS);
                return;
            }
            PracticeKit kit = new PracticeKit(s, new ItemBuilder(Material.DIAMOND_SWORD)
                    .setName(ChatColor.WHITE + s)
                    .setLore("Change me in config")
                    .toItemStack(),
                    null,
                    getContentFromPlayer((Player) commandSender),
                    SpecialKit.NONE);
            KitConfiguration configuration = new KitConfiguration((Practice) getPlugin(), new File(service.getDataFolder(), s + ".yml"));
            try {
                configuration.load();
                configuration.save(kit);
                configuration.save();
            } catch (YamlFileLoadException | YmlSerializationException | IOException | IllegalAccessException e) {
                message(commandSender, PracticeEn.KIT_ERROR);
                e.printStackTrace();
                return;
            }
            service.add(kit);
            message(commandSender, PracticeEn.KIT_CREATED, "name", s);
        }, () -> message(commandSender, CommandsEn.COMMAND_INVALID_SYNTAX, "syntax", getSyntax()));
        return CommandResult.SUCCESS;
    }

    private List<KitItem> getContentFromPlayer(@NonNull Player player) {
        List<KitItem> items = new ArrayList<>();
        for (int i = 0; i < player.getInventory().getContents().length; i++) {
            ItemStack itemStack = player.getInventory().getItem(i);
            if (Objects.isNull(itemStack))
                continue;
            int finalI = i;
            new Optional<>(new ArrayList<>(items).stream()
                    .filter(kitItem -> kitItem.getItem().equals(itemStack))
                    .findFirst())
                    .ifPresentOrElse(kitItem -> kitItem.getSlot().add(finalI), () -> items.add(new KitItem(new ArrayList<>(Collections.singletonList(finalI)), itemStack)));
        }
        for (int i = 36; i <= 39; i++) {
            ItemStack itemStack = player.getInventory().getItem(i);
            if (Objects.isNull(itemStack))
                continue;
            int finalI = i;
            new Optional<>(new ArrayList<>(items).stream()
                    .filter(kitItem -> kitItem.getItem().equals(itemStack))
                    .findFirst())
                    .ifPresentOrElse(kitItem -> kitItem.getSlot().add(finalI), () -> items.add(new KitItem(new ArrayList<>(Collections.singletonList(finalI)), itemStack)));
        }
        return items;
    }
}
