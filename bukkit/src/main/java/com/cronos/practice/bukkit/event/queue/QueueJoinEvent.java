package com.cronos.practice.bukkit.event.queue;

import com.cronos.practice.bukkit.model.queue.QueueElement;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class QueueJoinEvent extends QueueEvent {

    public QueueJoinEvent(QueueElement element) {
        super(element);
    }
}
