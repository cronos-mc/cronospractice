package com.cronos.practice.bukkit.ui.queue;

import com.cronos.bukkit.ui.CronosUI;
import com.cronos.bukkit.utils.items.ItemBuilder;
import com.cronos.bukkit.utils.xseries.XMaterial;
import com.cronos.core.bukkit.BukkitCore;
import com.cronos.core.commons.party.PartyService;
import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.Practice;
import com.cronos.practice.bukkit.event.queue.QueueQuitEvent;
import com.cronos.practice.bukkit.internalization.PracticeEn;
import com.cronos.practice.bukkit.model.game.GameType;
import com.cronos.practice.bukkit.model.queue.QueueElement;
import com.cronos.practice.bukkit.service.GameService;
import com.cronos.practice.bukkit.service.KitService;
import com.cronos.practice.bukkit.service.QueueService;
import lombok.NonNull;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class KitSelectionUI extends CronosUI {

    private final KitService kitService;
    private final QueueService queueService;
    private final PartyService partyService;
    private final GameService gameService;

    public KitSelectionUI(Practice plugin) {
        super(plugin);
        Bootstrap bootstrap = Practice.getBootstrap();
        this.kitService = bootstrap.getKitService();
        this.queueService = bootstrap.getQueueService();
        this.partyService = BukkitCore.getPlugin(BukkitCore.class).getPartyService();
        this.gameService = bootstrap.getGameService();

        createInventory("&c&lERROR", 9);
    }

    @Override
    public void open(Player player, Object[] objects) {
        GameType type = (GameType) objects[0];
        createInventory("&f» &6Select a kit", getNearestMultiple(kitService.getKits().size() + 19, 9));
        display(player, type);
    }

    private void display(@NonNull Player player, @NonNull GameType type) {
        boolean isInQueue = queueService.isInQueue(player);

        /* Kits */
        AtomicInteger slot = new AtomicInteger(0);
        kitService.getKits()
                .forEach(kit -> {
                    // boolean has = (Objects.isNull(kit.getPermission()) || kit.getPermission().isEmpty()) || player.hasPermission(kit.getPermission());
                    boolean has = true;
                    boolean isInThisQueue = queueService.isInQueue(player, type, kit);
                    int playing = gameService.getByTypeAndKit(type, kit);
                    int amount = playing;
                    if(amount > 64)
                        amount = 64;
                    else if(amount == 0)
                        amount = 1;
                    ItemBuilder item = new ItemBuilder(kit.getIcon())
                            .addLoreLine("&r", "&6&lIn Queue", "&f&l" + queueService.countByTypeAndKit(type, kit), "&r",
                                    "&6&lPlaying:", "&f&l" + playing, "&r")
                            .addFlags(ItemFlag.values())
                            .setAmount(amount);
                    if (isInQueue) {
                        if (isInThisQueue)
                            item.addLoreLine("&6&lClick&7 to leave queue.");
                        else
                            item.addLoreLine("&e&l!! &7Leave current queue before", "&7joining this one.");
                    } else {
                        if (!has)
                            item.addLoreLine("&c&lNot owned");
                        else
                            item.addLoreLine("&6&lClick&7 to join queue.");
                    }
                    addItem(getInsideSlots()[getInventory().getSize() / 9 - 1][slot.getAndIncrement()], item)
                            .setClickAction(e -> {
                                if (!has || (isInQueue && !isInThisQueue))
                                    return;
                                if (isInThisQueue) {
                                    queueService.getByPlayer(player)
                                            .ifPresent(element -> {
                                                queueService.remove(element, QueueQuitEvent.Reason.MANUAL);
                                                element.message(PracticeEn.QUEUE_LEFT, "kit", kit.getDisplayName());
                                            });
                                    player.closeInventory();
                                    return;
                                }
                                partyService.getPartyByPlayer(player.getUniqueId())
                                        .ifPresentOrElse(party -> {
                                            QueueElement element = new QueueElement(party.getId(), true, kit, type);
                                            queueService.add(element);
                                            element.message(PracticeEn.QUEUE_JOINED, "type", type.name().toLowerCase(), "kit", kit.getDisplayName());
                                        }, () -> {
                                            QueueElement element = new QueueElement(player.getUniqueId(), false, kit, type);
                                            element.message(PracticeEn.QUEUE_JOINED, "type", type.name().toLowerCase(), "kit", kit.getDisplayName());
                                            queueService.add(element);
                                        });
                                player.closeInventory();
                            });
                });

        /* Fillers */
        fillBorders(new ItemBuilder(XMaterial.WHITE_STAINED_GLASS_PANE.parseItem()).setName("&r"));
    }
}
