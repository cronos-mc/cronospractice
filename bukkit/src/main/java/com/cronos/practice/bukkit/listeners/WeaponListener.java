package com.cronos.practice.bukkit.listeners;

import com.cronos.bukkit.utils.BukkitUtil;
import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.internalization.PracticeEn;
import com.cronos.practice.bukkit.service.GameService;
import lombok.NonNull;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class WeaponListener extends BukkitUtil implements Listener {

    private final GameService service;

    public WeaponListener(@NonNull Bootstrap bootstrap) {
        super(bootstrap.getPlugin());
        this.service = bootstrap.getGameService();
    }

    @EventHandler
    public void onWeaponDrop(PlayerDropItemEvent e) {
        service.getByPlayer(e.getPlayer())
                .ifPresent(game -> {
                    String material = e.getItemDrop().getItemStack().getType().name().toUpperCase();
                    if (material.contains("_SWORD") || material.contains("_AXE")) {
                        e.setCancelled(true);
                        message(e.getPlayer(), PracticeEn.CANNOT_DROP_WEAPON);
                    }
                });
    }
}
