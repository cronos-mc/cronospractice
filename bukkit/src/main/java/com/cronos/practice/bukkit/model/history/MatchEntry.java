package com.cronos.practice.bukkit.model.history;

import com.cronos.practice.bukkit.model.hit.HitRecord;
import com.google.common.collect.ImmutableMap;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Map;
import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class MatchEntry {

    private final UUID uniqueId;
    private final String name;
    private final double health;
    private final int hunger;
    private final Map<ItemStackType, ItemStack[]> inventory;

    @Setter
    private HitRecord statistics;

    public MatchEntry(Player player) {
        this.uniqueId = player.getUniqueId();
        this.name = player.getName();
        this.health = player.getHealth();
        this.hunger = player.getFoodLevel();
        this.inventory = ImmutableMap.of(ItemStackType.NORMAL,
                copy(player.getInventory().getContents()),
                ItemStackType.ARMOR,
                copy(player.getInventory().getArmorContents()));
    }

    private ItemStack[] copy(ItemStack[] items) {
        ItemStack[] items1 = new ItemStack[items.length];
        System.arraycopy(items, 0, items1, 0, items.length);
        return items1;
    }

}
