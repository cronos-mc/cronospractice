package com.cronos.practice.bukkit.runnable.game;

import com.cronos.practice.bukkit.model.game.GameState;
import com.cronos.practice.bukkit.model.game.PracticeGame;
import com.cronos.practice.bukkit.model.game.SoloGame;
import com.cronos.practice.bukkit.model.game.TeamGame;
import com.cronos.practice.bukkit.model.winner.SoloWinnerSelector;
import com.cronos.practice.bukkit.model.winner.TeamWinnerSelector;

import java.util.concurrent.TimeUnit;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class GamePlayRunnable extends GameRunnable {

    private static final SoloWinnerSelector soloWinnerSelector = new SoloWinnerSelector();
    private static final TeamWinnerSelector teamWinnerSelector = new TeamWinnerSelector();

    public GamePlayRunnable(PracticeGame<?> game) {
        super(game, (int) TimeUnit.HOURS.toSeconds(1));
    }

    @Override
    public void onTick(int time) {
        if (game instanceof SoloGame) {
            SoloGame g = (SoloGame) game;
            soloWinnerSelector
                    .getWinner(g)
                    .ifPresent(player -> {
                        game.setWinner(player);
                        game.setState(GameState.CELEBRATING);
                    });
            return;
        }
        TeamGame g = (TeamGame) game;
        teamWinnerSelector
                .getWinner(g)
                .ifPresent(practiceTeam -> {
                    game.setWinner(practiceTeam);
                    game.setState(GameState.CELEBRATING);
                });
    }

    @Override
    public void onComplete() {
        game.setState(GameState.CELEBRATING);
    }
}
