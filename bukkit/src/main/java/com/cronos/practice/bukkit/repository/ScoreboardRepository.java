package com.cronos.practice.bukkit.repository;

import com.cronos.commons.utils.Optional;
import com.cronos.practice.bukkit.model.scoreboard.PracticeScoreboard;
import lombok.Getter;
import lombok.NonNull;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ScoreboardRepository {

    @Getter
    private final List<PracticeScoreboard> scoreboards = new ArrayList<>();

    public void add(@NonNull PracticeScoreboard scoreboard) {
        this.scoreboards.add(scoreboard);
    }

    public void remove(@NonNull PracticeScoreboard scoreboard) {
        this.scoreboards.remove(scoreboard);
    }

    public Optional<PracticeScoreboard> getByPlayer(@NonNull Player player) {
        return this.getByPlayer(player.getUniqueId());
    }

    public Optional<PracticeScoreboard> getByPlayer(@NonNull UUID uuid) {
        return new Optional<>(this.scoreboards
                .stream()
                .filter(scoreboard -> scoreboard.getPlayerId().equals(uuid))
                .findFirst());
    }
}
