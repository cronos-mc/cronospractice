package com.cronos.practice.bukkit.ui.selector;

import com.cronos.bukkit.ui.CronosUI;
import com.cronos.bukkit.utils.items.ItemBuilder;
import com.cronos.bukkit.utils.xseries.XMaterial;
import com.cronos.commons.utils.Pagination;
import com.cronos.core.bukkit.ui.selector.SelectorButtonProvider;
import com.cronos.core.bukkit.ui.selector.SelectorClickProvider;
import com.cronos.practice.bukkit.Practice;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public abstract class SingleSelectorUI<T> extends CronosUI {

    protected final int[] slots = new int[]{20, 21, 22, 23, 24, 29, 30, 31, 32, 33};
    protected final List<T> objects;
    protected final Pagination<T> pagination = new Pagination<>();

    /* Providers */
    protected SelectorButtonProvider<T> buttonProvider;
    protected SelectorClickProvider<T> clickProvider;

    public SingleSelectorUI(Practice plugin, SelectorButtonProvider<T> buttonProvider, SelectorClickProvider<T> clickProvider) {
        super(plugin);
        this.buttonProvider = buttonProvider;
        this.clickProvider = clickProvider;
        this.objects = new ArrayList<>();
    }

    public SingleSelectorUI(Practice plugin) {
        super(plugin);
        this.objects = new ArrayList<>();
    }

    @Override
    public void open(Player player, Object[] args) {
        if (clickProvider == null) clickProvider = (consumer, player1, object) -> {
            player1.closeInventory();
            consumer.accept(object);
        };

        Consumer<T> consumer = (Consumer<T>) args[0];
        if (args.length == 2 && args[1] instanceof Collection) {
            objects.clear();
            objects.addAll((Collection<T>) args[1]);
        }
        display(player, 1, consumer);
    }

    private void display(Player player, int page, Consumer<T> consumer) {
        getInventory().clear();
        int lastPage = getMaxPage(objects, slots.length);

        /* Objects */
        List<T> toDisplays = pagination.paginate(objects, slots.length, page);
        AtomicInteger slot = new AtomicInteger();
        toDisplays.forEach(obj -> addItem(slots[slot.getAndIncrement()], buttonProvider.provide(obj).setLore("&r", "&6&l!! &7Click to select."))
                .setClickAction(e -> clickProvider.accept(consumer, player, obj)));

        /* Fillers */
        fillBorders(new ItemBuilder(Objects.requireNonNull(XMaterial.YELLOW_STAINED_GLASS_PANE.parseItem())).setName("&r"));

        /* Navigation */
        if (page > 1)
            addItem(45, new ItemBuilder(Material.ARROW).setName("&f&lPrevious page"))
                    .setClickAction(inventoryClickEvent -> display(player, page - 1, consumer));
        if (page < lastPage)
            addItem(53, new ItemBuilder(Material.ARROW).setName("&f&lNext page"))
                    .setClickAction(inventoryClickEvent -> display(player, page + 1, consumer));
    }

}
