package com.cronos.practice.bukkit.runnable;

import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.service.CooldownService;
import com.cronos.practice.bukkit.service.GameService;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class CooldownUpdateRunnable extends BukkitRunnable {

    private final CooldownService service;
    private final GameService gameService;

    public CooldownUpdateRunnable(Bootstrap bootstrap, CooldownService service) {
        this.service = service;
        this.gameService = bootstrap.getGameService();
    }

    @Override
    public void run() {
        service.getCooldowns()
                .removeIf(cooldown -> !gameService.isInGame(cooldown.getPlayerId())
                        || cooldown.isExpired()
                        || Objects.isNull(Bukkit.getPlayer(cooldown.getPlayerId())));

        service.getCooldowns()
                .forEach(cooldown -> {
                    Player player = Bukkit.getPlayer(cooldown.getPlayerId());
                    player.setLevel(cooldown.getRemainingTime());
                    float remaining = (cooldown.getStart() + cooldown.getType().getSeconds() * 1000L) - System.currentTimeMillis();
                    float total = cooldown.getType().getSeconds() * 1000;
                    player.setExp(remaining / total);
                });
    }

}
