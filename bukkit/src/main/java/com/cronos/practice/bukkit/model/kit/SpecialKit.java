package com.cronos.practice.bukkit.model.kit;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public enum SpecialKit {
    SUMO,
    BOXING,
    BUILD_UHC,
    NONE
}
