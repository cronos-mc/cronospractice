package com.cronos.practice.bukkit.listeners;

import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.configuration.Configuration;
import com.cronos.practice.bukkit.service.GameService;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class LobbyListener implements Listener {

    private final Configuration configuration;
    private final GameService gameService;

    public LobbyListener(Bootstrap bootstrap) {
        this.configuration = bootstrap.getConfiguration();
        this.gameService = bootstrap.getGameService();
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player player = e.getPlayer();
        player.teleport(configuration.getSpawnLocation());
        player.setHealth(20d);
        player.setFoodLevel(20);
        player.setGameMode(GameMode.SURVIVAL);
    }

    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent e) {
        if (!gameService.isInGame(e.getEntity().getUniqueId()))
            e.setFoodLevel(20);
    }

    @EventHandler
    public void onPlayerDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof Player && !gameService.isInGame(e.getEntity().getUniqueId())) {
            e.setCancelled(true);

            if (e.getCause() == EntityDamageEvent.DamageCause.VOID)
                e.getEntity().teleport(configuration.getSpawnLocation());
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        e.getPlayer().getInventory().clear();
    }
}
