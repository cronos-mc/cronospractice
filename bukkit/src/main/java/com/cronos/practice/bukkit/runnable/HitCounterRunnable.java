package com.cronos.practice.bukkit.runnable;

import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.service.HitService;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class HitCounterRunnable extends BukkitRunnable {

    private final HitService service;

    public HitCounterRunnable(Bootstrap bootstrap, HitService service) {
        this.service = service;
    }

    @Override
    public void run() {
        service.getRecords()
                .stream()
                .filter(record -> System.currentTimeMillis() - record.getCheckStart() >= 1000)
                .forEach(record -> {
                    int cps = record.getTmpCps();
                    record.setAverage((record.getAverage() + cps) / 2);
                    if (cps > record.getHighest())
                        record.setHighest(cps);

                    // Reset
                    record.setCheckStart(System.currentTimeMillis());
                    record.setTmpCps(0);
                });
    }
}
