package com.cronos.practice.bukkit.configuration;

import com.cronos.bukkit.configuration.ConfigurableFileWrapper;
import com.cronos.commons.exceptions.YmlSerializationException;
import com.cronos.commons.utils.Optional;
import com.cronos.practice.bukkit.Practice;
import com.cronos.practice.bukkit.configuration.serializers.KitSerializer;
import com.cronos.practice.bukkit.model.kit.PracticeKit;
import lombok.NonNull;
import lombok.SneakyThrows;

import java.io.File;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class KitConfiguration extends ConfigurableFileWrapper {

    private final KitSerializer serializer;

    public KitConfiguration(Practice plugin, File file) {
        super(plugin, file, null);
        this.serializer = new KitSerializer();
    }

    @SneakyThrows
    public Optional<PracticeKit> parse() {
        return serializer.deserialize(this.getConfiguration());
    }

    public void save(@NonNull PracticeKit kit) throws YmlSerializationException {
        serializer.serialize(this.getConfiguration(), kit);
    }
}
