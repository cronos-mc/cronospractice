package com.cronos.practice.bukkit.model.scoreboard.content;

import com.cronos.core.commons.utils.TimeFormatter;
import com.cronos.practice.bukkit.Practice;
import com.cronos.practice.bukkit.internalization.PracticeEn;
import com.cronos.practice.bukkit.model.arena.PracticeArena;
import com.cronos.practice.bukkit.model.game.PracticeGame;
import com.cronos.practice.bukkit.model.kit.PracticeKit;
import com.cronos.practice.bukkit.runnable.game.GameRunnable;
import com.cronos.practice.bukkit.service.GameService;
import com.cronos.practice.bukkit.utils.PlayerUtils;
import lombok.NonNull;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class StartingScoreboardContent extends ScoreboardContent {

    @Override
    public List<String> getLines(@NonNull Player player) {
        GameService gameService = Practice.getBootstrap().getGameService();
        Optional<PracticeGame<?>> game = gameService.getByPlayer(player);
        String kitName = game.map(PracticeGame::getKit)
                .map(PracticeKit::getDisplayName)
                .orElse("?");
        String map = game.map(PracticeGame::getArena)
                .map(PracticeArena::getName)
                .orElse("?");
        String start = game.map(PracticeGame::getCurrentRunnable)
                .filter(Objects::nonNull)
                .map(GameRunnable::getRunningTime)
                .map(TimeFormatter::formatDuration)
                .orElse("?");
        return new ArrayList<>(format(PracticeEn.SCOREBOARD_STARTING, "map", map, "kit", kitName, "ping", PlayerUtils.getPing(player), "time", start));
    }

}
