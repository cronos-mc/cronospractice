package com.cronos.practice.bukkit.model.scoreboard.content;

import com.cronos.practice.bukkit.Practice;
import com.cronos.practice.bukkit.internalization.PracticeEn;
import com.cronos.practice.bukkit.model.arena.PracticeArena;
import com.cronos.practice.bukkit.model.game.PracticeGame;
import com.cronos.practice.bukkit.model.kit.PracticeKit;
import com.cronos.practice.bukkit.service.GameService;
import com.cronos.practice.bukkit.utils.PlayerUtils;
import lombok.NonNull;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class WaitingScoreboardContent extends ScoreboardContent {

    @Override
    public List<String> getLines(@NonNull Player player) {
        GameService gameService = Practice.getBootstrap().getGameService();
        Optional<PracticeGame<?>> game = gameService.getByPlayer(player);
        String kitName = game.map(PracticeGame::getKit)
                .map(PracticeKit::getDisplayName)
                .orElse("?");
        String map = game.map(PracticeGame::getArena)
                .map(PracticeArena::getName)
                .orElse("?");
        return new ArrayList<>(format(PracticeEn.SCOREBOARD_WAITING, "map", map, "kit", kitName, "ping", PlayerUtils.getPing(player)));
    }

}
