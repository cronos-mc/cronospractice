package com.cronos.practice.bukkit.repository;

import com.cronos.practice.bukkit.model.gamemode.PracticeGameMode;
import com.cronos.practice.bukkit.model.kit.SpecialKit;
import lombok.Getter;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class GamemodeRepository {

    @Getter
    private final List<PracticeGameMode> gameModes = new ArrayList<>();

    public void add(@NonNull PracticeGameMode gameMode) {
        gameModes.add(gameMode);
    }

    public void remove(@NonNull PracticeGameMode gameMode) {
        gameModes.remove(gameMode);
    }

    public Optional<PracticeGameMode> getByKit(@NonNull SpecialKit kit) {
        return gameModes.stream()
                .filter(gameMode -> gameMode.getKit().equals(kit))
                .findFirst();
    }
}
