package com.cronos.practice.bukkit.listeners;

import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.model.player.PlayerState;
import com.cronos.practice.bukkit.model.player.PracticePlayer;
import com.cronos.practice.bukkit.service.PlayerService;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ConnexionListener implements Listener {

    private final PlayerService service;

    public ConnexionListener(Bootstrap bootstrap) {
        this.service = bootstrap.getPlayerService();
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        service.add(new PracticePlayer(e.getPlayer()));
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        service.getByPlayer(e.getPlayer())
                .ifPresent(player -> player.setState(PlayerState.DISCONNECTED));
    }
}
