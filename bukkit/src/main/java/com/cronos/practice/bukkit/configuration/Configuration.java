package com.cronos.practice.bukkit.configuration;

import com.cronos.bukkit.configuration.ConfigurableFileWrapper;
import com.cronos.commons.configuration.Configurable;
import com.cronos.messaging.redis.RedisCredentials;
import com.cronos.practice.bukkit.Practice;
import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.potion.PotionType;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class Configuration extends ConfigurableFileWrapper {

    @Configurable(key = "redis")
    private RedisCredentials redisCredentials;

    @Configurable(key = "fallback-server")
    private String fallBackServer;

    @Configurable(key = "server-name")
    private String serverName;

    @Configurable(key = "items")
    private ConfigurationSection items;

    @Configurable(key = "hub")
    private Location spawnLocation;

    public Configuration(Practice plugin) {
        super(plugin, "config.yml");
    }
}
