package com.cronos.practice.bukkit.model.cooldown;

import lombok.Data;

import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class PracticeCooldown {

    private final UUID playerId;
    private final CooldownType type;
    private final long start;

    public PracticeCooldown(UUID playerId, CooldownType type) {
        this.playerId = playerId;
        this.type = type;
        this.start = System.currentTimeMillis();
    }

    public int getRemainingTime() {
        return (int) (((start + type.getSeconds() * 1000) - System.currentTimeMillis()) / 1000);
    }

    public boolean isExpired() {
        return System.currentTimeMillis() - start >= type.getSeconds() * 1000L;
    }
}
