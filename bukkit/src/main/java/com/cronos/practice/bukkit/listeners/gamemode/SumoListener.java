package com.cronos.practice.bukkit.listeners.gamemode;

import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.model.game.GameState;
import com.cronos.practice.bukkit.model.game.SoloGame;
import com.cronos.practice.bukkit.model.history.MatchEntry;
import com.cronos.practice.bukkit.model.kit.SpecialKit;
import com.cronos.practice.bukkit.model.player.PlayerState;
import com.cronos.practice.bukkit.model.player.PracticePlayer;
import com.cronos.practice.bukkit.service.GameService;
import com.cronos.practice.bukkit.service.HistoryService;
import com.cronos.practice.bukkit.service.PlayerService;
import lombok.NonNull;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class SumoListener implements Listener {

    private final GameService gameService;
    private final PlayerService playerService;
    private final HistoryService historyService;

    public SumoListener(Bootstrap bootstrap) {
        this.gameService = bootstrap.getGameService();
        this.playerService = bootstrap.getPlayerService();
        this.historyService = bootstrap.getHistoryService();
    }

    @EventHandler
    public void onFoodLvlChange(FoodLevelChangeEvent e) {
        Player player = (Player) e.getEntity();
        gameService.getByPlayer(player)
                .filter(game -> game.getKit()
                        .getSpecialKit()
                        .equals(SpecialKit.SUMO))
                .filter(game -> !game.getState().equals(GameState.ENDED))
                .ifPresent(game -> e.setFoodLevel(20));
    }

    @EventHandler
    public void onGameStart(PlayerMoveEvent e) {
        Player player = e.getPlayer();
        gameService.getByPlayer(player)
                .filter(game -> game.getKit()
                        .getSpecialKit()
                        .equals(SpecialKit.SUMO))
                .filter(game -> game.getState().equals(GameState.STARTING))
                .ifPresent(game -> player.getInventory().clear());
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        Player player = e.getPlayer();
        if (!isInWater(player))
            return;
        gameService.getByPlayer(player)
                .filter(game -> game.getKit()
                        .getSpecialKit()
                        .equals(SpecialKit.SUMO))
                .ifPresent(game -> {
                    PracticePlayer practicePlayer = playerService.getByPlayer(player)
                            .orElse(null);
                    if (game.getState().equals(GameState.ENDED) || Objects.isNull(practicePlayer))
                        return;
                    player.setHealth(0.1);
                    historyService.getByGame(game)
                            .ifPresent(history -> history.getEntries().add(new MatchEntry(player)));
                    player.getInventory().clear();
                    practicePlayer.setState(PlayerState.DEAD);
                    if (game instanceof SoloGame) {
                        ((SoloGame) game).getParticipants()
                                .stream()
                                .filter(p -> !p.getUniqueId().equals(player.getUniqueId()))
                                .forEach(p -> p.getAsPlayer().ifPresent(online -> {
                                    online.hidePlayer(player);
                                }));
                    }
                });
    }

    @EventHandler
    public void onPlayerMoveGameStart(PlayerMoveEvent e) {
        gameService.getByPlayer(e.getPlayer())
                .filter(game -> game.getKit().getSpecialKit().equals(SpecialKit.SUMO))
                .filter(game -> game.getState().equals(GameState.STARTING))
                .ifPresent(game -> {
                    if (((e.getTo().getX() != e.getFrom().getX()) || (e.getTo().getZ() != e.getFrom().getZ())))
                        e.setTo(e.getFrom());
                });
    }

    private boolean isInWater(@NonNull Player player) {
        Material m = player.getLocation().getBlock().getType();
        return m == Material.STATIONARY_WATER || m == Material.WATER;
    }
}
