package com.cronos.practice.bukkit.configuration.serializers;

import com.cronos.bukkit.configuration.serializers.YmlSerializer;
import com.cronos.commons.exceptions.YmlDeserializationException;
import lombok.NonNull;
import org.bukkit.configuration.ConfigurationSection;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public abstract class PracticeSerializer<T> implements YmlSerializer<T> {

    protected boolean exists(@NonNull String key, @NonNull ConfigurationSection section) throws YmlDeserializationException {
        if (!section.contains(key))
            throw new YmlDeserializationException(String.format("Key '%s' is missing!", key));
        return true;
    }
}
