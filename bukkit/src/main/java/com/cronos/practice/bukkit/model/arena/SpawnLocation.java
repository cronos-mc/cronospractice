package com.cronos.practice.bukkit.model.arena;

import lombok.Data;
import lombok.NonNull;
import org.bukkit.Location;
import org.bukkit.World;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class SpawnLocation {

    private final double x;
    private final double y;
    private final double z;
    private final float yaw;
    private final float pitch;

    public Location toLocation(@NonNull World world) {
        return new Location(world, x, y, z, yaw, pitch);
    }
}
