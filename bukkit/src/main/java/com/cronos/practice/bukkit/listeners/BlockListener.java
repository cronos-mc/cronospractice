package com.cronos.practice.bukkit.listeners;

import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.service.BlockService;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class BlockListener implements Listener {

    private final BlockService service;

    public BlockListener(Bootstrap bootstrap) {
        this.service = bootstrap.getBlockService();
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        if (!e.getPlayer().hasPermission("practice.blocks"))
            e.setCancelled(true);
        if (service.canBreakBlock(e.getPlayer(), e.getBlock()))
            service.remove(e.getPlayer().getUniqueId(), e.getBlock());
        else
            e.setCancelled(true);
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        if (!event.getPlayer().hasPermission("practice.blocks"))
            event.setCancelled(true);
        if (service.canPlaceBlock(event.getPlayer()))
            service.add(event.getPlayer().getUniqueId(), event.getBlock());
        else
            event.setCancelled(true);
    }
}
