package com.cronos.practice.bukkit.service;

import com.cronos.commons.utils.Optional;
import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.internalization.PracticeEn;
import com.cronos.practice.bukkit.model.game.PracticeGame;
import com.cronos.practice.bukkit.model.history.MatchEntry;
import com.cronos.practice.bukkit.model.history.MatchHistory;
import com.cronos.practice.bukkit.repository.HistoryRepository;
import lombok.NonNull;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;

import java.util.Arrays;
import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class HistoryService {

    private final HistoryRepository repository;
    private final HitService hitService;

    public HistoryService(@NonNull Bootstrap bootstrap) {
        this.repository = new HistoryRepository();
        this.hitService = bootstrap.getHitService();
    }

    public void add(@NonNull MatchHistory history) {
        this.repository.add(history);
    }

    public void remove(@NonNull MatchHistory history) {
        this.repository.remove(history);
    }

    public void sendMessage(@NonNull PracticeGame<?> game) {
        game.message(PracticeEn.GAME_RESULT);
        getByGame(game)
                .ifPresentOrElse(history -> {
                    history.getEntries()
                            .forEach(matchEntry -> hitService.getByPlayer(matchEntry.getUniqueId())
                                    .ifPresent(record -> {
                                        matchEntry.setStatistics(record);
                                        hitService.remove(record);
                                    }));
                    TextComponent winnerComponent = new TextComponent("Winner: ");
                    winnerComponent.setColor(ChatColor.GREEN);
                    TextComponent winnerComponent1 = new TextComponent(history.getWinner());
                    winnerComponent1.setColor(ChatColor.GOLD);
                    winnerComponent1.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/vgh " + history.getWinner() + " " + history.getId()));
                    winnerComponent.addExtra(winnerComponent1);
                    TextComponent separator = new TextComponent(" | ");
                    separator.setColor(ChatColor.GRAY);
                    winnerComponent.addExtra(separator);
                    TextComponent loserComponent = new TextComponent("Loser: ");
                    loserComponent.setColor(ChatColor.RED);
                    winnerComponent.addExtra(loserComponent);
                    Arrays.stream(history.getLoser().split(" "))
                            .forEach(s -> {
                                TextComponent loserComponent1 = new TextComponent(history.getLoser() + " ");
                                loserComponent1.setColor(ChatColor.GOLD);
                                loserComponent1.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/vgh " + s + " " + history.getId()));
                                winnerComponent.addExtra(loserComponent1);
                            });
                    game.message(winnerComponent);
                    game.message("&r");
                }, () -> game.message("&aWinner: &c&lN/A &7| &cLoser: &c&lN/A"));
    }

    public Optional<MatchHistory> getByGame(@NonNull PracticeGame<?> game) {
        return this.repository.getByGame(game);
    }

    public Optional<MatchHistory> getById(@NonNull UUID uuid) {
        return this.repository.getById(uuid);
    }

    public Optional<MatchEntry> getByIdAndPlayer(@NonNull UUID uuid, @NonNull UUID playerId) {
        return this.repository.getByIdAndPlayer(uuid, playerId);
    }
}
