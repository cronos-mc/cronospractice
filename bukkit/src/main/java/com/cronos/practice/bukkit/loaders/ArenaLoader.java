package com.cronos.practice.bukkit.loaders;

import com.cronos.commons.configuration.YamlFileLoadException;
import com.cronos.commons.tasks.startup.PostLoadTask;
import com.cronos.commons.utils.Optional;
import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.configuration.ArenaConfiguration;
import com.cronos.practice.bukkit.service.ArenaService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@RequiredArgsConstructor
@Slf4j(topic = "Arena Loader")
public class ArenaLoader implements PostLoadTask {

    private final Bootstrap bootstrap;
    private final ArenaService service;

    @Override
    public void run() {
        log.info("Loading arenas...");
        if (!service.getWorldsFolder().exists())
            if (!service.getWorldsFolder().mkdir())
                log.error("Cannot create 'worlds' folder!");
        if (!service.getDataFolder().exists())
            if (!service.getDataFolder().mkdir())
                log.error("Cannot create 'arenas' folder!");

        service.getArenas().clear();
        Arrays.stream(Objects.requireNonNull(service.getDataFolder().listFiles()))
                .filter(file -> file.getName().contains(".yml"))
                .map(file -> new ArenaConfiguration(bootstrap.getPlugin(), bootstrap.getKitService(), service, file))
                .peek(arenaConfiguration -> {
                    try {
                        arenaConfiguration.load();
                    } catch (YamlFileLoadException e) {
                        log.error("Cannot load arenas from file '{}'!", arenaConfiguration.getFile().getName());
                        e.printStackTrace();
                    }
                })
                .map(ArenaConfiguration::parse)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .forEach(service::add);

        log.info("{} arenas loaded!", service.getArenas().size());
    }
}
