package com.cronos.practice.bukkit.service;

import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.loaders.ItemLoader;
import com.cronos.practice.bukkit.model.items.PracticeItem;
import com.cronos.practice.bukkit.repository.ItemRepository;
import lombok.NonNull;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ItemService {

    private final ItemRepository repository;

    public ItemService(@NonNull Bootstrap bootstrap) {
        this.repository = new ItemRepository();

        bootstrap.getPlugin()
                .registerPostLoadTask(new ItemLoader(bootstrap, this));
    }

    public void add(@NonNull PracticeItem item) {
        this.repository.add(item);
    }

    public void remove(@NonNull PracticeItem item) {
        this.repository.remove(item);
    }

    public Optional<PracticeItem> getByItemStack(@NonNull ItemStack itemStack) {
        return this.repository.getByItemStack(itemStack);
    }

    public void give(@NonNull Player player, @NonNull PracticeItem.Type type) {
        player.getInventory().clear();
        player.getEquipment().setArmorContents(null);
        getItems()
                .stream()
                .filter(item -> item.getType().equals(type) || item.getType().equals(PracticeItem.Type.ALL))
                .forEach(item -> player.getInventory().setItem(item.getSlot(), item.getItemStack()));
    }

    public List<PracticeItem> getItems() {
        return this.repository.getItems();
    }
}
