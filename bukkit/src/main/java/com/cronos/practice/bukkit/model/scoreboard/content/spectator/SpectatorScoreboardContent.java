package com.cronos.practice.bukkit.model.scoreboard.content.spectator;

import com.cronos.practice.bukkit.Practice;
import com.cronos.practice.bukkit.model.game.PracticeGame;
import com.cronos.practice.bukkit.model.game.SoloGame;
import com.cronos.practice.bukkit.model.scoreboard.content.ScoreboardContent;
import lombok.NonNull;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public abstract class SpectatorScoreboardContent extends ScoreboardContent {

    public abstract List<String> getSoloContent(@NonNull PracticeGame<?> game, @NonNull Player player1, @NonNull Player player2);

    @Override
    public List<String> getLines(@NonNull Player player) {
        return Practice.getBootstrap()
                .getGameService()
                .getGames()
                .stream()
                .filter(game1 -> game1.getSpectators()
                        .stream()
                        .anyMatch(player1 -> player1.getUniqueId().equals(player.getUniqueId())))
                .map(game1 -> {
                    if (game1 instanceof SoloGame && game1.getParticipants().size() == 2) {
                        SoloGame sg = (SoloGame) game1;
                        return getSoloContent(game1,
                                sg.getParticipants()
                                        .get(0)
                                        .getAsPlayer()
                                        .orElseThrow(() -> new IllegalStateException("Cannot display spectator scoreboard for game #" + game1.getUniqueId().toString())),
                                sg.getParticipants()
                                        .get(1)
                                        .getAsPlayer()
                                        .orElseThrow(() -> new IllegalStateException("Cannot display spectator scoreboard for game #" + game1.getUniqueId().toString())));
                    }
                    return null;
                })
                .findFirst()
                .orElseGet(ArrayList::new);
    }
}
