package com.cronos.practice.bukkit.repository;

import com.cronos.practice.bukkit.model.cooldown.CooldownType;
import com.cronos.practice.bukkit.model.cooldown.PracticeCooldown;
import lombok.Getter;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class CooldownRepository {

    private final List<PracticeCooldown> cooldowns = new ArrayList<>();

    public void add(@NonNull PracticeCooldown cooldown) {
        this.cooldowns.add(cooldown);
    }

    public void remove(@NonNull PracticeCooldown cooldown) {
        this.cooldowns.remove(cooldown);
    }

    public Optional<PracticeCooldown> get(@NonNull UUID uuid, @NonNull CooldownType type) {
        return get(uuid).stream()
                .filter(cooldown -> cooldown.getType().equals(type))
                .findFirst();
    }

    public List<PracticeCooldown> get(@NonNull UUID uuid) {
        return cooldowns.stream()
                .filter(cooldown -> cooldown.getPlayerId().equals(uuid))
                .collect(Collectors.toList());
    }
}
