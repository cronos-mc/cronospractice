package com.cronos.practice.bukkit.messaging;

import com.cronos.bukkit.utils.BukkitUtil;
import com.cronos.messaging.IPacket;
import com.cronos.messaging.PacketHandler;
import com.cronos.messaging.redis.RedisMessenger;
import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.service.KitService;
import lombok.NonNull;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Setter
@Slf4j(topic = "Packet Handler")
public class BukkitPacketHandler extends BukkitUtil implements PacketHandler {

    private KitService kitService;
    private RedisMessenger messenger;
    private final Bootstrap bootstrap;

    public BukkitPacketHandler(@NonNull Bootstrap bootstrap) {
        super(bootstrap.getPlugin());
        this.bootstrap = bootstrap;
    }

    @Override
    public void handlePacketReception(@NonNull IPacket p) {
    }

}
