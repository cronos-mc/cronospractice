package com.cronos.practice.bukkit.listeners;

import com.cronos.bukkit.utils.BukkitUtil;
import com.cronos.bukkit.utils.xseries.XSound;
import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.internalization.PracticeEn;
import com.cronos.practice.bukkit.model.game.GameState;
import com.cronos.practice.bukkit.model.game.PracticeGame;
import com.cronos.practice.bukkit.model.game.SoloGame;
import com.cronos.practice.bukkit.model.history.MatchEntry;
import com.cronos.practice.bukkit.model.player.PlayerState;
import com.cronos.practice.bukkit.model.player.PracticePlayer;
import com.cronos.practice.bukkit.service.GameService;
import com.cronos.practice.bukkit.service.HistoryService;
import com.cronos.practice.bukkit.service.PlayerService;
import lombok.NonNull;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Random;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class DeathListener extends BukkitUtil implements Listener {

    private final GameService gameService;
    private final PlayerService playerService;
    private final HistoryService historyService;

    private static final HashMap<String, String> lastDamages = new HashMap<>();

    public DeathListener(Bootstrap bootstrap) {
        super(bootstrap.getPlugin());
        this.gameService = bootstrap.getGameService();
        this.playerService = bootstrap.getPlayerService();
        this.historyService = bootstrap.getHistoryService();
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerDamageByPlayer(EntityDamageByEntityEvent e) {
        if ((e.getDamager() instanceof Player && e.getEntity() instanceof Player)
                && !e.isCancelled())
            lastDamages.put(e.getEntity().getName(), e.getDamager().getName());
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerDamage(EntityDamageEvent e) {
        if (!(e.getEntity() instanceof Player))
            return;
        Player player = (Player) e.getEntity();
        if (player.getHealth() - e.getFinalDamage() > 0)
            return;
        PracticePlayer practicePlayer = playerService.getByPlayer(player)
                .orElse(null);
        gameService.getByPlayer(player)
                .ifPresent(game -> {
                    if (game.getState().equals(GameState.ENDED) || Objects.isNull(practicePlayer))
                        return;
                    e.setCancelled(true);
                    player.setHealth(0.1);
                    sendRandomDeathMessage(game, player.getName());
                    historyService.getByGame(game)
                            .ifPresent(history -> history.getEntries().add(new MatchEntry(player)));
                    player.getInventory().clear();
                    practicePlayer.setState(PlayerState.DEAD);
                    if (game instanceof SoloGame) {
                        ((SoloGame) game).getParticipants()
                                .stream()
                                .filter(p -> !p.getUniqueId().equals(player.getUniqueId()))
                                .forEach(p -> p.getAsPlayer().ifPresent(online -> {
                                    online.hidePlayer(player);
                                }));
                    }
                });
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        PracticePlayer practicePlayer = playerService.getByPlayer(player)
                .orElse(null);
        gameService.getByPlayer(player)
                .ifPresent(game -> {
                    if (game.getState().equals(GameState.ENDED) || Objects.isNull(practicePlayer))
                        return;
                    player.setHealth(0.1);
                    sendRandomDeathMessage(game, player.getName());
                    historyService.getByGame(game)
                            .ifPresent(history -> history.getEntries().add(new MatchEntry(player)));
                    player.getInventory().clear();
                    player.getEquipment().clear();
                    practicePlayer.setState(PlayerState.DEAD);
                });
    }

    private void sendRandomDeathMessage(@NonNull PracticeGame<?> game, @NonNull String killed) {
        List<String> msgs = translate(PracticeEn.PLAYER_KILLED);
        game.message(msgs.get(new Random().nextInt(msgs.size())), "killed", killed, "killer", lastDamages.getOrDefault(killed, "?"));
        game.playSound(XSound.ENTITY_PLAYER_DEATH);
    }
}
