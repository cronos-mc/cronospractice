package com.cronos.practice.bukkit.model.gamemode;

import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.listeners.gamemode.SumoListener;
import com.cronos.practice.bukkit.model.kit.SpecialKit;
import lombok.NonNull;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class SumoGameMode implements PracticeGameMode {

    @Override
    public SpecialKit getKit() {
        return SpecialKit.SUMO;
    }

    @Override
    public void init(@NonNull Bootstrap bootstrap) {
        bootstrap.registerListeners(new SumoListener(bootstrap));
    }
}
