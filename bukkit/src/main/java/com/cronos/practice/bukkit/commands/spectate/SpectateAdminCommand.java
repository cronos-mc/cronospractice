package com.cronos.practice.bukkit.commands.spectate;

import com.cronos.bukkit.commands.BukkitCommand;
import com.cronos.commons.commands.CommandResult;
import com.cronos.commons.commands.SenderType;
import com.cronos.commons.internalization.integrated.CommandsEn;
import com.cronos.commons.utils.Optional;
import com.cronos.practice.bukkit.Practice;
import com.cronos.practice.bukkit.internalization.PracticeEn;
import com.cronos.practice.bukkit.service.GameService;
import com.cronos.practice.bukkit.service.PlayerService;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class SpectateAdminCommand extends BukkitCommand {

    private final GameService service;
    private final PlayerService playerService;

    public SpectateAdminCommand(Practice plugin) {
        super(plugin, "stp");
        this.service = Practice.getBootstrap().getGameService();
        this.playerService = Practice.getBootstrap().getPlayerService();

        setPermission("practice.spectate");
        setDescription("Spectate a game.");
        setAllowedSenderType(SenderType.PLAYER);

        addRequiredArgument("player", Player.class);
    }

    @Override
    protected CommandResult execute(CommandSender commandSender, Object[] objects) {
        getArgument(objects, Player.class)
                .ifPresentOrElse(target -> {
                    Player player = (Player) commandSender;
                    new Optional<>(service.getByPlayer(player))
                            .ifPresentOrElse(practiceGame -> message(commandSender, PracticeEn.CANNOT_SPECTATE),
                                    () -> new Optional<>(service.getByPlayer(target))
                                            .ifPresentOrElse(practiceGame -> {
                                                new Optional<>(playerService.getByPlayer(player))
                                                        .ifPresentOrElse(practicePlayer -> {
                                                            service.addSpectatorAdmin(practiceGame, practicePlayer, player);
                                                            message(commandSender, PracticeEn.SPECTATE, "name", target.getName());
                                                        }, () -> message(commandSender, CommandsEn.COMMAND_ERROR));
                                            }, () -> message(commandSender, PracticeEn.NOT_IN_GAME, "name", target.getName())));
                }, () -> message(commandSender, CommandsEn.COMMAND_INVALID_SYNTAX, "syntax", getSyntax()));
        return CommandResult.SUCCESS;
    }
}
