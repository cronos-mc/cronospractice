package com.cronos.practice.bukkit.model.hit;

import lombok.Data;
import lombok.Setter;

import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
@Setter
public class HitRecord {

    private final UUID id;
    private int average;
    private int highest;
    private int hits;
    private long checkStart;
    private int tmpCps;

    public HitRecord(UUID id) {
        this.id = id;
        this.average = 0;
        this.highest = 0;
        this.checkStart = System.currentTimeMillis();
        this.tmpCps = 0;
        this.hits = 0;
    }
}
