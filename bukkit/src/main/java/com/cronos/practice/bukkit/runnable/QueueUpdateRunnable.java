package com.cronos.practice.bukkit.runnable;

import com.cronos.commons.utils.Optional;
import com.cronos.core.bukkit.BukkitCore;
import com.cronos.core.commons.party.Party;
import com.cronos.core.commons.party.PartyService;
import com.cronos.core.commons.players.PlayerProfile;
import com.cronos.core.commons.players.PlayerRepository;
import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.event.queue.QueueQuitEvent;
import com.cronos.practice.bukkit.internalization.PracticeEn;
import com.cronos.practice.bukkit.model.arena.ArenaState;
import com.cronos.practice.bukkit.model.arena.PracticeArena;
import com.cronos.practice.bukkit.model.duel.DuelQueue;
import com.cronos.practice.bukkit.model.game.GameType;
import com.cronos.practice.bukkit.model.game.SoloGame;
import com.cronos.practice.bukkit.model.game.TeamGame;
import com.cronos.practice.bukkit.model.player.PracticePlayer;
import com.cronos.practice.bukkit.model.queue.QueueElement;
import com.cronos.practice.bukkit.model.queue.QueueMatch;
import com.cronos.practice.bukkit.model.team.PracticeTeam;
import com.cronos.practice.bukkit.service.ArenaService;
import com.cronos.practice.bukkit.service.GameService;
import com.cronos.practice.bukkit.service.PlayerService;
import com.cronos.practice.bukkit.service.QueueService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Slf4j(topic = "Queue")
public class QueueUpdateRunnable implements Runnable {

    private final QueueService queueService;
    private final GameService gameService;
    private final ArenaService arenaService;
    private final PlayerRepository playerRepository;
    private final PlayerService playerService;
    private final PartyService partyService;

    public QueueUpdateRunnable(@NonNull Bootstrap bootstrap, @NonNull QueueService queueService) {
        this.queueService = queueService;
        this.gameService = bootstrap.getGameService();
        this.arenaService = bootstrap.getArenaService();
        this.playerRepository = BukkitCore.getPlugin(BukkitCore.class).getPlayerRepository();
        this.partyService = BukkitCore.getPlugin(BukkitCore.class).getPartyService();
        this.playerService = bootstrap.getPlayerService();
    }

    @Override
    public void run() {
        refresh(GameType.RANKED);
        refreshDuels();
        refresh(GameType.UNRANKED);
    }

    private void refreshDuels() {
        LinkedList<DuelQueue> queue = queueService.getElements()
                .stream()
                .filter(queueElement -> queueElement instanceof DuelQueue)
                .map(queueElement -> (DuelQueue) queueElement)
                .sorted(Comparator.comparing(QueueElement::getSubscriptionDate))
                .collect(Collectors.toCollection(LinkedList::new));
        if (queue.size() == 0)
            return;
        queue.forEach(duelQueue -> arenaService.getArenas()
                .stream()
                .filter(practiceArena -> practiceArena.getState().equals(ArenaState.AVAILABLE))
                .filter(practiceArena -> practiceArena.getKits().contains(duelQueue.getKit()))
                .findFirst()
                .ifPresent(practiceArena -> {
                    PracticePlayer p1 = playerService.getByPlayer(duelQueue.getPlayer1()).orElse(null);
                    PracticePlayer p2 = playerService.getByPlayer(duelQueue.getPlayer2()).orElse(null);
                    queueService.getElements().remove(duelQueue);
                    if (Objects.isNull(p1) || Objects.isNull(p2))
                        return;
                    gameService.add(new SoloGame(Arrays.asList(p1, p2), practiceArena, duelQueue.getKit(), duelQueue.getType()));
                }));
    }

    private void refresh(@NonNull GameType type) {
        getMatchingElements(type)
                .ifPresent(queueMatch -> getArena(queueMatch)
                        .ifPresent(arena -> createGame(queueMatch, arena)));
    }

    private void createGame(@NonNull QueueMatch match, @NonNull PracticeArena arena) {
        match.getElement1().message(PracticeEn.GAME_FOUND, "kit", match.getKit().getDisplayName());
        match.getElement2().message(PracticeEn.GAME_FOUND, "kit", match.getKit().getDisplayName());

        queueService.remove(match.getElement1(), QueueQuitEvent.Reason.GAME_JOIN);
        queueService.remove(match.getElement2(), QueueQuitEvent.Reason.GAME_JOIN);

        if (match.isParty())
            match.getElement1()
                    .getAsParty()
                    .ifPresent(party -> match.getElement2()
                            .getAsParty()
                            .ifPresent(party1 -> toPracticeTeam(party1)
                                    .ifPresent(practiceTeam -> toPracticeTeam(party)
                                            .ifPresent(practiceTeam1 -> gameService.add(new TeamGame(Arrays.asList(practiceTeam, practiceTeam1), arena, match.getKit(), match.getType()))))));
        else
            match.getElement1()
                    .getAsPlayer()
                    .ifPresent(player -> playerService.getByPlayer(player)
                            .ifPresent(player1 -> match.getElement2()
                                    .getAsPlayer()
                                    .ifPresent(player2 -> playerService.getByPlayer(player2)
                                            .ifPresent(player3 -> gameService.add(new SoloGame(Arrays.asList(player1, player3), arena, match.getKit(), match.getType()))))));
    }

    private Optional<PracticeArena> getArena(@NonNull QueueMatch match) {
        List<PracticeArena> a = arenaService.getByKit(match.getKit())
                .stream()
                .filter(arena -> arena.getState().equals(ArenaState.AVAILABLE))
                .collect(Collectors.toList());
        if (a.size() == 0)
            return Optional.empty();
        if (match.isParty()) {
            int size = match.getElement1()
                    .getAsParty()
                    .map(Party::getMembers)
                    .map(List::size)
                    .orElse(4);
            a.removeIf(arena -> arena.getSpawnLocations().size() < size);
        }

        return Optional.of(a.get(new Random().nextInt(a.size())));
    }

    private Optional<QueueMatch> getMatchingElements(@NonNull GameType type) {
        LinkedList<QueueElement> elements = queueService.getElements()
                .stream()
                .filter(queueElement -> !(queueElement instanceof DuelQueue))
                .collect(Collectors.toCollection(LinkedList::new));
        List<QueueElement> potentialMatches = new ArrayList<>();
        QueueElement element = null;
        for (QueueElement e : elements) {
            element = e;
            potentialMatches = new ArrayList<>();
            for (QueueElement e1 : elements) {
                if (areMatchable(e, e1))
                    potentialMatches.add(e1);
            }
            if (potentialMatches.size() > 0)
                break;
        }
        if (potentialMatches.size() == 0)
            return Optional.empty();
        int elo = getElo(element);
        QueueElement finalElement = element;
        return new Optional<>(potentialMatches.stream()
                .min(Comparator.comparingInt(o -> Math.abs(elo - getElo(o))))
                .map(element1 -> new QueueMatch(finalElement,
                        element1,
                        element1.getKit(),
                        type,
                        element1.isParty())));
    }

    private boolean areMatchable(@NonNull QueueElement e1, @NonNull QueueElement e2) {
        boolean sameSize;
        if (e1.isParty() && e2.isParty())
            sameSize = partyService.getPartyById(e1.getParticipant())
                    .flatMap(party -> partyService.getPartyById(e1.getParticipant())
                            .map(party1 -> party.getMembers().size() == party1.getMembers().size()))
                    .orElse(false);
        else
            sameSize = true;
        return e2.getKit().equals(e1.getKit())
                && e1.getType().equals(e2.getType())
                && e1.isParty() == e2.isParty()
                && e1.getParticipant() != e2.getParticipant()
                && sameSize;
    }

    private int getElo(@NonNull QueueElement element) {
        if (!element.isParty())
            return playerRepository.getByUniqueId(element.getParticipant())
                    .map(PlayerProfile::getElo)
                    .orElse(0);
        AtomicInteger elo = new AtomicInteger(0);
        element.getAsParty()
                .ifPresent(party -> party.getMembers()
                        .forEach(uuid -> playerRepository.getByUniqueId(uuid)
                                .ifPresent(profile -> elo.addAndGet(profile.getElo()))));
        return elo.get();
    }

    private Optional<PracticeTeam> toPracticeTeam(@NonNull Party party) {
        return new Optional<>(playerService.getByUniqueId(party.getOwner())
                .map(player -> new PracticeTeam(party.getId(), player, party.getMembers()
                        .stream()
                        .map(playerService::getByUniqueId)
                        .filter(java.util.Optional::isPresent)
                        .map(java.util.Optional::get)
                        .collect(Collectors.toList()))));
    }
}
