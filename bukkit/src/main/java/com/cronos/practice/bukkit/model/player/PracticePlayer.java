package com.cronos.practice.bukkit.model.player;

import com.cronos.bukkit.utils.xseries.XSound;
import com.cronos.commons.internalization.TranslationPack;
import com.cronos.practice.bukkit.model.participant.GameParticipant;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import net.md_5.bungee.api.chat.BaseComponent;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.Objects;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
@Setter
public class PracticePlayer extends GameParticipant {

    private final String name;
    private final OfflinePlayer player;
    private PlayerState state = PlayerState.AVAILABLE;

    public PracticePlayer(Player player) {
        super(player.getUniqueId());
        this.name = player.getName();
        this.player = player;
    }

    @Override
    public <T extends TranslationPack> void message(T translation, Object... placeholders) {
        getAsPlayer().ifPresent(player1 -> message(player1, translation, placeholders));
    }

    @Override
    public void message(String message, Object... placeholders) {
        getAsPlayer().ifPresent(player1 -> message(player1, message, placeholders));
    }

    @Override
    public void message(BaseComponent message) {
        getAsPlayer().map(Player::spigot)
                .ifPresent(spigot -> spigot.sendMessage(message));
    }

    @Override
    public void playSound(@NonNull XSound sound) {
        getAsPlayer().ifPresent(player1 -> sound.play(player1, 1, 1));
    }

    public Optional<Player> getAsPlayer() {
        if (Objects.nonNull(player.getPlayer()) && player.isOnline())
            return Optional.of(player.getPlayer());
        return Optional.empty();
    }

    public boolean isConnected() {
        return getAsPlayer().isPresent();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PracticePlayer that = (PracticePlayer) o;
        return Objects.equals(getUniqueId(), that.getUniqueId()) && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUniqueId(), name);
    }
}
