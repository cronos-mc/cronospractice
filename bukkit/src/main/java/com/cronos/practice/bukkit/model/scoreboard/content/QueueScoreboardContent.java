package com.cronos.practice.bukkit.model.scoreboard.content;

import com.cronos.commons.utils.Optional;
import com.cronos.practice.bukkit.Practice;
import com.cronos.practice.bukkit.internalization.PracticeEn;
import com.cronos.practice.bukkit.model.kit.PracticeKit;
import com.cronos.practice.bukkit.model.queue.QueueElement;
import com.cronos.practice.bukkit.service.QueueService;
import lombok.NonNull;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class QueueScoreboardContent extends ScoreboardContent {

    @Override
    public List<String> getLines(@NonNull Player player) {
        QueueService service = Practice.getBootstrap().getQueueService();
        Optional<QueueElement> qe = service.getByPlayer(player);
        String kitName = qe.map(QueueElement::getKit)
                .map(PracticeKit::getIcon)
                .map(ItemStack::getItemMeta)
                .map(ItemMeta::getDisplayName)
                .orElse("?");
        return new ArrayList<>(format(PracticeEn.SCOREBOARD_QUEUE,
                "position",
                service.getPosition(player) + 1,
                "kit",
                kitName,
                "in_queue",
                qe.map(q -> service.countByTypeAndKit(q.getType(), q.getKit())).orElse(0)));
    }

}
