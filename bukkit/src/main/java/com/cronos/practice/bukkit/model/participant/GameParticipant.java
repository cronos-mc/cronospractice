package com.cronos.practice.bukkit.model.participant;

import com.cronos.bukkit.utils.BukkitUtil;
import com.cronos.bukkit.utils.xseries.XSound;
import com.cronos.commons.internalization.TranslationPack;
import com.cronos.practice.bukkit.Practice;
import lombok.Getter;
import lombok.NonNull;
import net.md_5.bungee.api.chat.BaseComponent;

import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public abstract class GameParticipant extends BukkitUtil {

    private final UUID uniqueId;

    public GameParticipant(UUID uniqueId) {
        super(Practice.getPlugin(Practice.class));
        this.uniqueId = uniqueId;
    }

    public abstract <T extends TranslationPack> void message(T translation, Object... placeholders);

    public abstract void message(String message, Object... placeholders);

    public abstract void message(BaseComponent message);

    public abstract void playSound(@NonNull XSound sound);
}
