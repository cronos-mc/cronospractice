package com.cronos.practice.bukkit.model.history;

import com.cronos.practice.bukkit.model.game.SoloGame;
import com.cronos.practice.bukkit.model.game.TeamGame;
import com.cronos.practice.bukkit.model.kit.PracticeKit;
import lombok.Data;
import lombok.NonNull;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
@Setter
public class MatchHistory {

    private final UUID id;
    private final UUID gameId;
    private final PracticeKit kit;
    private final List<MatchEntry> entries = new ArrayList<>();
    private String winner;
    private String loser;

    public MatchHistory(@NonNull SoloGame game) {
        this.id = UUID.randomUUID();
        this.gameId = game.getUniqueId();
        this.kit = game.getKit();
    }

    public MatchHistory(@NonNull TeamGame game) {
        this.id = UUID.randomUUID();
        this.kit = game.getKit();
        this.gameId = game.getUniqueId();
    }


}
