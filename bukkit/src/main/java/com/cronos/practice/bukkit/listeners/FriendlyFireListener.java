package com.cronos.practice.bukkit.listeners;

import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.model.game.GameState;
import com.cronos.practice.bukkit.service.GameService;
import lombok.NonNull;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class FriendlyFireListener implements Listener {

    private final GameService service;

    public FriendlyFireListener(Bootstrap bootstrap) {
        this.service = bootstrap.getGameService();
    }

    @EventHandler
    public void onPlayerDamage(@NonNull EntityDamageByEntityEvent e) {
        if (!(e.getEntity() instanceof Player)
                || !(e.getDamager() instanceof Player))
            return;
        Player damaged = (Player) e.getEntity();
        Player damager = (Player) e.getDamager();
        service.getByPlayer(damaged)
                .filter(game -> game.getState().equals(GameState.PLAYING))
                .flatMap(g -> service.getTeamByPlayer(damaged))
                .ifPresent(practiceTeam -> {
                    if (practiceTeam.getMembers()
                            .stream()
                            .anyMatch(player -> player.getUniqueId().equals(damager.getUniqueId())))
                        e.setCancelled(true);
                });
    }
}
