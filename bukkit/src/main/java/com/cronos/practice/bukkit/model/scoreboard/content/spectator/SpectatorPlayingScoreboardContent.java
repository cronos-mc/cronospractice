package com.cronos.practice.bukkit.model.scoreboard.content.spectator;

import com.cronos.practice.bukkit.internalization.PracticeEn;
import com.cronos.practice.bukkit.model.game.PracticeGame;
import com.cronos.practice.bukkit.utils.PlayerUtils;
import lombok.NonNull;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class SpectatorPlayingScoreboardContent extends SpectatorScoreboardContent {

    @Override
    public List<String> getSoloContent(@NonNull PracticeGame<?> game, @NonNull Player player1, @NonNull Player player2) {
        return new ArrayList<>(format(PracticeEn.SCOREBOARD_SPECTATOR_PLAYING,
                "player1", player1.getName(),
                "p1_ping", PlayerUtils.getPing(player1),
                "player2", player2.getName(),
                "p2_ping", PlayerUtils.getPing(player2)));
    }
}