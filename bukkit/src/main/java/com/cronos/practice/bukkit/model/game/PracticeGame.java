package com.cronos.practice.bukkit.model.game;

import com.cronos.bukkit.utils.xseries.XSound;
import com.cronos.commons.internalization.TranslationPack;
import com.cronos.practice.bukkit.Practice;
import com.cronos.practice.bukkit.event.game.GameStateChangeEvent;
import com.cronos.practice.bukkit.model.arena.ArenaState;
import com.cronos.practice.bukkit.model.arena.PracticeArena;
import com.cronos.practice.bukkit.model.kit.PracticeKit;
import com.cronos.practice.bukkit.model.participant.GameParticipant;
import com.cronos.practice.bukkit.model.player.PracticePlayer;
import com.cronos.practice.bukkit.runnable.game.*;
import lombok.Data;
import lombok.NonNull;
import lombok.Setter;
import net.md_5.bungee.api.chat.BaseComponent;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public abstract class PracticeGame<P extends GameParticipant> {

    private final UUID uniqueId;
    private final List<P> participants;
    private final List<PracticePlayer> spectators = new ArrayList<>();
    private final PracticeArena arena;
    private final PracticeKit kit;
    private final GameType type;
    private GameRunnable currentRunnable;
    @Setter
    private GameParticipant winner;

    private GameState state = GameState.WAITING;

    public <T extends TranslationPack> void message(T translation, Object... placeholders) {
        participants.forEach(p -> p.message(translation, placeholders));
        spectators.forEach(p -> p.message(translation, placeholders));
    }

    public void message(String message, Object... placeholders) {
        participants.forEach(p -> p.message(message, placeholders));
        spectators.forEach(p -> p.message(message, placeholders));
    }

    public void message(BaseComponent message) {
        participants.forEach(p -> p.message(message));
        spectators.forEach(p -> p.message(message));
    }

    public void playSound(@NonNull XSound sound) {
        participants.forEach(p -> p.playSound(sound));
        spectators.forEach(p -> p.playSound(sound));
    }

    public void setState(GameState state) {
        if (Objects.nonNull(currentRunnable))
            currentRunnable.cancel();
        if (this.state.equals(state))
            return;
        switch (state) {
            case STARTING:
                arena.setState(ArenaState.BUSY);
                currentRunnable = new GameStartRunnable(this, 5);
                break;
            case PLAYING:
                currentRunnable = new GamePlayRunnable(this);
                break;
            case CELEBRATING:
                currentRunnable = new GameEndRunnable(this, 3);
                break;
            case ENDED:
                currentRunnable = new GameEndedRunnable(this);
                break;
        }
        if (Objects.nonNull(currentRunnable))
            currentRunnable.runTaskTimer(Practice.getPlugin(Practice.class), 0, 20);
        Bukkit.getPluginManager().callEvent(new GameStateChangeEvent(this, this.state, state));
        this.state = state;
    }
}
