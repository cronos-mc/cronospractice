package com.cronos.practice.bukkit.listeners.gamemode;

import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.service.GameService;
import com.cronos.practice.bukkit.service.ScoreboardService;
import org.bukkit.event.Listener;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class BoxingListener implements Listener {

    private final ScoreboardService scoreboardService;
    private final GameService gameService;

    public BoxingListener(Bootstrap bootstrap) {
        this.gameService = bootstrap.getGameService();
        this.scoreboardService = bootstrap.getScoreboardService();
    }
}
