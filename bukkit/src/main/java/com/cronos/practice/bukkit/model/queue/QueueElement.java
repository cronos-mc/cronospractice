package com.cronos.practice.bukkit.model.queue;

import com.cronos.bukkit.utils.BukkitUtil;
import com.cronos.commons.internalization.TranslationPack;
import com.cronos.commons.utils.Optional;
import com.cronos.core.bukkit.BukkitCore;
import com.cronos.core.commons.party.Party;
import com.cronos.practice.bukkit.Practice;
import com.cronos.practice.bukkit.model.kit.PracticeKit;
import com.cronos.practice.bukkit.model.game.GameType;
import lombok.Getter;
import lombok.NonNull;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class QueueElement extends BukkitUtil {

    private final UUID participant;
    private final PracticeKit kit;
    private final GameType type;
    private final boolean party;
    private final Date subscriptionDate;

    public QueueElement(UUID id, boolean party, PracticeKit kit, GameType type) {
        super(Practice.getPlugin(Practice.class));
        this.participant = id;
        this.kit = kit;
        this.type = type;
        this.party = party;
        this.subscriptionDate = new Date();
    }

    public Optional<Player> getAsPlayer() {
        if (party)
            return Optional.empty();
        return Optional.ofNullable(Bukkit.getPlayer(participant));
    }

    public Optional<Party> getAsParty() {
        if (!party)
            return Optional.empty();
        return BukkitCore.getPlugin(BukkitCore.class).getPartyService().getPartyById(participant);
    }

    public <T extends TranslationPack> void message(@NonNull T translation, Object... placeholders) {
        getAsParty().ifPresentOrElse(party1 -> party1.getMembers()
                        .stream()
                        .map(Bukkit::getPlayer)
                        .filter(Objects::nonNull)
                        .forEach(player -> message(player, translation, placeholders)),
                () -> getAsPlayer().ifPresent(player -> message(player, translation, placeholders)));
    }
}
