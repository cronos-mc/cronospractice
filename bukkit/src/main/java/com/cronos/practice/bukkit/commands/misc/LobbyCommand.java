package com.cronos.practice.bukkit.commands.misc;

import com.cronos.bukkit.commands.BukkitCommand;
import com.cronos.commons.commands.CommandResult;
import com.cronos.commons.commands.SenderType;
import com.cronos.practice.bukkit.Practice;
import com.cronos.practice.bukkit.configuration.Configuration;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class LobbyCommand extends BukkitCommand {

    private final Configuration configuration;

    public LobbyCommand(Practice plugin) {
        super(plugin, "lobby");
        this.configuration = Practice.getBootstrap().getConfiguration();
        setAllowedSenderType(SenderType.PLAYER);
    }

    @Override
    protected CommandResult execute(CommandSender commandSender, Object[] objects) {
        ((Player) commandSender).teleport(configuration.getSpawnLocation());
        return CommandResult.SUCCESS;
    }
}
