package com.cronos.practice.bukkit.model.gamemode;

import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.listeners.gamemode.BoxingListener;
import com.cronos.practice.bukkit.model.kit.SpecialKit;
import com.cronos.practice.bukkit.runnable.gamemode.BoxingUpdateRunnable;
import lombok.NonNull;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class BoxingGameMode implements PracticeGameMode {

    @Override
    public SpecialKit getKit() {
        return SpecialKit.BOXING;
    }

    @Override
    public void init(@NonNull Bootstrap bootstrap) {
        bootstrap.registerListeners(new BoxingListener(bootstrap));
        new BoxingUpdateRunnable(bootstrap).runTaskTimer(bootstrap.getPlugin(), 0, 1);
    }
}
