package com.cronos.practice.bukkit.repository;

import com.cronos.commons.utils.Optional;
import com.cronos.practice.bukkit.model.duel.DuelRequest;
import lombok.Data;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class DuelRepository {

    private final List<DuelRequest> requests = new ArrayList<>();

    public void add(@NonNull DuelRequest request) {
        requests.add(request);
    }

    public void remove(@NonNull DuelRequest request) {
        requests.remove(request);
    }

    public Optional<DuelRequest> getRequest(@NonNull UUID requester, @NonNull UUID target) {
        return new Optional<>(requests.stream()
                .filter(request -> request.getRequester().getUniqueId().equals(requester))
                .filter(request -> request.getRequested().getUniqueId().equals(target))
                .findFirst());
    }
}
