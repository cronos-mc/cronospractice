package com.cronos.practice.bukkit.model.winner;

import com.cronos.practice.bukkit.model.game.SoloGame;
import com.cronos.practice.bukkit.model.player.PlayerState;
import com.cronos.practice.bukkit.model.player.PracticePlayer;
import lombok.NonNull;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class SoloWinnerSelector implements WinnerSelector<PracticePlayer, SoloGame> {

    @Override
    public Optional<PracticePlayer> getWinner(@NonNull SoloGame game) {
        List<PracticePlayer> remaining = game.getParticipants()
                .stream()
                .filter(player -> player.getState().equals(PlayerState.ALIVE))
                .collect(Collectors.toList());
        if (remaining.size() != 1)
            return Optional.empty();
        return Optional.ofNullable(remaining.get(0));
    }

}
