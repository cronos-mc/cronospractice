package com.cronos.practice.bukkit.listeners;

import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.service.GameService;
import lombok.NonNull;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class PotionListener implements Listener {

    private final GameService service;

    public PotionListener(@NonNull Bootstrap bootstrap) {
        this.service = bootstrap.getGameService();
    }

    @EventHandler
    public void onPotionDrop(PlayerDropItemEvent e) {
        Item item = e.getItemDrop();
        if (service.isInGame(e.getPlayer().getUniqueId()) && item.getItemStack().getType() == Material.GLASS_BOTTLE)
            item.remove();
    }
}
