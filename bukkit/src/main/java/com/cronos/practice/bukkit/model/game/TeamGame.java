package com.cronos.practice.bukkit.model.game;

import com.cronos.practice.bukkit.model.arena.PracticeArena;
import com.cronos.practice.bukkit.model.kit.PracticeKit;
import com.cronos.practice.bukkit.model.team.PracticeTeam;
import com.cronos.practice.bukkit.model.game.GameType;

import java.util.List;
import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class TeamGame extends PracticeGame<PracticeTeam> {

    public TeamGame(List<PracticeTeam> participants, PracticeArena arena, PracticeKit kit, GameType type) {
        super(UUID.randomUUID(), participants, arena, kit, type);
    }
}
