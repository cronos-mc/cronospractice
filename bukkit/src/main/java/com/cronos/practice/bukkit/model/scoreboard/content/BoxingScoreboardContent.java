package com.cronos.practice.bukkit.model.scoreboard.content;

import com.cronos.practice.bukkit.Practice;
import com.cronos.practice.bukkit.internalization.PracticeEn;
import com.cronos.practice.bukkit.model.game.PracticeGame;
import com.cronos.practice.bukkit.model.game.SoloGame;
import com.cronos.practice.bukkit.model.game.TeamGame;
import com.cronos.practice.bukkit.model.hit.HitRecord;
import com.cronos.practice.bukkit.model.player.PracticePlayer;
import com.cronos.practice.bukkit.service.GameService;
import com.cronos.practice.bukkit.service.HitService;
import com.cronos.practice.bukkit.utils.PlayerUtils;
import lombok.NonNull;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class BoxingScoreboardContent extends ScoreboardContent {

    @Override
    public List<String> getLines(@NonNull Player player) {
        GameService gameService = Practice.getBootstrap().getGameService();
        HitService hitService = Practice.getBootstrap().getHitService();

        Optional<PracticeGame<?>> game = gameService.getByPlayer(player);
        AtomicInteger oPing = new AtomicInteger(0);
        AtomicInteger oHits = new AtomicInteger(0);
        game.ifPresent(game1 -> {
            if (game1 instanceof SoloGame)
                ((SoloGame) game1).getParticipants()
                        .stream()
                        .filter(player1 -> !player1.getUniqueId().equals(player.getUniqueId()))
                        .findFirst()
                        .flatMap(PracticePlayer::getAsPlayer)
                        .ifPresent(player2 -> {
                            hitService.getByPlayer(player2)
                                    .map(HitRecord::getHits)
                                    .ifPresent(oHits::set);
                            oPing.set(PlayerUtils.getPing(player2));
                        });
            else {
                AtomicInteger playersCount = new AtomicInteger(0);
                TeamGame teamGame = (TeamGame) game1;
                teamGame.getParticipants()
                        .stream()
                        .filter(practiceTeam -> practiceTeam.getMembers().stream().noneMatch(player1 -> player1.getUniqueId().equals(player.getUniqueId())))
                        .forEach(practiceTeam -> practiceTeam.getMembers()
                                .stream()
                                .map(PracticePlayer::getAsPlayer)
                                .filter(Optional::isPresent)
                                .map(Optional::get)
                                .forEach(player1 -> {
                                    playersCount.incrementAndGet();
                                    oPing.addAndGet(PlayerUtils.getPing(player1));
                                }));
                oPing.set(oPing.get() / playersCount.get());
            }
        });
        int hits = hitService.getByPlayer(player)
                .map(HitRecord::getHits)
                .orElse(0);
        int diff_ = hits - oHits.get();
        String color = "&a";
        if (diff_ < 0)
            color = "&c";
        String diff = "+" + diff_;
        if (diff_ < 0)
            diff = String.valueOf(diff_);
        return new ArrayList<>(format(PracticeEn.SCOREBOARD_BOXING,
                "my_ping", PlayerUtils.getPing(player),
                "their_ping", oPing.get(),
                "color", color,
                "diff", diff,
                "cps", hits,
                "their_cps", oHits.get()));
    }
}
