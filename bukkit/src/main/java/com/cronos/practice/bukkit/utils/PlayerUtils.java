package com.cronos.practice.bukkit.utils;

import lombok.NonNull;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class PlayerUtils {

    public static int getPing(@NonNull Player player) {
        return ((CraftPlayer) player).getHandle().ping;
    }
}
