package com.cronos.practice.bukkit.service;

import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.configuration.Configuration;
import com.cronos.practice.bukkit.model.game.*;
import com.cronos.practice.bukkit.model.history.MatchHistory;
import com.cronos.practice.bukkit.model.items.PracticeItem;
import com.cronos.practice.bukkit.model.kit.PracticeKit;
import com.cronos.practice.bukkit.model.kit.SpecialKit;
import com.cronos.practice.bukkit.model.player.PlayerState;
import com.cronos.practice.bukkit.model.player.PracticePlayer;
import com.cronos.practice.bukkit.model.scoreboard.Scoreboards;
import com.cronos.practice.bukkit.model.team.PracticeTeam;
import com.cronos.practice.bukkit.repository.GameRepository;
import lombok.NonNull;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class GameService {

    private final GameRepository repository;
    private final HistoryService historyService;
    private final Configuration configuration;
    private final ItemService itemService;
    private final ScoreboardService scoreboardService;

    public GameService(Bootstrap bootstrap) {
        this.repository = new GameRepository();
        this.scoreboardService = bootstrap.getScoreboardService();
        this.configuration = bootstrap.getConfiguration();
        this.historyService = bootstrap.getHistoryService();
        this.itemService = bootstrap.getItemService();
    }

    public void add(@NonNull PracticeGame<?> game) {
        this.repository.add(game);
        game.setState(GameState.STARTING);

        if (game instanceof SoloGame)
            this.historyService.add(new MatchHistory((SoloGame) game));
        else
            this.historyService.add(new MatchHistory((TeamGame) game));
    }

    public void remove(@NonNull PracticeGame<?> game) {
        this.repository.remove(game);
    }

    public boolean isInGame(@NonNull UUID uuid) {
        return this.repository.isInGame(uuid);
    }

    public void addSpectator(@NonNull PracticeGame<?> game, @NonNull PracticePlayer practicePlayer, @NonNull Player player) {
        game.getSpectators().add(practicePlayer);
        practicePlayer.setState(PlayerState.SPECTATOR);
        player.setAllowFlight(true);
        player.setFlying(true);
        getParticipants(game).forEach(player1 -> player1.hidePlayer(player));
        player.setGameMode(GameMode.SURVIVAL);
        player.teleport(game.getArena()
                .getSpawnLocations()
                .get(0)
                .toLocation(game.getArena().getWorld()));

        scoreboardService.getByPlayer(player)
                .filter(practiceScoreboard -> game.getState().equals(GameState.PLAYING))
                .ifPresent(practiceScoreboard -> {
                    if (game.getKit().getSpecialKit().equals(SpecialKit.BOXING))
                        practiceScoreboard.setContent(Scoreboards.SPECTATOR_BOXING);
                    else
                        practiceScoreboard.setContent(Scoreboards.SPECTATOR_PLAYING);
                });

        itemService.give(player, PracticeItem.Type.SPECTATOR);
    }

    public void addSpectatorAdmin(@NonNull PracticeGame<?> game, @NonNull PracticePlayer practicePlayer, @NonNull Player player) {
        game.getSpectators().add(practicePlayer);
        practicePlayer.setState(PlayerState.SPECTATOR);
        getParticipants(game).forEach(player1 -> player1.hidePlayer(player));
        game.getSpectators()
                .stream()
                .map(PracticePlayer::getAsPlayer)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .forEach(player1 -> player1.hidePlayer(player));
        player.setAllowFlight(true);
        player.setFlying(true);
        player.setGameMode(GameMode.ADVENTURE);
        player.teleport(game.getArena()
                .getSpawnLocations()
                .get(0)
                .toLocation(game.getArena().getWorld()));
        itemService.give(player, PracticeItem.Type.SPECTATOR);

        scoreboardService.getByPlayer(player)
                .filter(practiceScoreboard -> game.getState().equals(GameState.PLAYING))
                .ifPresent(practiceScoreboard -> {
                    if (game.getKit().getSpecialKit().equals(SpecialKit.BOXING))
                        practiceScoreboard.setContent(Scoreboards.SPECTATOR_BOXING);
                    else
                        practiceScoreboard.setContent(Scoreboards.SPECTATOR_PLAYING);
                });
    }

    public void removeSpectator(@NonNull PracticeGame<?> game, @NonNull PracticePlayer player) {
        player.setState(PlayerState.AVAILABLE);
        game.getSpectators().remove(player);
        player.getAsPlayer()
                .ifPresent(player1 -> {
                    itemService.give(player1, PracticeItem.Type.HUB);
                    player1.setGameMode(GameMode.SURVIVAL);
                    player1.setFlying(false);
                    player1.setAllowFlight(false);
                    Bukkit.getOnlinePlayers().forEach(player2 -> player2.showPlayer(player1));
                    player1.teleport(configuration.getSpawnLocation());
                });
    }

    public Optional<PracticeTeam> getTeamByPlayer(@NonNull OfflinePlayer player) {
        return this.getTeamByPlayer(player.getUniqueId());
    }

    public Optional<PracticeTeam> getTeamByPlayer(@NonNull UUID uuid) {
        return getByPlayer(uuid)
                .map(game -> {
                    if (!(game instanceof TeamGame))
                        return null;
                    return ((TeamGame) game).getParticipants()
                            .stream()
                            .filter(practiceTeam -> practiceTeam.getMembers()
                                    .stream()
                                    .anyMatch(player -> player.getUniqueId().equals(uuid)))
                            .findFirst()
                            .orElse(null);
                });
    }

    public List<Player> getParticipants(@NonNull PracticeGame<?> game) {
        List<Player> players = new ArrayList<>();
        if (game instanceof TeamGame)
            ((TeamGame) game).getParticipants()
                    .forEach(practiceTeam -> players.addAll(practiceTeam.getMembers()
                            .stream()
                            .map(PracticePlayer::getAsPlayer)
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .collect(Collectors.toList())));
        else
            players.addAll(((SoloGame) game).getParticipants()
                    .stream()
                    .map(PracticePlayer::getAsPlayer)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .collect(Collectors.toList()));
        return players;
    }

    public int getByType(@NonNull GameType type) {
        AtomicInteger i = new AtomicInteger(0);
        getGames().stream()
                .filter(game -> game.getType().equals(type))
                .forEach(game -> {
                     if(game instanceof SoloGame)
                         i.addAndGet(game.getParticipants().size());
                     else
                         ((TeamGame) game).getParticipants()
                                 .forEach(practiceTeam -> i.addAndGet(practiceTeam.getMembers().size()));
                });
        return i.get();
    }

    public int getByTypeAndKit(@NonNull GameType type, @NonNull PracticeKit kit) {
        AtomicInteger i = new AtomicInteger(0);
        getGames().stream()
                .filter(game -> game.getType().equals(type))
                .filter(game -> game.getKit().equals(kit))
                .forEach(game -> {
                    if(game instanceof SoloGame)
                        i.addAndGet(game.getParticipants().size());
                    else
                        ((TeamGame) game).getParticipants()
                                .forEach(practiceTeam -> i.addAndGet(practiceTeam.getMembers().size()));
                });
        return i.get();
    }

    public Optional<PracticeGame<?>> getByPlayer(@NonNull Player player) {
        return this.getByPlayer(player.getUniqueId());
    }

    public Optional<PracticeGame<?>> getByPlayer(@NonNull UUID uuid) {
        return this.repository.getByPlayer(uuid);
    }

    public Optional<PracticeGame<?>> getByUniqueId(@NonNull UUID uuid) {
        return this.repository.getByUniqueId(uuid);
    }

    public List<PracticeGame<?>> getGames() {
        return this.repository.getGames();
    }
}
