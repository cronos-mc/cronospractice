package com.cronos.practice.bukkit.commands.kits;

import com.cronos.bukkit.commands.BukkitCommand;
import com.cronos.commons.commands.CommandResult;
import com.cronos.practice.bukkit.Practice;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class KitCommand extends BukkitCommand {

    public KitCommand(Practice plugin) {
        super(plugin, "kits");
        setDescription("Manage kits.");
        setPermission("practice.kits");
        addSubCommands(new KitCreateCommand(plugin));
    }

    @Override
    protected CommandResult execute(CommandSender commandSender, Object[] objects) {
        // TODO : do something
        return CommandResult.SUCCESS;
    }
}
