package com.cronos.practice.bukkit.model.items;

import lombok.Data;
import org.bukkit.inventory.ItemStack;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class PracticeItem {

    private final int slot;
    private final ItemStack itemStack;
    private final String command;
    private final Type type;

    public enum Type {
        QUEUE,
        HUB,
        SPECTATOR,
        ALL
    }
}
