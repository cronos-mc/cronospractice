package com.cronos.practice.bukkit.internalization;

import com.cronos.commons.internalization.InternalizationService;
import com.cronos.commons.internalization.Translation;
import com.cronos.commons.internalization.TranslationPack;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public enum PracticeEn implements TranslationPack {
    /* SPECTATORS */
    CANNOT_SPECTATE("&c&l!! &eYou can't spectate anyone since you're playing a match!"),
    NOT_IN_GAME("&c&l!! &eYou can't spectate &c%name%&e because he isn't playing a match!"),
    SPECTATE("&a&l!! &7You've been teleported to &a%name%&7 match!"),
    NEW_SPECTATOR("&b&l!! %name%&7 has started spectating."),

    /* Players */
    PLAYER_KILLED("&e&l!! &6%killer%&7 killed &6%killed%&7!", "&e&l!! &6%killer%&7 destroyed &6%killed%&7!"),

    /* Game */
    GAME_FOUND("&a&l!! &7We found a &a%kit%&7 game for you!"),
    GAME_START_TIMER("&6%time%..."),
    GAME_STARTED("&e&l!! &7The match has started!"),
    GAME_WON("&b&l!! %winner%&7 won the game!"),
    GAME_INFORMATION("&e&l%kit% Duel", " &e&l Map: &7%map%", " &e&l Opponent: &c%opponent%", " &e&l Ping: &c%ping% ms"),
    GAME_RESULT("&6&lMatch Results: &7(click player to", "&7view):"),
    GAME_RESULT_NOT_FOUND("&e&l!! &7We couldn't find the match result you asked for!"),

    /* Scoreboards */
    SCOREBOARD_DEFAULT("&7&m--------------", "&fOnline: &6%online%", "&fPlaying: &7%playing%", "&7&m--------------"),
    SCOREBOARD_QUEUE("&7&m--------------", "&fKit: &6%kit%", "&fPosition: &6%position%", "&fIn Queue: &7%in_queue%", "&7&m--------------"),
    SCOREBOARD_WAITING("&7&m--------------", "&fMap: &e%map%", "&fKit: &6%kit%", "&fPing: &6%ping% ms", "&7&m--------------"),
    SCOREBOARD_STARTING("&7&m--------------", "&fGame Start", "&6%time%", "&r", "&fMap: &e%map%", "&fKit: &6%kit%", "&fPing: &6%ping% ms", "&7&m--------------"),
    SCOREBOARD_PLAYING("&7&m--------------", "&r", "&fYour Ping: &6&l%my_ping% ms", "&fTheir Ping: &6&l%their_ping% ms", "&r", "&7&m--------------"),
    SCOREBOARD_BOXING("&7&m--------------", "&r", "&fHits %color%(%diff%)", " &fYou: &6&l%cps%", " &fThem: &6&l%their_cps%", "&r", "&fYour Ping: &6&l%my_ping% ms", "&fTheir Ping: &6&l%their_ping% ms", "&r", "&7&m--------------"),
    SCOREBOARD_ENDING("&7&m--------------", "&fKit: &6%kit%", "&fPosition: &6%position%", "&fIn Queue: &7%in_queue%", "&7&m--------------"),

    /* Spectators Scoreboards */
    SCOREBOARD_SPECTATOR_PLAYING("&7&m--------------", "&r", "&f%player1%'s ping: &6&l%p1_ping% ms", "&f%player2%'s ping: &6&l%p2_ping% ms", "&r", "&7&m--------------"),
    SCOREBOARD_SPECTATOR_BOXING("&7&m--------------", "&r", "&f%player1%: &6&l%p1_cps%", "&f%player2%: &6&l%p2_cps%", "&r", "&fYour Ping: &6&l%my_ping% ms", "&fTheir Ping: &6&l%their_ping% ms", "&r", "&7&m--------------"),

    /* QUEUE */
    QUEUE_JOINED("&a&l &7You joined queue for &a%kit%&7 (&a%type%&7)!"),
    QUEUE_LEFT("&e&l!! &7You &e%kit%&7 left queue!"),
    NOT_IN_QUEUE("&e&l!! &7You are not in any queue!"),

    /* Kits */
    KIT_ALREADY_EXISTS("&c&l!! &eA kit with this name already exists!"),
    KIT_CREATED("&a&l!! &7Kit &a%name%&7 has been created!"),
    KIT_ERROR("&c&l!! &eAn error occurred while saving kit!"),

    /* DUEL */
    DUEL_INVITATION_ALREADY_SENT("&e&l!! &7You already sent a duel invitation to &e%name%&7!"),
    DUEL_INVITATION_SENT("&a&l!! &7You sent a duel invitation to %name%!"),
    DUEL_REQUEST("&e&m                                ", "&7Duel request from &e%player%&7", "&7Kit: &e%kit%&7.", "%actions%", "&e&m                                "),
    NO_DUEL_REQUEST("&e&l!! &7You don't have any duel request from &e%name%&7!"),
    DUEL_REQUEST_ACCEPTED("&a&l!! &7You accepted &a%name%&7 duel request!"),
    DUEL_REQUEST_ACCEPTED_("&a&l!! %name% &7accepted your duel request!"),
    DUEL_REQUEST_DENIED_("&e&l!! &c%name% &edeclined your duel request!"),
    DUEL_REQUEST_DENIED("&a&l!! &7You denied &a%name%&7 duel request!"),

    /* Misc */
    CANNOT_DROP_WEAPON("&c&l!! &eYou can't drop your weapon during a fight!"),
    COMING_SOON("&e&l!! &7This feature isn't fully working. Coming soon");

    private final List<String> content;
    private InternalizationService manager;

    PracticeEn(String... content) {
        this.content = Arrays.asList(content);
    }

    @Override
    public Locale getLanguage() {
        return new Locale("en", "US");
    }

    @Override
    public Translation get() {
        return new Translation(this, this.name(), this.content);
    }

    @Override
    public List<Translation> translations() {
        return Arrays.stream(values()).map(PracticeEn::get).collect(Collectors.toList());
    }

    @Override
    public void setManager(InternalizationService manager) {
        this.manager = manager;
    }
}
