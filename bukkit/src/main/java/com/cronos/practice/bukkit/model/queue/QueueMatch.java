package com.cronos.practice.bukkit.model.queue;

import com.cronos.practice.bukkit.model.kit.PracticeKit;
import com.cronos.practice.bukkit.model.game.GameType;
import lombok.Data;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class QueueMatch {

    private final QueueElement element1;
    private final QueueElement element2;
    private final PracticeKit kit;
    private final GameType type;
    private final boolean party;

}
