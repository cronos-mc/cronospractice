package com.cronos.practice.bukkit.ui.game;

import com.cronos.bukkit.ui.CronosUI;
import com.cronos.bukkit.utils.items.ItemBuilder;
import com.cronos.practice.bukkit.Practice;
import com.cronos.practice.bukkit.model.history.ItemStackType;
import com.cronos.practice.bukkit.model.history.MatchEntry;
import com.cronos.practice.bukkit.model.history.MatchHistory;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class MatchHistoryUI extends CronosUI {

    public MatchHistoryUI(Practice plugin) {
        super(plugin);
        createInventory("&c&lError", 9);
    }

    @Override
    public void open(Player player, Object[] objects) {
        MatchEntry entry = (MatchEntry) objects[0];
        MatchHistory history = (MatchHistory) objects[1];
        createInventory("&f" + entry.getName() + "&6's inventory", 54);

        AtomicInteger slot = new AtomicInteger(0);

        /* Regular Stuff */
        Arrays.stream(entry.getInventory()
                        .getOrDefault(ItemStackType.NORMAL, new ItemStack[]{}))
                .forEach(itemStack -> {
                    if (!Objects.isNull(itemStack))
                        addItem(slot.getAndIncrement(), itemStack);
                    else
                        slot.getAndIncrement();
                });

        /* Armor */
        slot.set(36);
        Arrays.stream(entry.getInventory()
                        .getOrDefault(ItemStackType.ARMOR, new ItemStack[]{}))
                .forEach(itemStack -> {
                    if (!Objects.isNull(itemStack))
                        addItem(slot.getAndIncrement(), itemStack);
                    else
                        slot.getAndIncrement();
                });

        /* Information */
        int hunger = entry.getHunger() / 2;
        addItem(48, new ItemBuilder(Material.COOKED_BEEF)
                .setAmount(hunger)
                .setName("&6Hunger: &f " + hunger + "/10"));
        int health = (int) entry.getHealth();
        if (health < 0.5)
            health = 0;
        addItem(49, new ItemBuilder(Material.SKULL_ITEM)
                .setAmount(health)
                .setName("&6Health: &f " + health + "/20"));
        addItem(50, new ItemBuilder(Material.DIAMOND_SWORD)
                .setName("&f&lCPS")
                .setLore("&r",
                        "&6Average",
                        "&f" + entry.getStatistics().getAverage() + " CPS",
                        "&r",
                        "&6Highest",
                        "&f" + entry.getStatistics().getHighest() + " CPS"));

        /* Navigation */
        MatchEntry next = getNext(history, entry);
        MatchEntry previous = getPrevious(history, entry);

        addItem(45, new ItemBuilder(Material.ARROW)
                .setName("&f" + previous.getName()))
                .setClickAction(clickEvent -> player.performCommand("vgh " + previous.getName() + " " + history.getId().toString()));

        addItem(53, new ItemBuilder(Material.ARROW)
                .setName("&f" + next.getName()))
                .setClickAction(clickEvent -> player.performCommand("vgh " + next.getName() + " " + history.getId().toString()));
    }

    private MatchEntry getNext(MatchHistory history, MatchEntry entry) {
        int index = history.getEntries().indexOf(entry) + 1;
        if (index >= history.getEntries().size())
            index = 0;
        return history.getEntries().get(index);
    }

    private MatchEntry getPrevious(MatchHistory history, MatchEntry entry) {
        int index = history.getEntries().indexOf(entry) - 1;
        if (index == -1)
            index = history.getEntries().size() - 1;
        return history.getEntries().get(index);
    }
}
