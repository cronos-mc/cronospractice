package com.cronos.practice.bukkit.model.scoreboard.content.spectator;

import com.cronos.practice.bukkit.Practice;
import com.cronos.practice.bukkit.internalization.PracticeEn;
import com.cronos.practice.bukkit.model.game.PracticeGame;
import com.cronos.practice.bukkit.model.hit.HitRecord;
import com.cronos.practice.bukkit.service.HitService;
import com.cronos.practice.bukkit.utils.PlayerUtils;
import lombok.NonNull;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class SpectatorBoxingScoreboardContent extends SpectatorScoreboardContent {

    @Override
    public List<String> getSoloContent(@NonNull PracticeGame<?> game, @NonNull Player player1, @NonNull Player player2) {
        HitService service = Practice.getBootstrap().getHitService();
        return new ArrayList<>(format(PracticeEn.SCOREBOARD_SPECTATOR_BOXING,
                "player1", player1.getName(),
                "p1_ping", PlayerUtils.getPing(player1),
                "p1_cps", service.getByPlayer(player1).map(HitRecord::getHits).orElse(0),
                "p1_cps_diff", getCpsDiff(player1, player2),
                "player2", player2.getName(),
                "p2_ping", PlayerUtils.getPing(player2),
                "p2_cps_diff", getCpsDiff(player2, player1),
                "p2_cps", service.getByPlayer(player2).map(HitRecord::getHits).orElse(0)));
    }

    private String getCpsDiff(@NonNull Player p1, @NonNull Player p2) {
        HitService service = Practice.getBootstrap().getHitService();

        int c1 = service.getByPlayer(p1).map(HitRecord::getHits).orElse(0);
        int c2 = service.getByPlayer(p2).map(HitRecord::getHits).orElse(0);
        ChatColor color = ChatColor.WHITE;
        String sign = "";
        if (c1 > c2) {
            color = ChatColor.GREEN;
            sign = "+";
        } else if (c1 < c2) {
            color = ChatColor.RED;
            sign = "-";
        }
        return color + "(" + sign + " " + Math.abs(c1 - c2) + ")";
    }
}
