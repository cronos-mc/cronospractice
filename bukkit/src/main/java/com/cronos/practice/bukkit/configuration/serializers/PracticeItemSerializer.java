package com.cronos.practice.bukkit.configuration.serializers;

import com.cronos.bukkit.configuration.serializers.ItemStackSerializer;
import com.cronos.commons.exceptions.YmlDeserializationException;
import com.cronos.commons.exceptions.YmlSerializationException;
import com.cronos.commons.utils.Optional;
import com.cronos.practice.bukkit.model.items.PracticeItem;
import lombok.NonNull;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class PracticeItemSerializer extends PracticeSerializer<PracticeItem> {

    private static final ItemStackSerializer itemStackSerializer = new ItemStackSerializer();

    @Override
    public Optional<PracticeItem> deserialize(@NonNull ConfigurationSection section) throws YmlDeserializationException {
        if (!exists("material", section)
                || !exists("slot", section)
                || !exists("command", section)
                || !exists("type", section))
            return Optional.empty();
        ItemStack itemStack = itemStackSerializer.deserialize(section)
                .orElseThrow(() -> new YmlDeserializationException("Invalid item!"));
        int slot = section.getInt("slot");
        String command = section.getString("command");
        PracticeItem.Type type = PracticeItem.Type.valueOf(section.getString("type", "HUB"));
        return Optional.of(new PracticeItem(slot, itemStack, command, type));
    }

    @Override
    public void serialize(ConfigurationSection section, @NonNull PracticeItem item) throws YmlSerializationException {

    }
}
