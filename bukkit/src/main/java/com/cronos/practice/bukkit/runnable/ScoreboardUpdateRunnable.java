package com.cronos.practice.bukkit.runnable;

import com.cronos.commons.utils.MinecraftColor;
import com.cronos.practice.bukkit.service.ScoreboardService;
import lombok.RequiredArgsConstructor;

import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@RequiredArgsConstructor
public class ScoreboardUpdateRunnable implements Runnable {

    private final ScoreboardService service;

    @Override
    public void run() {
        service.getScoreboards()
                .stream()
                .filter(scoreboard -> Objects.nonNull(scoreboard.getPlayer()))
                .forEach(scoreboard -> {
                    if (Objects.isNull(scoreboard.getContent()))
                        return;
                    scoreboard.getFastBoard().updateTitle(MinecraftColor.translate(scoreboard.getContent().getTitle(scoreboard.getPlayer())));
                    scoreboard.getFastBoard().updateLines(scoreboard.getContent().getLines(scoreboard.getPlayer())
                            .stream()
                            .map(MinecraftColor::translate)
                            .collect(Collectors.toList()));
                });
    }
}
