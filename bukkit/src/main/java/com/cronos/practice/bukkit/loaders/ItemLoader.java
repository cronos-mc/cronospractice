package com.cronos.practice.bukkit.loaders;

import com.cronos.commons.exceptions.YmlDeserializationException;
import com.cronos.commons.tasks.startup.PostLoadTask;
import com.cronos.commons.utils.Optional;
import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.configuration.Configuration;
import com.cronos.practice.bukkit.configuration.serializers.PracticeItemSerializer;
import com.cronos.practice.bukkit.service.ItemService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Slf4j(topic = "Item Loader")
public class ItemLoader implements PostLoadTask {

    private final Configuration configuration;
    private final ItemService service;

    public ItemLoader(@NonNull Bootstrap bootstrap, @NonNull ItemService service) {
        this.configuration = bootstrap.getConfiguration();
        this.service = service;
    }

    @Override
    public void run() {
        log.info("Loading items...");
        service.getItems().clear();
        service.getItems().addAll(configuration.getItems()
                .getKeys(true)
                .stream()
                .map(s -> configuration.getItems().getConfigurationSection(s))
                .filter(Objects::nonNull)
                .map(section -> {
                    try {
                        return new PracticeItemSerializer().deserialize(section);
                    } catch (YmlDeserializationException e) {
                        e.printStackTrace();
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList()));
        log.info("{} items loaded!", service.getItems().size());
    }
}
