package com.cronos.practice.bukkit.model.cooldown;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@RequiredArgsConstructor
@Getter
public enum CooldownType {
    ENDER_PEARL(16),
    ;

    private final int seconds;
}
