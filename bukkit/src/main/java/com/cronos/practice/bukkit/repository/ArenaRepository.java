package com.cronos.practice.bukkit.repository;

import com.cronos.practice.bukkit.model.arena.PracticeArena;
import com.cronos.practice.bukkit.model.kit.PracticeKit;
import lombok.Getter;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ArenaRepository {

    @Getter
    private final List<PracticeArena> arenas = new ArrayList<>();

    public void add(@NonNull PracticeArena arena) {
        this.arenas.add(arena);
    }

    public void remove(@NonNull PracticeArena arena) {
        this.arenas.remove(arena);
    }

    public List<PracticeArena> getByKit(@NonNull PracticeKit kit) {
        return this.arenas
                .stream()
                .filter(arena -> arena.getKits().contains(kit))
                .collect(Collectors.toList());
    }

    public Optional<PracticeArena> getByName(@NonNull String name) {
        return this.arenas
                .stream()
                .filter(arena -> arena.getName().equals(name))
                .findFirst();
    }
}
