package com.cronos.practice.bukkit.runnable.game;

import com.cronos.practice.bukkit.Practice;
import com.cronos.practice.bukkit.model.arena.ArenaState;
import com.cronos.practice.bukkit.model.game.PracticeGame;
import org.bukkit.Bukkit;

import java.util.ArrayList;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class GameEndedRunnable extends GameRunnable {

    public GameEndedRunnable(PracticeGame<?> game) {
        super(game, 0);
    }

    @Override
    public void onTick(int time) {

    }

    @Override
    public void onComplete() {
        getGame().getArena().reset();
        getGame().getArena().setState(ArenaState.AVAILABLE);
        getBukkitPlayers().forEach(player -> Bukkit.getOnlinePlayers().forEach(player::showPlayer));

        /* Spectators */
        new ArrayList<>(game.getSpectators())
                .forEach(player -> Practice.getBootstrap()
                        .getGameService()
                        .removeSpectator(game, player));
    }
}
