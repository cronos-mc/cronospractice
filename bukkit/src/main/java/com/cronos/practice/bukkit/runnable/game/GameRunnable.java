package com.cronos.practice.bukkit.runnable.game;

import com.cronos.practice.bukkit.model.game.PracticeGame;
import com.cronos.practice.bukkit.model.game.SoloGame;
import com.cronos.practice.bukkit.model.game.TeamGame;
import com.cronos.practice.bukkit.model.player.PracticePlayer;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public abstract class GameRunnable extends BukkitRunnable {

    protected final PracticeGame<?> game;
    protected int runningTime;

    public GameRunnable(PracticeGame<?> game, int runningTime) {
        this.game = game;
        this.runningTime = runningTime;
    }

    @Override
    public void run() {
        if (runningTime == 0) {
            onComplete();
            this.cancel();
            return;
        }
        onTick(runningTime);
        runningTime--;
    }

    public abstract void onTick(int time);

    public abstract void onComplete();

    protected List<Player> getBukkitPlayers() {
        return getPlayers()
                .stream()
                .map(PracticePlayer::getAsPlayer)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    protected List<PracticePlayer> getPlayers() {
        if (game instanceof SoloGame)
            return ((SoloGame) game)
                    .getParticipants();
        List<PracticePlayer> players = new ArrayList<>();
        ((TeamGame) game).getParticipants()
                .forEach(practiceTeam -> players.addAll(practiceTeam.getMembers()));
        return players;
    }
}
