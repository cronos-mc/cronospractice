package com.cronos.practice.bukkit;

import com.cronos.bukkit.BukkitPlugin;
import com.cronos.practice.bukkit.commands.duel.DuelCommand;
import com.cronos.practice.bukkit.commands.kits.KitCommand;
import com.cronos.practice.bukkit.commands.misc.LobbyCommand;
import com.cronos.practice.bukkit.commands.misc.ViewHistoryCommand;
import com.cronos.practice.bukkit.commands.misc.WorldCommand;
import com.cronos.practice.bukkit.commands.queue.QueueCommand;
import com.cronos.practice.bukkit.commands.queue.RankedCommand;
import com.cronos.practice.bukkit.commands.queue.UnRankedCommand;
import com.cronos.practice.bukkit.commands.spectate.SpectateAdminCommand;
import com.cronos.practice.bukkit.commands.spectate.SpectateCommand;
import com.cronos.practice.bukkit.configuration.serializers.PracticeItemSerializer;
import com.cronos.practice.bukkit.configuration.serializers.RedisCredentialSerializer;
import com.cronos.practice.bukkit.internalization.PracticeEn;
import com.cronos.practice.bukkit.ui.game.MatchHistoryUI;
import com.cronos.practice.bukkit.ui.queue.GameTypeUI;
import com.cronos.practice.bukkit.ui.queue.KitSelectionUI;
import com.cronos.practice.bukkit.ui.selector.KitSelectorUI;
import com.cronos.practice.bukkit.utils.WorldUtils;
import lombok.Getter;

@Getter
public final class Practice extends BukkitPlugin {

    @Getter
    private static Bootstrap bootstrap;

    @Override
    public void preEnable() {
    }

    @Override
    public void postEnable() {
        /* Hooks */

        /* Worlds */
        WorldUtils.deleteTmpWorlds();

        /* Configuration */
        registerYMLSerializer(RedisCredentialSerializer.class);
        registerYMLSerializer(PracticeItemSerializer.class);

        /* Bootstrap */
        bootstrap = new Bootstrap(this);

        /* Translations */
        registerTranslation(PracticeEn.class);

        /* Commands */
        registerCommand(QueueCommand.class, this);
        registerCommand(RankedCommand.class, this);
        registerCommand(UnRankedCommand.class, this);
        registerCommand(ViewHistoryCommand.class, this);
        registerCommand(KitCommand.class, this);
        registerCommand(WorldCommand.class, this);
        registerCommand(SpectateCommand.class, this);
        registerCommand(SpectateAdminCommand.class, this);
        registerCommand(LobbyCommand.class, this);
        registerCommand(DuelCommand.class, bootstrap);

        /* UI */
        registerUI(GameTypeUI.class, this);
        registerUI(KitSelectionUI.class, this);
        registerUI(MatchHistoryUI.class, this);
        registerUI(KitSelectorUI.class, this);

        /* Worlds */
        registerShutdownHook(WorldUtils::deleteTmpWorlds);
    }

    @Override
    public String getLocale() {
        return "en_US";
    }
}
