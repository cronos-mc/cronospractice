package com.cronos.practice.bukkit.event.queue;

import com.cronos.practice.bukkit.model.queue.QueueElement;
import lombok.Getter;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class QueueQuitEvent extends QueueEvent {

    private final Reason reason;

    public QueueQuitEvent(QueueElement element, Reason reason) {
        super(element);
        this.reason = reason;
    }

    public enum Reason {
        MANUAL,
        GAME_JOIN,
        DISCONNECTION
    }
}
