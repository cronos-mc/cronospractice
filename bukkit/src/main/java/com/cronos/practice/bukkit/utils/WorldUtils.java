package com.cronos.practice.bukkit.utils;

import com.cronos.commons.utils.Optional;
import com.cronos.practice.bukkit.model.arena.PracticeArena;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Slf4j(topic = "World Utils")
public class WorldUtils {

    private static final ArrayList<String> ignored = new ArrayList<>(Arrays.asList("uid.dat", "session.dat"));
    private static int ID = 0;

    public static Optional<World> createTmpWorld(@NonNull PracticeArena arena) {
        return copyWorld(arena)
                .map(file -> new WorldCreator(file.getName())
                        .createWorld());
    }

    public static Optional<File> copyWorld(@NonNull PracticeArena map) {
        String newName = "prc_" + map.getName() + "_" + ++ID;
        File newWorld = new File(Bukkit.getWorldContainer(), newName);
        if (newWorld.exists())
            deleteFolder(newWorld);
        log.info("Copying {} to {}...", map.getOriginalWorld().getPath(), newWorld.getPath());
        copyFileStructure(map.getOriginalWorld(), newWorld);
        return Optional.of(newWorld);
    }

    @SneakyThrows
    public static void deleteTmpWorlds() {
        Bukkit.getWorlds()
                .stream()
                .filter(world -> world.getName().contains("prc_"))
                .forEach(world -> Bukkit.unloadWorld(world, false));
        Files.walk(Bukkit.getWorldContainer().toPath())
                .filter(path -> path.getFileName().toString().contains("prc_"))
                .forEach(path -> {
                    try {
                        FileUtils.deleteDirectory(path.toFile());
                    } catch (IOException ignored1) {
                    }
                });
    }

    public static void deleteWorld(@NonNull World world) {
        Bukkit.unloadWorld(world, false);
        deleteFolder(world.getWorldFolder());
    }

    public static void deleteFolder(@NonNull File world) {
        try {
            Files.walk(world.toPath())
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        } catch (IOException e) {
            log.error("Cannot delete world {}!", world.getName());
            e.printStackTrace();
        }
    }

    private static void copyFileStructure(File source, File target) {
        try {
            if (!ignored.contains(source.getName())) {
                if (source.isDirectory()) {
                    if (!target.exists())
                        if (!target.mkdirs())
                            throw new IOException("Couldn't create world directory!");
                    String[] files = source.list();
                    for (String file : Objects.requireNonNull(files)) {
                        File srcFile = new File(source, file);
                        File destFile = new File(target, file);
                        copyFileStructure(srcFile, destFile);
                    }
                } else {
                    InputStream in = new FileInputStream(source);
                    OutputStream out = new FileOutputStream(target);
                    byte[] buffer = new byte[1024];
                    int length;
                    while ((length = in.read(buffer)) > 0)
                        out.write(buffer, 0, length);
                    in.close();
                    out.close();
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
