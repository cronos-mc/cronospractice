package com.cronos.practice.bukkit.repository;

import com.cronos.practice.bukkit.model.kit.PracticeKit;
import lombok.Getter;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class KitRepository {

    @Getter
    private final List<PracticeKit> kits = new ArrayList<>();

    public void add(@NonNull PracticeKit kit) {
        this.kits.add(kit);
    }

    public void remove(@NonNull PracticeKit kit) {
        this.kits.remove(kit);
    }

    public Optional<PracticeKit> getByName(@NonNull String name) {
        return this.kits.stream()
                .filter(kit -> kit.getName().equals(name))
                .findFirst();
    }
}
