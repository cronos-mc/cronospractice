package com.cronos.practice.bukkit.model.scoreboard;

import com.cronos.commons.utils.MinecraftColor;
import com.cronos.practice.bukkit.model.scoreboard.content.ScoreboardContent;
import fr.mrmicky.fastboard.FastBoard;
import lombok.Data;
import org.bukkit.entity.Player;

import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class PracticeScoreboard {

    private final UUID playerId;
    private final Player player;
    private final FastBoard fastBoard;
    private ScoreboardContent content;

    public PracticeScoreboard(Player player) {
        this.playerId = player.getUniqueId();
        this.player = player;
        this.fastBoard = new FastBoard(player);
        this.content = Scoreboards.LOBBY.getContent();
    }

    public void setContent(Scoreboards content) {
        if (Objects.isNull(content.getContent().getLines(player))) return;
        fastBoard.updateTitle(MinecraftColor.translate(content.getContent().getTitle(player)));
        fastBoard.updateLines(content.getContent().getLines(player)
                .stream()
                .map(MinecraftColor::translate)
                .collect(Collectors.toList()));
        this.content = content.getContent();
    }
}
