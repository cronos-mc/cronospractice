package com.cronos.practice.bukkit.service;

import com.cronos.practice.bukkit.Bootstrap;
import com.cronos.practice.bukkit.loaders.KitLoader;
import com.cronos.practice.bukkit.model.kit.PracticeKit;
import com.cronos.practice.bukkit.repository.KitRepository;
import lombok.Getter;
import lombok.NonNull;

import java.io.File;
import java.util.List;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class KitService {

    private final KitRepository repository;
    @Getter
    private final File dataFolder;

    public KitService(Bootstrap bootstrap) {
        this.repository = new KitRepository();
        this.dataFolder = new File(bootstrap.getPlugin().getDataFolder(), "kits");

        bootstrap.getPlugin()
                .registerPostLoadTask(new KitLoader(bootstrap.getPlugin(), this));
    }

    public void add(@NonNull PracticeKit kit) {
        this.repository.add(kit);
    }

    public void remove(@NonNull PracticeKit kit) {
        this.repository.remove(kit);
    }

    public Optional<PracticeKit> getByName(@NonNull String name) {
        return this.repository.getByName(name);
    }

    public List<PracticeKit> getKits() {
        return this.repository.getKits();
    }
}
