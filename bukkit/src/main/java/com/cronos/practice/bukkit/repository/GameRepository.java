package com.cronos.practice.bukkit.repository;

import com.cronos.practice.bukkit.model.game.PracticeGame;
import com.cronos.practice.bukkit.model.game.SoloGame;
import com.cronos.practice.bukkit.model.game.TeamGame;
import lombok.Data;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class GameRepository {

    private final List<PracticeGame<?>> games = new ArrayList<>();

    public void add(@NonNull PracticeGame<?> game) {
        this.games.add(game);
    }

    public void remove(@NonNull PracticeGame<?> game) {
        this.games.remove(game);
    }

    public boolean isInGame(@NonNull UUID uuid) {
        return this.getByPlayer(uuid).isPresent();
    }

    public Optional<PracticeGame<?>> getByPlayer(@NonNull UUID uuid) {
        return this.games
                .stream()
                .filter(game -> {
                    if (game instanceof SoloGame)
                        return ((SoloGame) game).getParticipants()
                                .stream()
                                .anyMatch(player -> player.getUniqueId().equals(uuid));
                    return ((TeamGame) game).getParticipants()
                            .stream()
                            .anyMatch(practiceTeam -> practiceTeam.getMembers()
                                    .stream()
                                    .anyMatch(player -> player.getUniqueId().equals(uuid)));
                })
                .findFirst();
    }

    public Optional<PracticeGame<?>> getByUniqueId(@NonNull UUID uuid) {
        return this.games.stream()
                .filter(game -> game.getUniqueId().equals(uuid))
                .findFirst();
    }
}
