package com.cronos.practice.bukkit.model.scoreboard.content;

import com.cronos.practice.bukkit.Practice;
import com.cronos.practice.bukkit.internalization.PracticeEn;
import com.cronos.practice.bukkit.model.game.SoloGame;
import com.cronos.practice.bukkit.model.game.TeamGame;
import com.cronos.practice.bukkit.service.GameService;
import lombok.NonNull;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class LobbyScoreboardContent extends ScoreboardContent {

    @Override
    public List<String> getLines(@NonNull Player player) {
        GameService gameService = Practice.getBootstrap().getGameService();
        AtomicInteger playing = new AtomicInteger(0);
        gameService.getGames()
                .forEach(game -> {
                    if (game instanceof TeamGame)
                        ((TeamGame) game).getParticipants()
                                .forEach(practiceTeam -> playing.addAndGet(practiceTeam.getMembers().size()));
                    else
                        playing.addAndGet(((SoloGame) game).getParticipants().size());
                });
        return new ArrayList<>(format(PracticeEn.SCOREBOARD_DEFAULT, "online", Bukkit.getOnlinePlayers().size(), "playing", playing.get()));
    }

}
