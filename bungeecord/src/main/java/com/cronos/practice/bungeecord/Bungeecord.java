package com.cronos.practice.bungeecord;

import com.cronos.bungeecord.BungeecordPlugin;
import com.cronos.messaging.NetworkManager;
import com.cronos.messaging.redis.RedisMessenger;
import com.cronos.practice.bungeecord.configuration.Configuration;
import com.cronos.practice.bungeecord.configuration.serializers.RedisCredentialSerializer;
import com.cronos.practice.bungeecord.internalization.PracticeEn;
import com.cronos.practice.bungeecord.messaging.BungeePacketHandler;
import lombok.Getter;

import java.util.UUID;

@Getter
public final class Bungeecord extends BungeecordPlugin implements NetworkManager {

    /* Identifier */
    private UUID identifier;

    /* Configuration */
    private Configuration configuration;

    /* Messenger */
    private RedisMessenger messenger;

    @Override
    public String getName() {
        return "Practice";
    }

    @Override
    public void preEnable() {
        identifier = UUID.randomUUID();
    }

    @Override
    public void postEnable() {
        /* Serializers */
        registerYMLSerializer(RedisCredentialSerializer.class);

        /* Configuration */
        this.configuration = registerConfiguration(Configuration.class, this);

        /* Messenger */
        BungeePacketHandler packetHandler = new BungeePacketHandler(this);
        this.messenger = new RedisMessenger(configuration.getRedisCredentials(), packetHandler);
        this.messenger.registerChannel(() -> "practice@general");
        this.messenger.init(this);

        /* Translations */
        registerTranslation(PracticeEn.class);
    }

    @Override
    public String getLocale() {
        return "en_US";
    }
}
