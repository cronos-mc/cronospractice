package com.cronos.practice.bungeecord.internalization;

import com.cronos.commons.internalization.InternalizationService;
import com.cronos.commons.internalization.Translation;
import com.cronos.commons.internalization.TranslationPack;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public enum PracticeEn implements TranslationPack {
    /* DUEL */
    DUEL_INVITATION_ALREADY_SENT("&e&l!! &7You already sent a duel invitation to &e%name%&7!"),
    DUEL_INVITATION_SENT("&a&l!! &7You sent a duel invitation to %name%!"),
    DUEL_REQUEST("&e&m                                ", "&7Duel request from &e%player%&7", "&7Kit: &e%kit%&7.", "%actions%", "&e&m                                "),
    NO_DUEL_REQUEST("&e&l!! &7You don't have any duel request from &e%name%&7!"),
    DUEL_REQUEST_ACCEPTED("&a&l!! &7You accepted &a%player%&7 duel request!"),
    DUEL_REQUEST_ACCEPTED_("&a&l!! %name% &7accepted your duel request!"),
    DUEL_REQUEST_DENIED_("&a&l!! %name% &declined your duel request!"),
    DUEL_REQUEST_DENIED("&a&l!! &7You denied &a%name%&7 duel request!"),

    ;

    private final List<String> content;
    private InternalizationService manager;

    PracticeEn(String... content) {
        this.content = Arrays.asList(content);
    }

    @Override
    public Locale getLanguage() {
        return new Locale("en", "US");
    }

    @Override
    public Translation get() {
        return new Translation(this, this.name(), this.content);
    }

    @Override
    public List<Translation> translations() {
        return Arrays.stream(values()).map(PracticeEn::get).collect(Collectors.toList());
    }

    @Override
    public void setManager(InternalizationService manager) {
        this.manager = manager;
    }
}
