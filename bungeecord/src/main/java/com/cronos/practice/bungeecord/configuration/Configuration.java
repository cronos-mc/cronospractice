package com.cronos.practice.bungeecord.configuration;

import com.cronos.bungeecord.configuration.ConfigurableFileWrapper;
import com.cronos.commons.configuration.Configurable;
import com.cronos.messaging.redis.RedisCredentials;
import com.cronos.practice.bungeecord.Bungeecord;
import lombok.Getter;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class Configuration extends ConfigurableFileWrapper {

    @Configurable(key = "redis")
    private RedisCredentials redisCredentials;


    public Configuration(Bungeecord plugin) {
        super(plugin, "config.yml");
    }
}
