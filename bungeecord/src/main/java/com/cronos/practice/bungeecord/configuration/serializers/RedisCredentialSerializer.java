package com.cronos.practice.bungeecord.configuration.serializers;

import com.cronos.bungeecord.configuration.serializers.YmlSerializer;
import com.cronos.commons.exceptions.YmlDeserializationException;
import com.cronos.commons.exceptions.YmlSerializationException;
import com.cronos.commons.utils.Optional;
import com.cronos.messaging.redis.RedisCredentials;
import lombok.NonNull;
import net.md_5.bungee.config.Configuration;

import java.util.List;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class RedisCredentialSerializer implements YmlSerializer<RedisCredentials> {

    @Override
    public Optional<RedisCredentials> deserialize(@NonNull Configuration section) throws YmlDeserializationException {
        String hostname = section.getString("host", "localhost");
        int port = section.getInt("port", 6379);
        String password = section.getString("password");
        boolean ssl = section.getBoolean("ssl");
        return Optional.of(new RedisCredentials(hostname, port, password, ssl));
    }

    @Override
    public List<RedisCredentials> deserializeList(@NonNull Configuration configuration) throws YmlDeserializationException {
        return null;
    }

    @Override
    public void serialize(Configuration section, @NonNull RedisCredentials redisCredentials) throws YmlSerializationException {

    }

    @Override
    public void serializeList(Configuration configuration, @NonNull List<RedisCredentials> list) throws YmlSerializationException {

    }
}
