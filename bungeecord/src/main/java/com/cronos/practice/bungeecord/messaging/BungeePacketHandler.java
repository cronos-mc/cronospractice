package com.cronos.practice.bungeecord.messaging;

import com.cronos.bungeecord.BungeecordPlugin;
import com.cronos.bungeecord.utils.BungeeUtil;
import com.cronos.messaging.IPacket;
import com.cronos.messaging.PacketHandler;
import lombok.NonNull;
import lombok.Setter;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Setter
public class BungeePacketHandler extends BungeeUtil implements PacketHandler {

    public BungeePacketHandler(BungeecordPlugin plugin) {
        super(plugin);
    }

    @Override
    public void handlePacketReception(@NonNull IPacket p) {
    }

}
