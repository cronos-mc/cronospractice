package com.cronos.practice.commons;

import com.cronos.messaging.NetworkManager;
import com.cronos.messaging.Packet;
import lombok.Getter;

import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class OpenKitSelectionPacket extends Packet {

    private final UUID requester;
    private final UUID invited;

    public OpenKitSelectionPacket(NetworkManager manager, UUID requester, UUID invited) {
        super(manager);
        this.requester = requester;
        this.invited = invited;
    }
}
