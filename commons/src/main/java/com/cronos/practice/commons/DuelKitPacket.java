package com.cronos.practice.commons;

import com.cronos.messaging.NetworkManager;
import com.cronos.messaging.Packet;
import lombok.Getter;

import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class DuelKitPacket extends Packet {

    private final String kit;
    private final String name;
    private final UUID requester;
    private final UUID requested;

    public DuelKitPacket(NetworkManager manager, String kit, String name, UUID requester, UUID requested) {
        super(manager);
        this.kit = kit;
        this.name = name;
        this.requester = requester;
        this.requested = requested;
    }
}
