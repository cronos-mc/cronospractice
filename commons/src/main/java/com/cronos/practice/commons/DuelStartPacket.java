package com.cronos.practice.commons;

import com.cronos.messaging.NetworkManager;
import com.cronos.messaging.Packet;
import lombok.Getter;

import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class DuelStartPacket extends Packet {

    private final UUID first;
    private final UUID second;
    private final String kit;

    public DuelStartPacket(NetworkManager manager, UUID first, UUID second, String kit) {
        super(manager);
        this.first = first;
        this.second = second;
        this.kit = kit;
    }
}
